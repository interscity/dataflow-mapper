
#Create topic
bin/kafka-topics.sh --create --zookeeper localhost:2181 --partition 1 --topic test --replication-factor 1

#List topic
bin/kafka-topics.sh --list --zookeeper localhost:2181

#Producer
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test

#Consumer
bin/kafka-console-consumer.sh --zookeeper localhost:2181 --topic test --from-beginning
