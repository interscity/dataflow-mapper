
#Build the application
mvn clean package -DskipTests

#Run all tests
#mvn test

#Run a specific test
mvn -Dtest=<class_name> test

#Example: 
#mvn -Dtest=Hdfs2KafkaTest test
