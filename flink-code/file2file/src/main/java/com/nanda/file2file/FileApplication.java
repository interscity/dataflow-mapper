package com.nanda.file2file;

import com.nanda.file2file.util.FileData;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.core.fs.FileSystem;

/*** Usage: FileApplication --input <path> --output <path>
  * If no parameters are provided, the program is run with default data.
  * This code is a simple Flink program which reads from and writes to files.
***/

public class FileApplication {

    public static void main(String[] args) throws Exception {

        final ParameterTool params = ParameterTool.fromArgs(args);

        // set up the execution environment
        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        // make parameters available in the web interface
        env.getConfig().setGlobalJobParameters(params);

        // get input data
        DataSet<String> text;
        if (params.has("input")) {
            text = env.readTextFile(params.get("input"));

            // read text file from a HDFS running at nnHost:nnPort
            //DataSet<String> hdfsLines = env.readTextFile("hdfs://nnHost:nnPort/" + params.get("input"));


        } else {
            System.out.println("Executing FileApplication with default input data set.");
            System.out.println("Use --input to specify file input.");
            text = FileData.getDefaultTextLineDataSet(env);
        }

        // emit result
        if (params.has("output")) {
            System.out.println(params.get("output"));
            // write DataSet to a file and overwrite the file if it exists
            text.writeAsText("target/" + params.get("output"), FileSystem.WriteMode.OVERWRITE);

            // write DataSet to a file on a HDFS with a namenode running at nnHost:nnPort
            //text.writeAsText("hdfs://namenode:50010/" + params.get("output"), FileSystem.WriteMode.OVERWRITE);

            env.execute("FileApplication Example");
        } else {
            System.out.println("Printing result to stdout. Use --output to specify output path.");
            text.print();
        }
    }
}