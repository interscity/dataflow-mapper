package com.nanda.bdOperations;

import org.apache.flink.api.common.operators.Order;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;

public class SimpleWindowStream {

    public static void main(String args[]) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        DataStream<Tuple2<String, Integer>> data = env.fromElements(
                new Tuple2<>("uno", 1),
                new Tuple2<>("dos", 2),
                new Tuple2<>("tres", 3),
                new Tuple2<>("tres", 4),
                new Tuple2<>("tres", 5),
                new Tuple2<>("tres", 6),
                new Tuple2<>("tres", 7),
                new Tuple2<>("tres", 8)
        );

        final int windowSize = 10;
        final int slideSize = 3;
        /*Try to understand why the results are not being printed*/
        data
                .keyBy(0)
                //.window(TumblingProcessingTimeWindows.of(Time.seconds(10)))
                .countWindow(windowSize, slideSize)
                .maxBy(1)
                .print();

        env.execute("Simple window");


    }
}


