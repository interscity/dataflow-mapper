package com.nanda.bdOperations;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;


/*For this example to work, it is necessary to run 'nc -lk localhost 9000' in
a terminal window*/
public class WindowExample {

    public static void main(String args[]) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<String> source = env.socketTextStream("localhost", 9000);


        DataStream<Tuple2<String, Integer>> values = source
                .flatMap(new FlatMapFunction<String, String>() {
                    @Override
                    public void flatMap(String value, Collector<String> out) {
                        for (String word : value.split("\\s+")) {
                            out.collect(word);
                        }
                    }})
                .map(new MapFunction<String, Tuple2<String, Integer>>() {
                    @Override
                    public Tuple2<String, Integer> map(String key) {
                        return new Tuple2<>(key, 1);
                    }
                });

        values
                .keyBy(0)
                //.timeWindow(Time.seconds(15))
                //.timeWindow(Time.seconds(15),Time.seconds(5))
                .countWindow(5)  //five elements under same key (five occurrences)
                .sum(1)
                .print();

        env.execute();
    }
}
