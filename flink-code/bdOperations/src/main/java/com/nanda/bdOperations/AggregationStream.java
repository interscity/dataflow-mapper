/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nanda.bdOperations;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class AggregationStream {

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<Tuple2<String, Integer>> content = env.fromElements(
                new Tuple2<>("bye", 5),
                new Tuple2<>("bye", 8),
                new Tuple2<>("bye", 2),
                new Tuple2<>("bye", 4)
        );

        /*Print max value seen until now*/
        System.out.println("==>Max");
        content.keyBy(0).maxBy(1).print().setParallelism(1);

        /*Print min value seen until now (so it prints several lines, one per tuple)*/
        System.out.println("==>Min");
        content.keyBy(0).minBy(1).print().setParallelism(1);

        /*Print min value seen until now (so it prints several lines, one per tuple).
        * Only the last one contains the final sum*/
        System.out.println("==>Sum");
        content.keyBy(0).sum(1).print();

        env.execute("Flink Streaming Aggregation example");
    }
}
