package com.nanda.bdOperations;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;

public class SumMapStream {

    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<Tuple2<Integer, Integer>> test = env.fromElements(
          new Tuple2<>(1, 1), new Tuple2<>(1, 2), new Tuple2<>(2, 3));

        //DataStream<Tuple2<Integer, Integer>> test = env.addSource(new Info());

        test
                .keyBy(0)
                //.timeWindow(Time.seconds(10))
                .sum(1)
                .map(new Printer())
                .print();
        env.execute();
    }

    private static class Printer implements MapFunction<Tuple2<Integer, Integer>, Integer> {
        @Override
        public Integer map(Tuple2<Integer, Integer> value) {
            System.out.println(value);
            return value.f1;
        }
    }

    private static class Info extends RichParallelSourceFunction<Tuple2<Integer, Integer>> {
        private volatile boolean running = true;

        @Override
        public void run(SourceContext<Tuple2<Integer, Integer>> ctx) throws Exception {
            final long startTime = System.currentTimeMillis();
            final long numElements = 10;
            final long numKeys = 5;
            Integer val = 1;
            long count = 0L;

            while (running && count < numElements) {
                count++;
                ctx.collect(new Tuple2<>(val++, 1));
                if (val > numKeys) val = 1;
            }
            final long endTime = System.currentTimeMillis();
            System.out.println("Took " + (endTime - startTime) + " msecs for " + numElements + " values");
        }

        @Override
        public void cancel() {
            running = false;
        }
    }
}