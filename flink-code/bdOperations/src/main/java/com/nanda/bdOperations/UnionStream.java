package com.nanda.bdOperations;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class UnionStream {

    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<Tuple2<String, Integer>> first = env.fromElements(
                new Tuple2<>("hello", 1),
                new Tuple2<>("world", 2),
                new Tuple2<>("world", 3),
                new Tuple2<>("world", 4)
        );

        DataStream<Tuple2<String, Integer>> second = env.fromElements(
                new Tuple2<>("ola", 3),
                new Tuple2<>("mis amigos", 4)
        );

        System.out.println("==>Union:");
        first.union(second).print();

    }
}