package com.nanda.bdOperations;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.RichParallelSourceFunction;
import org.apache.flink.streaming.api.windowing.time.Time;

public class SumReduceStream {
    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        DataStream<Tuple2<Integer, Integer>> stream = env.addSource(new Info());

        DataStream<Tuple2<Integer, Integer>> result =
                stream
                        .keyBy(0)
                        .timeWindow(Time.seconds(10))
                        .reduce(new SummingReducer());

        /*WHY does this print not working???*/
        result.print().setParallelism(1);

        env.execute();
    }

    private static class SummingReducer implements ReduceFunction<Tuple2<Integer, Integer>> {
        @Override
        public Tuple2<Integer, Integer> reduce(Tuple2<Integer, Integer> value1, Tuple2<Integer, Integer> value2) {
            System.out.println("Pair of values: " + value1.f1 + " " + (value1.f1 + value2.f1));
            return new Tuple2<>(value1.f0, value1.f1 + value2.f1);
        }
    }

    private static class Info extends RichParallelSourceFunction<Tuple2<Integer, Integer>> {
        private volatile boolean running = true;

        @Override
        public void run(SourceContext<Tuple2<Integer, Integer>> ctx) throws Exception {
            final long startTime = System.currentTimeMillis();
            final long numElements = 10;
            final long numKeys = 5;
            Integer val = 1;
            long count = 0L;

            while (running && count < numElements) {
                count++;
                ctx.collect(new Tuple2<>(val++, 1));
                if (val > numKeys) val = 1;
            }
            final long endTime = System.currentTimeMillis();
            System.out.println("Took " + (endTime - startTime) + " msecs for " + numElements + " values");
        }

        @Override
        public void cancel() {
            running = false;
        }
    }
}