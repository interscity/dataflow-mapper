package com.nanda.csvExample;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.core.fs.FileSystem;


public class FilterProducts {

    public static void main(String[] args) throws Exception {

        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        DataSet<Tuple3<Long, String, String>> lines = env.readCsvFile(System.getProperty("user.dir") + "/products.csv")
                .ignoreFirstLine()
                .fieldDelimiter(",")  //The default is ,
                .lineDelimiter("\n")  //The default is \n
                .parseQuotedStrings('"')
                .ignoreInvalidLines()
                .types(Long.class, String.class, String.class);

        DataSet<Tuple2<String, Double>> p =
                lines.map(new MapFunction<Tuple3<Long,String,String>, Tuple2<String, Double>>() {
            @Override
            public Tuple2<String, Double> map(Tuple3<Long, String, String> csvLine) throws Exception {
                String productName = csvLine.f1;
                String[] values = csvLine.f2.split("\\|");
                Double avg = 0.0;

                for (int i = 0; i < values.length; i++)
                    avg += Double.valueOf(values[i]);
                avg /= values.length;

                return new Tuple2<>(productName, avg);
            }
        });

        /*With parallelism equals to 1 we will produce only output file*/
        p.writeAsCsv("output.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        env.execute();
    }
}