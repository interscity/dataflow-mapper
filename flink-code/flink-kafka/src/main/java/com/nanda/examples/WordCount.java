package com.nanda.examples;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.util.Collector;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.aggregation.Aggregations;

import java.io.File;

public class WordCount {

    public static void main(String[] args) throws Exception {

        final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        String filePath = new File("").getAbsolutePath();

        DataSet<String> text = env.readTextFile(filePath + "/files/text.txt");

        DataSet<Tuple2<String, Integer>> counts = text.flatMap(new LineSplitter());
        DataSet<Tuple2<String, Integer>> t2 = counts.groupBy(0).aggregate(Aggregations.SUM, 1);;

        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        counts.print();
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        t2.print();
        //The second parameter specifies the overwrite mode
        counts.writeAsText(filePath + "/files/output.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        env.execute("WordCount Example");
    }

    public static class LineSplitter implements FlatMapFunction<String, Tuple2<String, Integer>> {

        private static final long serialVersionUID = 1L;

        @Override
        public void flatMap(String value, Collector<Tuple2<String, Integer>> out) {
            String[] tokens = value.toLowerCase().split("\\W+");

            for (String token : tokens) {
                if (token.length() > 0) {
                    out.collect(new Tuple2<String, Integer>(token, 1));
                }
            }
        }
    }

}