#!/bin/bash

#Move to aimed folder
cd ~/Programas/

#Download flink - version 1.5.3
wget https://archive.apache.org/dist/flink/flink-1.5.3/flink-1.5.3-bin-hadoop28-scala_2.11.tgz

#Start Flink up
~/Programas/flink-1.5.3/bin/start-cluster.sh

#Inside package folder, do this to create the package and execute it via terminal
#mvn clean package
#~/Programas/flink-1.5.3/bin/flink run target/<JARNAME>-1.0-SNAPSHOT.jar

#Stop Flink
#~/Programas/flink-1.5.3/bin/stop-cluster.sh

