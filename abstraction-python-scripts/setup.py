#!/usr/bin/python3

import sys
import os
from lxml import etree
from xml.etree import cElementTree as ET

sys.path.insert(0, os.path.abspath(os.path.dirname(sys.argv[0]) + "/"))

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"
pom_ns = dict(pom='http://maven.apache.org/POM/4.0.0')

import Util

def usage():
    print("Usage: python3 [ARG1]")
    print("[ARG1]: processingChoice (batch or stream)")
    print("[ARG2]: packageName")
    print("[ARG3]: className")


def add_pom_transformer(path_to_pom, package_name, class_name):
    try:
        correct_class = "com.nanda." + package_name + "." + class_name

        tree = etree.parse(path_to_pom + "pom.xml")
        ET.register_namespace('',pom_ns.get('pom'))

        tree.find('pom:build/pom:plugins/pom:plugin/pom:executions/pom:execution/pom:configuration/pom:transformers/pom:transformer/', pom_ns).text = correct_class
        tree.write(path_to_pom + "pom.xml", pretty_print=True)
    except Exception as e:
        print('Failed to open or parse file: ' + str(e))


def main():

    if (len(sys.argv) < 4):
        usage()
        return
    processingChoice = sys.argv[1]
    package_name = sys.argv[2]
    class_name = sys.argv[3]

    print("Inside setup.py")
    
    if (processingChoice.lower() == "batch"):
        fname = "flink/batchSetup.java"
    elif (processingChoice.lower() == "stream"):
        fname = "flink/streamSetup.java"
    else:
        print("Invalid processingChoice argument (it should be batch or stream)")
        return

    to_import, content, udf = Util.read_java_file(fname)

    content = content.replace("$className", class_name)

    path_to_folder = "flink/out/"
    Util.write_java_file(path_to_folder + "imports.java", to_import)
    Util.write_java_file(path_to_folder + "content.java", content)

    path_to_pom = dirpath + "../generated-code/flink/" + package_name + "/"
    add_pom_transformer(path_to_pom, package_name, class_name)

if __name__ == "__main__":
    main()
