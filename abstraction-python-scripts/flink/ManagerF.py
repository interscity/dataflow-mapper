#!/usr/bin/python3

import sys
import os
import os.path as p
import re

def remove_file(filename):
    try:
        os.remove(filename)
    except OSError as e:
        #print("Could not delete " + filename)
        pass


dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

if (len(sys.argv) < 3):
    print("Usage: python3 ManagerF.py [args]")
    print("arg1: package_name")
    print("arg2: class_name")
    sys.exit(1)

package_name = sys.argv[1] 
class_name = sys.argv[2]
main_path = "../../generated-code/flink/" + package_name + "/src/main/java/com/nanda/" + package_name + "/"
print(main_path)

to_import = content = udf = ''
lines_seen = []

#Names of files to be opened
properties_file = dirpath + "../properties-files/flink/out/general.properties"
import_file = dirpath + "out/imports.java"
content_file = dirpath + "out/content.java"
parsed_file =  dirpath + "out/" + class_name + "ParsedF.java"
udf_file = dirpath + "out/udf.java"
end_file = dirpath + main_path + class_name + ".java"
end_properties_file = dirpath + properties_path + class_name + ".properties"

#Creates properties file
if (p.exists(properties_file)):
    with open(properties_file, "r") as in_file, open(end_properties_file, "w") as out_file:
        for line in in_file:
            out_file.write(line)

#Avoid repeated imports
if (p.exists(import_file)):
    with open(import_file , "r") as infile:
        for line in infile:
            if line not in lines_seen:
                lines_seen.append(line)
lines_seen.append(to_import)

if (p.exists(content_file)):
    with open(content_file , "r") as resp:
        content = resp.read()
    content = content.split("//placeholder\n")
    

print(parsed_file)
if (p.exists(parsed_file)):
    with open(parsed_file, "r") as resp:
        parsed_content = resp.readlines()

    pattern = re.compile("\\/\\*\\$placeholder(VariableDeclarationExpr|MethodCall)\\*\\/")

    i = 1 #ignores whitespaces that comes before the first placeholder
    line = 0
    while line < len(parsed_content) and i < len(content):
        """ if re.search(pattern, parsed_content[line]):
            parsed_content[line] = re.sub(pattern, content[i].strip() + " ", parsed_content[line])
            i+=1
        line+=1 """

        if re.search(pattern, parsed_content[line]):
            beforePattern = re.split(pattern, parsed_content[line])[0]
            if (beforePattern.endswith(")")):
                beforePattern += ";\n\t\t"
            afterPattern = re.split(pattern, parsed_content[line])[-1]

            #Remove ";" char if afterPattern has some content different from whitespace
            if (content[i].strip().endswith(";") and not(re.match("^[ \t\n]*$", afterPattern))):
                content[i] = content[i].strip()[:-1]
            parsed_content[line] = beforePattern + content[i].strip() + afterPattern + "\n"
            #parsed_content[line] = re.sub(pattern, content[i].strip() + " ", parsed_content[line])
            i+=1
        line+=1

    line = 0
    while line < len(parsed_content):
        if re.match("^[ \t\n]*$", parsed_content[line]):
            if (line + 1 < len(parsed_content) and re.match("^[ \t\n]*$", parsed_content[line + 1])):
                print("line is ", line)
                del parsed_content[line]
                line-=1
        line+=1
    
    
    with open(parsed_file, "w") as resp:
        parsed_content = lines_seen + parsed_content
        resp.writelines(parsed_content)

"""
with open(end_file, "w") as f:
    f.write("\n\t\tenv.execute(\"Your application\");\n\t}")
"""    

#Clean created files
#remove_file(import_file)
#remove_file(content_file)
#remove_file(properties_file)