#!/usr/bin/python3

#Join the pieces together
import sys
import os
import os.path as p
import re
import fileinput

def remove_file(filename):
    try:
        if (p.exists(filename)):
            os.remove(filename)
    except OSError as e:
        #print("Could not delete " + filename)
        pass


dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def main():
    if (len(sys.argv) < 3):
        print("Usage: python3 ManagerA.py [args]")
        print("arg1: package_name")
        print("arg2: class_name")
        sys.exit(1)

    print("Inside ManagerA.py")
    package_name = sys.argv[1]
    class_name = sys.argv[2]
    #Paths
    test_path = "../../generated-code/apex/" + package_name + "/src/test/java/com/nanda/" + package_name + "/"

    #Names of files to be opened
    #udf_file = dirpath + "out/udf.java"
    import_file = dirpath + "out/importsTest.java"
    content_file = dirpath + "out/contentTest.java"
    jdbc_setup_file = dirpath + "out/jdbcSetup.java"
    declaration_file = dirpath + "out/declarations.java"
    parsedA_file = dirpath + "out/" + class_name  + "ParsedA.java"
    test_file = dirpath + "testClass.java"
    #Name of generated files
    end_test_file = dirpath + test_path + class_name + "Test.java"

    content = ''
    jdbc_setup = ""
    lines_seen = []

    #Avoid repeated imports
    if (p.exists(import_file)):
        with open(import_file , "r") as infile:
            for line in infile:
                if line not in lines_seen:
                    lines_seen.append(line)

    if (p.exists(declaration_file) and p.exists(parsedA_file)):
        with open(declaration_file , "r") as infile:
            dec_content = infile.read()
        for line in fileinput.FileInput(parsedA_file, inplace=1):
            if "/*$placeholderDeclarations*/" in line:
                print(line.replace("/*$placeholderDeclarations*/", "\t\t" + dec_content))
            else:
                print(line, end='')


    if (p.exists(content_file)):
        with open(content_file , "r") as resp:
            content = resp.read()
        content = content.split("//placeholderKafka\n")[1:]

    if (p.exists(jdbc_setup_file)):
        with open(jdbc_setup_file , "r") as resp:
            jdbc_setup = resp.read()

        jdbc_setup = jdbc_setup.replace("//placeholder", content)

        #Generate the class with populateDAG function
        #with open(parsed_file, "w") as resp:
        #    #parsed_content = ''.join(lines_seen) + parsed_content
        #    parsed_content = lines_seen + parsed_content
        #    resp.writelines(parsed_content)


    #Generate the test class to execute the application
    with open(test_file, "r") as f:
        test_content = f.read()
    test_content = test_content.replace("$className", class_name)

    pattern = re.compile("\\/\\*\\placeholderKafka\\*\\/")
    i = 0
    while i < len(content):
        if re.search(pattern, test_content):
            test_content = pattern.sub(content[i], test_content, 1)
        i+=1

    print(test_content)

    with open(end_test_file, "w") as f:
        f.write("package com.nanda." + package_name + ";\n\n")
        if (lines_seen != []):
            lines_seen = '\n'.join(lines_seen)
            f.write(lines_seen)
        f.write(test_content)
        f.write("\n")
        f.write(jdbc_setup)
        f.write("\n}")


    #Clean temporary files
    remove_file(import_file)
    remove_file(content_file)
    remove_file(declaration_file)
    remove_file(jdbc_setup_file)

if __name__ == "__main__":
    main()
