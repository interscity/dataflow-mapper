#!/usr/bin/python3

import os
import sys

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 jdbc_setup_replace.py [databaseDriver] [databaseUrl] [query]")
    print("[databaseDriver]: database driver name")
    print("[databaseUrl]: the database url used in the connection")
    print("[query]: the query asked by the user")
    

def read_info():
    fname = dirpath + "../apex/jdbcSetup.java"
    with open(fname) as f:
        info = f.read()
    return info

def write_to_file(databaseDriver, databaseUrl, query):
    quotes = "\""
    content = read_info()
    content = content.replace("$databaseDriver", quotes + databaseDriver + quotes)
    content = content.replace("$databaseUrl", quotes + databaseUrl + quotes)
    content = content.replace("$query", quotes + query + quotes)
    
    fname = dirpath + "../apex/out/jdbcSetup.java"
    with open(fname, "w") as f:
        f.write(content)


if (len(sys.argv) < 4):
    usage()
    sys.exit(1)

databaseDriver = sys.argv[1]
databaseUrl =  sys.argv[2].lower()
query = sys.argv[3]

write_to_file(databaseDriver, databaseUrl, query)