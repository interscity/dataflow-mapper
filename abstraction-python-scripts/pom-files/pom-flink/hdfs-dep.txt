 		<!--HDFS dependency -->
         <dependency>
 	        <groupId>org.apache.flink</groupId>
 	        <artifactId>flink-shaded-hadoop2</artifactId>
 	        <version>${flink.version}</version>
 	        <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.flink</groupId>
            <artifactId>flink-connector-filesystem_${scala.binary.version}</artifactId>
            <version>${flink.version}</version>
        </dependency>

        <!--Hadoop environment variables-->
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
            <!--<version>3.0.0-M3</version>-->
            <configuration>
                <environmentVariables>
                    <!-- Make sure external hadoop environment will not affect maven building -->
                    <HADOOP_HOME />
                    <HADOOP_CONF_DIR />
                </environmentVariables>
            </configuration>
        </plugin>

        <!--Inside profile-->
        <profile>
            <dependencies>
 	            <dependency>
 	                <groupId>org.apache.flink</groupId>
 	                <artifactId>flink-shaded-hadoop2</artifactId>
 	                <version>${flink.version}</version>
 	                <scope>compile</scope>
                </dependency>
            </dependencies>
        </profile>
  	