import os
import sys
from lxml import etree
#cElementTree is a more efficient implementation
from xml.etree import cElementTree as ET


dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def add_dependency(path_to_pom, fname, dep_type):
    print("Inside add_dependency")
    dependency = ""

    try:
        fname = dirpath + fname
        path_to_pom = dirpath + path_to_pom

        with open(fname) as f:
            dependency += f.read()
        dependency = "<data>\n" + dependency + "\n</data>"
        dep = etree.fromstring(dependency)
        dep.tail = "\n\t"

        #print(etree.tostring(dep, pretty_print=True))

        #parser = etree.XMLParser(remove_blank_text=True)
        tree = etree.parse(path_to_pom + "pom.xml")

        pom_ns = dict(pom='http://maven.apache.org/POM/4.0.0')  
        ET.register_namespace('',pom_ns.get('pom'))

        #Checks if the dependency already exists
        """ for dependency_element in tree.findall('pom:dependencies/pom:dependency',pom_ns):  
            checkartifactid = dependency_element.find('pom:artifactId',pom_ns).text  
            if (dep_type in checkartifactid):
                return """

        all_deps = tree.findall('pom:dependencies/pom:dependency/pom:artifactId', pom_ns)
        
        for item in dep.findall('dependency'):
            print(item.find('artifactId',pom_ns))
            if (item.find('artifactId',pom_ns).text not in ):
                item.tail = "\n\t"
                tree.find('pom:dependencies', pom_ns).append(item)

        """ for item in dep.findall('plugin'):
            item.tail = "\n\t"
            #print(etree.tostring(tree.find('pom:build/plugins', pom_ns)))
            if (tree.find('pom:build', pom_ns) != None):
                tree.find('pom:build', pom_ns).append(item)
        
        for item in dep.findall('profile'):
            item.tail = "\n\t"
            if (tree.find('pom:profiles', pom_ns) != None):
                tree.find('pom:profiles', pom_ns).append(item) """
        
        print(path_to_pom + "pom.xml")
        tree.write(path_to_pom + "pom.xml", pretty_print=True)
    except Exception as e:
        print('Failed to open file: '+ str(e))


path = "../../generated-code/flink/novinho2/"
add_dependency(path, "pom-flink/hdfs-dep.txt", "hdfs")