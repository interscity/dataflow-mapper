#!/usr/bin/python3

import sys
import os
import shutil
import re
import Util

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 transformations.py [mapperType], [functionType], [nameClass] [dataName]")
    print("[mapperType]: flink, apex or both")
    print("[functionType]: map, flatMap, reduce, fold, filter, sort")
    print("[nameClass]: name of the class containing UDF function")
    print("[dataName]: name of the variable containing the data")

def write_content(fname):
    with open(dirpath + fname, "r") as f:
        content = f.readlines()

    i = 0
    for item in content:
        content[i] = item.replace("$dataName", data_name)
        """ Remove .java of className. Rsplit is to split only on the last '.' of the name"""
        content[i] = content[i].replace("$className", class_name.rsplit(".", 1)[0])
        i+=1

    content = "\n" + ''.join(content)
    path_to_file = "../" + folder.split("-")[1] + "/out/"
    Util.write_java_file(path_to_file + "content.java", content)



if (len(sys.argv) <= 4):
    usage()
    sys.exit(1)

global mapper_type, function_type, class_name, data_name
mapper_type = sys.argv[1].lower()
function_type = sys.argv[2].lower()
class_name = sys.argv[3]
data_name = sys.argv[4]

print("Function type")
print(function_type)

############################################
# UDF - dealing with the function calling
#for example: data.flatMap(new Tokenizer())
############################################

folder = "dt-" + mapper_type
cont = 1

#This check and cont variable are used for running twice the commands 
#when mapper_type is 'both'
if (mapper_type == "both"):
    folder = "dt-flink"

while (cont != 2):
    if (function_type == "map"):
        write_content(folder + "/map.java")
    elif (function_type == "flatmap"):
        print("It is flatmap")
        write_content(folder + "/flatMap.java")
    elif (function_type == "reduce"):
        write_content(folder + "/reduce.java")
    # elif (function_type == "fold"):
    #     split_import_udf(folder + "/fold.java")
    # elif (function_type == "filter"):
    #     split_import_udf(folder + "/filter.java")
    # elif (function_type == "sort"):
    #     split_import_udf(folder + "/sort.java")

    cont+=1
    if (mapper_type == "both" and folder != "dt-apex"):
        cont = 1
        folder = "dt-apex"

sys.exit(0)