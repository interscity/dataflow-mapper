import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;

        String[] ex = {$bootstrap_servers, $group_id};
		ParameterTool parameters = ParameterTool.fromArgs();

        DataStream<String> dataFromKafka = env
                .addSource(new FlinkKafkaConsumer011<>($topic,
                                                            new SimpleStringSchema(), prop));