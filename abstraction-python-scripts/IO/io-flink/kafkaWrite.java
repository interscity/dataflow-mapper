import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import java.util.HashMap;
import java.util.Properties;


        HashMap<String, String> config = new HashMap<>();
        config.put("bootstrap.servers", "$bootstrap_servers");
        ParameterTool params = ParameterTool.fromMap(config);
        Properties prop = params.getProperties();

        //DataStream<String> dataToKafka = env.addSource(new SimpleStringGenerator());
        dataToKafka.addSink(new FlinkKafkaProducer011<>($topic,
                                                    new SimpleStringSchema(), prop));

//UDF

    /**
     * Simple Class to generate data
     */
    public static class SimpleStringGenerator implements SourceFunction<String> {
        private static final long serialVersionUID = 119007289730474249L;
        boolean running = true;
        long i = 0;
        @Override
        public void run(SourceContext<String> ctx) throws Exception {
            while(running) {
                ctx.collect("FLINK-"+ (i++));
                Thread.sleep(10);
            }
        }
        @Override
        public void cancel() {
            running = false;
        }
    }
