import org.apache.flink.api.java.DataSet;
import org.apache.flink.core.fs.FileSystem;

        // write DataSet to a file and overwrite the file if it exists
        $dataSet.writeAsText($outputFile, FileSystem.WriteMode.OVERWRITE);