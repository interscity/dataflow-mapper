#!/usr/bin/python3

""" I need to improve the if elif statements at main """

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.dirname(sys.argv[0]) + "/"))

import Util
#import PomAddDep

folder = ""
fname = ""

def read(group_id, bootstrap_servers, topic, zookeeper):
    print("Inside Kafka read")
    to_import, content, udf = Util.read_java_file(fname)
       
    content = content.replace("$group_id", '\"' + group_id + '\"')
    content = content.replace("$bootstrap_servers", '\"' + bootstrap_servers + '\"')
    content = content.replace("$topic", '\"' + topic + '\"')

    path_to_folder = "../" + folder + "out/"
    Util.write_java_file(path_to_folder + "imports.java", to_import)
    Util.write_java_file(path_to_folder + "content.java", content)


def write(bootstrap_servers, topic, input_operator, tag_input_operator):
    print("Inside Kafka write")
    to_import, content, udf = Util.read_java_file(fname)

    content = content.replace("$bootstrap_servers", '\"' + bootstrap_servers + '\"')
    content = content.replace("$topic", '\"' + topic + '\"')
    content = content.replace("$inputOperator", input_operator)
    content = content.replace("$tagInputOperator", tag_input_operator)

    path_to_folder = "../" + folder + "out/"
    Util.write_java_file(path_to_folder + "imports.java", to_import)
    Util.write_java_file(path_to_folder + "content.java", content)
    Util.write_java_file(path_to_folder + "udf.java", udf)

def usage():
    print("Usage: python3 [action] [args]")
    print("[action]: read, readF, readA, write, writeF, writeA")
    print("[args]: groupId, bootstrap, topic, zookeeper, input_operator, tag_input_operator")


def main():

    if (len(sys.argv) <= 1):
        usage()
        return
    action = sys.argv[1]

    if (len(sys.argv) < 6):
        usage()
        return
    #This way Python knows the variables are the global ones, not local
    global folder, fname
    if (action in {"read", "readF", "readA"}):
        if (action == "read" or action == "readF"):
            fname = "io-flink/kafkaRead.java"
            folder = "flink/"
            read(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
        if (action == "read" or action == "readA"):
            fname = "io-apex/kafkaRead.java"
            folder = "apex/"
            read(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

    elif (action in {"write", "writeF", "writeA"}):
        if (action == "write" or action == "writeF"):
            fname = "io-flink/kafkaWrite.java"
            folder = "flink/"
            write(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])
        if (action == "write" or action == "writeA"):
            fname = "io-apex/kafkaWrite.java"
            folder = "apex/"
            write(sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

    else:
        print("Invalid option")

if __name__ == "__main__":
    main()
