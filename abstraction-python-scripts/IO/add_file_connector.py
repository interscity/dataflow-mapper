#!/usr/bin/python3

import sys
import os
from shutil import copyfile

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

def usage():
    print("Usage: python3 add_file_connector.py package_name action type [type2]")
    print("[package_name]: name of the package created by the abstraction user")
    print("[action]: read, readA, write or writeA")
    print("[type]: String, Integer, Double, etc.")
    print("[type2]: only in case of Key Value pair")


def copy_connector_input(fname, defined_type):
    source = dirpath + "io-apex/connectors/" + fname
    destination = rel_path + "File" + defined_type + "Input.java"
    with open(source, "r") as src, open(destination, "w") as out:
        out.write(package)
        content = ("").join(src.readlines())
        content = content.replace("$Type", defined_type)
        if (defined_type == "String"):
            content = content.replace("/*$placeholder*/", "return line;")
        elif (defined_type == "Integer"):
            content = content.replace("/*$placeholder*/",
                    "return Integer.parseInt(line);")
        else:
            content = content.replace("/*$placeholder*/",
                    "return %s.parse%s(line);" % (defined_type, defined_type))

        for line in content:
            out.write(line)

def copy_connector_output(fname, defined_type):
    source = dirpath + "io-apex/connectors/" + fname
    destination = rel_path + "File" + defined_type + "Output.java"
    with open(source, "r") as src, open(destination, "w") as out:
        out.write(package)
        content = ("").join(src.readlines())
        content = content.replace("$Type", defined_type)

        for line in content:
            out.write(line)


def copy_connector_KV_input(fname, defined_type1, defined_type2):
    source = dirpath + "io-apex/connectors/" + fname
    destination = rel_path + "File" + defined_type1 + defined_type2 + "Input.java"
    with open(source, "r") as src, open(destination, "w") as out:
        out.write(package)
        content = ("").join(src.readlines())
        content = content.replace("$Type1", defined_type1)
        content = content.replace("$Type2", defined_type2)

        if (defined_type1 == "String"):
            param1 = "parts[0]"
        elif (defined_type1 == "Integer"):
            param1 = "Integer.parseInt(parts[0])"
        else:
            param1 = "%s.parse%s(parts[0])" % (defined_type1, defined_type1)

        if (defined_type2 == "String"):
            param2 = "parts[1]"
        elif (defined_type2 == "Integer"):
            param2 = "Integer.parseInt(parts[1])"
        else:
            param2 = "%s.parse%s(parts[1])" % (defined_type2, defined_type2)

        to_replace = "return new KeyValPair<>(%s, %s)" % (param1, param2)
        content = content.replace("/*$placeholder*/", to_replace)

        for line in content:
            out.write(line)



def copy_connector_KV_output(fname, defined_type1, defined_type2):
    source = dirpath + "io-apex/connectors/" + fname
    destination = rel_path + "File" + defined_type1 + defined_type2 + "Output.java"
    with open(source, "r") as src, open(destination, "w") as out:
        out.write(package)
        content = ("").join(src.readlines())
        content = content.replace("$Type1", defined_type1)
        content = content.replace("$Type2", defined_type2)

        for line in content:
            out.write(line)



if (len(sys.argv) < 4):
    usage()
    sys.exit(1)

package_name = sys.argv[1]
action = sys.argv[2]
defined_type = sys.argv[3]

global package
package = "package com.nanda." + package_name + ";\n\n"
defined_type2 = ""

if (len(sys.argv) == 5):
    defined_type2 = sys.argv[4]

global rel_path
rel_path = dirpath + "../../generated-code/apex/" + package_name + "/src/main/java/com/nanda/" + package_name + "/"

print("Inside AddConnector")
print("Type is ", defined_type)
print("Type2 is ", defined_type2)

if (action in {"read", "readA"}):
    if (defined_type2 == ""):
        copy_connector_input("FileGenericInput.java", defined_type)
    else:
        copy_connector_KV_input("FileKVGenericInput.java", defined_type, defined_type2)

elif (action in {"write", "writeA"}):
    if (defined_type2 == ""):
        copy_connector_output("FileGenericOutput.java", defined_type)
    else:
        copy_connector_KV_output("FileKVGenericOutput.java", defined_type, defined_type2)

else:
    print("Invalid action given.")
