#!/usr/bin/python3

import sys
import os

sys.path.insert(0, os.path.abspath(os.path.dirname(sys.argv[0]) + "/"))

import Util

folder = ""
fname = ""

def read(caller, file_dir, file_name):
    print("Inside read function")
    to_import, content, udf = Util.read_java_file(fname)
    input_file = file_dir + file_name
    content = content.replace("$dataSet", caller)
    content = content.replace("$inputFile", '"' + input_file + '"')

    path_to_folder = "../" + folder + "out/"
    Util.write_java_file(path_to_folder + "imports.java", to_import)
    Util.write_java_file(path_to_folder + "content.java", content)

def write(data_set_name, file_dir, file_name, input_operator, tag_input_operator):
    print("Inside write function")
    to_import, content, udf = Util.read_java_file(fname)

    output_file = file_dir + file_name
    content = content.replace("$dataSet", data_set_name)
    content = content.replace("$outputFile", '"' + output_file + '"')
    content = content.replace("$inputOperator", input_operator)
    content = content.replace("$tagInputOperator", tag_input_operator)

    path_to_folder = "../" + folder + "out/"
    Util.write_java_file(path_to_folder + "imports.java", to_import)
    Util.write_java_file(path_to_folder + "content.java", content)


def usage():
    print("Usage: python3 [caller] [action] [args]")
    print("[caller]: name of the instance calling the method")
    print("[action]: read, readF, readA, write, writeF or writeA")
    print("[args]: fileDirName fileName")
    

def main():

    if (len(sys.argv) < 5):
        usage()
        return

    caller = sys.argv[1]
    action = sys.argv[2]

    #This way Python knows the variables are the global ones, not local
    global folder, fname

    if (action in {"read", "readF", "readA"}):
        if (action == "read" or action == "readF"):
            fname = "io-flink/fileTextRead.java"
            folder = "flink/"
            read(caller, sys.argv[3], sys.argv[4]) #this is inside 'if' because both needs to run twice
        if (action == "read" or action == "readA"):
            fname = "io-apex/fileTextRead.java"
            folder = "apex/"
            read(caller, sys.argv[3], sys.argv[4])

    elif (action in {"write", "writeF", "writeA"}):
        if (len(sys.argv) < 7):
            print("Unsufficient number of arguments")
            usage()
            return

        if (action == "write" or action == "writeF"):
            fname = "io-flink/fileTextWrite.java"
            folder = "flink/"
            write(caller, sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
        if (action == "write" or action == "writeA"):
            fname = "io-apex/fileTextWrite.java"
            folder = "apex/"
            write(caller, sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6])
            #copyfile(connectors/FileInputOperator.java, ../../../generated-code/flink/$className/src/main/java/com/nanda/$className/FileOutputOperator.java)

    else:
        print("Invalid option")

if __name__ == "__main__":
    main()
