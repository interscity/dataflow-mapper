/*The code below is based on Apache Apex fileIO-simple
* and FileIO examples: https://github.com/dtpublic/examples
* */

import javax.validation.constraints.NotNull;

import com.datatorrent.api.Context;
import com.datatorrent.api.DefaultOutputPort;
import com.datatorrent.api.annotation.OutputPortFieldAnnotation;
import com.datatorrent.lib.io.fs.AbstractFileOutputOperator;
import com.datatorrent.lib.util.KeyValPair;

/*** Write incoming lines to output file */
public class File$Type1$Type2Output extends AbstractFileOutputOperator<KeyValPair<$Type1, $Type2>> {
    private static final String CHARSET_NAME = "UTF-8";
    private static final String NL = System.lineSeparator();

    /*
     * @OutputPortFieldAnnotation(optional = false) public final transient
     * DefaultOutputPort<T> output = new DefaultOutputPort<>();
     */

    @NotNull
    private String fileName;
    private transient String fName; // per partition file name

    @Override
    public void setup(Context.OperatorContext context) {
        // create file name for this partition by appending the operator id to
        // the base name
        long id = context.getId();
        fName = fileName + "_p" + id;
        super.setup(context);
    }

    @Override
    protected String getFileName(KeyValPair<$Type1, $Type2> tuple) {
        return fName;
    }

    @Override
    protected byte[] getBytesForTuple(KeyValPair<$Type1, $Type2> line) {
        byte result[] = null;
        try {
            result = (line + NL).getBytes(CHARSET_NAME);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String v) {
        fileName = v;
    }
}
