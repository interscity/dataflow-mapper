import com.datatorrent.contrib.kafka.KafkaSinglePortOutputOperator;
import com.datatorrent.api.DAG;

        KafkaSinglePortOutputOperator<String,String> kafkaOut = new KafkaSinglePortOutputOperator<String,String>();

        dag.addStream("data", $inputOperator.kafkaOutput, kafkaOut.$tagInputOperator);