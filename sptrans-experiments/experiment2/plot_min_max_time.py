#Save figure to file since it is being executed inside a docker container

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import collections

N = 3
datasets = np.arange(N)
colors = ['b', 'r']

months = ["Abril", "Maio", "Junho"]
lines = ['1450', '1465', '1651', '198', '32772', '34694']

#Previous
# max_time = {
#     "Abril": [20.642857142857142, 15.0, 3.0, 16.09090909090909, 10.454545454545455, 10.545454545454545],
#     "Maio": [20.642857142857142, 14.147058823529411, 3.25, 16.272727272727273, 10.454545454545455, 11.0],
#     "Junho" : [19.142857142857142, 14.235294117647058, 3.25, 16.818181818181817, 10.545454545454545, 11.0]
#     }

# min_time = {
#     "Abril": [4.928571428571429, 3.0588235294117645, 1.5, 5.2727272727272725, 2.8181818181818183, 3.5454545454545454],
#     "Maio": [2.7142857142857144, 2.735294117647059, 1.25, 5.0, 2.3636363636363638, 2.0],
#     "Junho" : [5.071428571428571, 3.411764705882353, 1.25, 5.090909090909091, 2.727272727272727, 3.272727272727273]
#     }
max_time =  {
    "Abril" : [29.0, 29.0, 4.0, 29.0, 29.0, 29.0],
    "Maio" : [29.0, 29.0, 7.0, 29.0, 29.0, 29.0],
    "Junho" : [29.0, 29.0, 5.0, 29.0, 29.0, 29.0],
}

min_time =  {
    "Abril": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    "Maio": [1.0, 1.0, 1.0, 1.0, 1.0, 1.0],
    "Junho" : [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
}

avg_time = {
    "Abril": [7.351813768457959, 6.408740606419891, 1.824019285557747, 3.282099816097351, 3.149216539257507, 4.965564956116075],
    "Maio": [7.429436233893605, 6.430885818643799, 1.7825938099150918, 3.1078149140027054, 3.1797874626712472, 5.012821208732752],
    "Junho": [7.828092133191512, 6.637271104448318, 1.7616919393455706, 3.0329554167628205, 3.213687130606096, 5.0225135675886445]
    #"Abril": [9.175900712759148, 9.242782425388503, 2.1922629743156117, 3.9041652662110518, 4.139912997871225, 6.2327583208708655],
    #"Maio": [9.207178729548104, 9.22585182719266, 2.1451330093682683, 3.6323898394032517, 4.111143666820798, 6.20342359209702],
    #"Junho": [9.26108283415856, 9.314996780853852, 2.1269335222576453, 3.54380751890905, 4.186422565460583, 6.324696863285976]
}

max_line_1651 = [4, 7, 5]
min_line_1651 = [1, 1, 1]
avg_line_1651 = [1.82, 1.78, 1.76]

#for month in months:
#    plt.ylabel('Tempo (minutos)')
#    plt.xlabel('Linhas de ônibus')
#    plt.grid(True)
#    title = "Tempos máximo e mínimo - " + month + " de 2019"
#    plt.title(title)
#
#    fig = plt.figure(1, figsize=(7, 2.5))
#
#    #plt.ylim(7, 9)
#    plt.bar(datasets, max_time[month], alpha=0.9, color='b', label='Máximos')
#    plt.bar(datasets, avg_time[month], alpha=0.9, color='m', label='Tempo médio')
#    plt.bar(datasets, min_time[month], alpha=0.9, color='r', label='Mínimos')
#    
#    plt.xticks(datasets, ('1450', '1465', '1651', '198', '32772', '34694'))
#
#    fig_title = 'min_max_chart_' + month.lower() + '.pdf'   
#    plt.legend()                    
#    plt.savefig(fig_title, format='pdf')
#    plt.close()
#

plt.ylabel('Tempo (minutos)')
plt.xlabel('Meses de 2019')
plt.grid(True)
title = "Tempos máximo, médio e mínimo de espera entre pontos"
plt.title(title)

fig = plt.figure(1, figsize=(7, 2.5))

plt.bar(datasets, max_line_1651, alpha=0.9, color='b', label='Máximos')
plt.bar(datasets, avg_line_1651, alpha=0.9, color='m', label='Tempo médio')
plt.bar(datasets, min_line_1651, alpha=0.9, color='r', label='Mínimos')


plt.xticks(datasets, ('Abril', 'Maio', 'Junho'))

fig_title = 'min_max_chart_line_1651.pdf'   

plt.legend()                    
plt.savefig(fig_title, format='pdf')
plt.close()
