#!/usr/bin/python3

import os
import sys
import math
import collections

fnames = ["max_april.txt", "max_may.txt", "max_june.txt", "min_april.txt", "min_may.txt", "min_june.txt"]


def calculate_avg(fname):
    elements = {}
    el_sum = {}
    el_cont = {}
    el_avg = {}

    print(name)
    
    with open(fname, "r") as infile:
        for line in infile:
            content = line.split(",")
            if (len(content) >= 2):
                key = content[0].strip()
                if ("-" in key):
                    key = key.split("-")[0].strip()
                value = content[1].strip()
                time_diff = float(value)
                if (time_diff < 100 and time_diff > 0):
                    if (key in elements):
                        elements[key].append(time_diff)
                        el_sum[key] += time_diff
                        el_cont[key] += 1
                    else:
                        elements[key] = [time_diff]
                        el_sum[key] = time_diff
                        el_cont[key] = 1

    for key in el_sum:
        el_avg[key] = el_sum[key] / el_cont[key]


    el_avg = collections.OrderedDict(sorted(el_avg.items()))
    print(el_avg)
    print("\n")



def calculate_min_max(fname):

    elements = {}
    el_max = {}
    el_min = {}
        
    with open(fname, "r") as infile:
        for line in infile:
            content = line.split(",")
            if (len(content) >= 2):
                key = content[0].strip()
                if ("-" in key):
                    key = key.split("-")[0].strip()
                value = content[1].strip()
                time_diff = float(value)
                if (time_diff < 30 and time_diff > 1):
                    if (key in elements):
                        elements[key].append(time_diff)
                        if (time_diff > el_max[key]):
                            el_max[key] = time_diff
                        if (time_diff < el_min[key]):    
                            el_min[key] = time_diff
                    else:
                        elements[key] = [time_diff]
                        el_max[key] = time_diff
                        el_min[key] = time_diff
    print("Max:")
    print(el_max)
    print("Min:")
    print(el_min)
    print("=====================================")



print("This program calculates the average of given values")
for fname in fnames:
    name = "data/" + fname
    calculate_avg(name)
    calculate_min_max(name)