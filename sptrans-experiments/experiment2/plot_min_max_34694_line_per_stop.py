#Save figure to file since it is being executed inside a docker container

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import collections

N = 11
datasets = np.arange(N)

months = ["Abril", "Maio", "Junho"]


#Line 34694
max_time =  {
    "Abril" : [3.0, 3.0, 4.0, 26.0, 2.0, 4.0, 30.0, 14.0, 4.0, 22.0, 4.0],
    "Maio" : [3.0, 3.0, 4.0, 26.0, 2.0, 4.0, 30.0, 19.0, 4.0, 22.0, 4.0],
    "Junho" : [3.0, 2.0, 4.0, 26.0, 2.0, 4.0, 30.0, 20.0, 4.0, 22.0, 4.0]
}


min_time =  {
    "Abril": [1.0, 1.0, 1.0, 9.0, 1.0, 1.0, 10.0, 6.0, 1.0, 7.0, 1.0],
    "Maio": [1.0, 1.0, 1.0, 5.0, 1.0, 1.0, 5.0, 3.0, 1.0, 2.0, 1.0],
    "Junho" : [1.0, 1.0, 1.0, 8.0, 1.0, 1.0, 9.0, 6.0, 1.0, 6.0, 1.0]
}


for month in months:

    plt.ylabel('Tempo (minutos)')
    plt.xlabel('Conjunto de pontos consecutivos')
    plt.grid(True)
    title = "Tempos máximo e mínimo de espera entre pontos - " + month.lower() + " de 2019"
    plt.title(title)

    fig = plt.figure(1, figsize=(7, 2.5))

    plt.bar(datasets, max_time[month], alpha=0.9, color='b', label='Máximos')
    plt.bar(datasets, min_time[month], alpha=0.9, color='r', label='Mínimos')


    plt.xticks(datasets, ('1-2', '2-3', '3-4', '4-5', '5-6', '6-7', '7-8', '8-9', '9-10', '10-11', '11-12'))

    fig_title = 'min_max_chart_' + month + '.pdf'   

    plt.legend()                    
    plt.savefig(fig_title, format='pdf')
    plt.close()
