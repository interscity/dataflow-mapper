#!/usr/bin/python3

bus_lines = ['1450', '1465', '1651', '198', '32772', '34694']

def read_content(fname):
    line_stops = {}
    info_days = {}
    time_quantity = {}
    for bus_line in bus_lines:
        line_stops[bus_line] = {}
        info_days[bus_line] = {}
        time_quantity[bus_line] = {}

    with open(fname, "r") as infile:
        for line in infile:
            line = line.strip()
            content = line.split(" ")
            if (len(content) >= 2):
                line_stop = content[0].strip()
                if ("-" in line_stop):
                    bus_line = line_stop.split("-")[0].strip()
                    stop = line_stop.split("-")[1].strip()
                    day = line_stop.split("-")[3].strip()

                    value = content[1].strip()
                    if ("=" in value):
                        time = value.split("=")[0].strip()
                        quantity = int(value.split("=")[1].strip())


                        if (time in time_quantity[bus_line]):
                            time_quantity[bus_line][time] += quantity
                            if (stop not in line_stops[bus_line][time]):
                                line_stops[bus_line][time].append(stop)
                            if (day not in info_days[bus_line][time]):
                                info_days[bus_line][time].append(day)
                        else: #First time need to say the value is an array
                            time_quantity[bus_line][time] = quantity
                            line_stops[bus_line][time] = [stop]
                            info_days[bus_line][time] = [day]
    
    for bus_line in bus_lines:
        for time in time_quantity[bus_line]:
            time_quantity[bus_line][time] /= len(line_stops[bus_line][time])
            time_quantity[bus_line][time] /= len(info_days[bus_line][time])

    print(time_quantity)
    
    


#fnames = ["data/output_reduce_flink_july3_formatted.txt", 
#"data/output_reduce_flink_august20_formatted.txt", 
#"data/output_reduce_flink_september23_formatted.txt"]
fnames = ["data/output_reduce_month_july.txt",
"data/output_reduce_month_august.txt",
"data/output_reduce_month_september.txt"]

for fname in fnames:
    print(fname)
    read_content(fname)

