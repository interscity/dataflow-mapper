#Save figure to file since it is being executed inside a docker container

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt
import collections

plt.ylabel('Tempo para execução do experimento (em segundos)')
plt.xlabel('Conjunto de dados utilizados')
plt.grid(True)
plt.title('Intervalo de confiança para dados de abril, maio e junho')

N = 3
datasets = np.arange(N)
width = 0.35

fig = plt.figure(1, figsize=(7, 2.5))

#April, May and June, respectively
apex_means = np.array([43.27, 59.00, 49.57])
#apex_std_dev = ['1': 2.64, '2': 2.42, '3': 2.51]
#lb = [42.0284609638238, 57.8619225501718, 48.3895973557567]
#ub = [44.5115390361762, 60.1380774498282, 50.7504026442433]
yerr1 = [1.241539036, 1.13807745, 1.180402644]
plt.bar(datasets, apex_means, yerr=yerr1, width=width, label='Apex')

flink_means = np.array([22.90, 41.37, 36.40])
#flink_std_dev = ['1': 01.03, '2': 01.38, '3': 01.10]
#lb = [22.4156116639161, 40.7210136856352, 35.8826920682599]
#ub = [23.3843883360839, 42.0189863143648, 36.9173079317401]
r2 = [x + width for x in datasets]
yerr2 = [0.484388336, 0.648986314, 0.517307932]
plt.bar(r2,flink_means, yerr=yerr2, width=width, label='Flink')


plt.xticks(datasets + width/2, ('Abril', 'Maio', 'Junho'))
plt.legend()                                  
plt.savefig('confidence_interval_chart.pdf', format='pdf')
plt.close()
