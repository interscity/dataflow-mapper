#par(mfrow=c(2,1)) # to stack the charts on column


#Line 1450
mean = c(8.32068665762172, 8.356397430363355, 8.652139186799692)
lowerlimit = c(8.25, 8.31, 8.60)
upperlimit = c(8.39, 8.40, 8.70)


#Line 1465
mean = c(8.25274367202988, 8.252341743646747, 8.439851987901275)
lowerlimit = c(8.21, 8.22, 8.41)
upperlimit = c(8.30, 8.28, 8.47)


#Line 1651
mean = c(1.824019285557747, 1.7825938099150918, 1.7616919393455706)
lowerlimit = c(1.81, 1.77, 1.75)
upperlimit = c(1.84, 1.79, 1.77)

#Line 198
mean = c(4.020659473399131, 3.848878812641185, 3.779591727611006)
lowerlimit = c(3.98, 3.83, 3.75)
upperlimit = c(4.05, 3.87, 3.80)

#Line 32772
mean = c(3.855326341934594, 3.8848632505501413, 3.918607045724065)
lowerlimit = c(3.81, 3.86, 3.89)
upperlimit = c(3.89, 3.91, 3.95)


#Line 34694
mean = c(4.987437285189428, 5.048703921579259, 5.114771235164387)
lowerlimit = c(4.94, 5.02, 5.08)
upperlimit = c(5.04, 5.08, 5.15)


df = data.frame(cbind(upperlimit,lowerlimit,mean))

months <- c('Abril', 'Maio', 'Junho')
plot(df$mean, main="IC do tempo médio entre paradas da linha X", ylim = c(4,6), ylab="Tempo (minutos)", xlab="Meses da coleta")
axis(1, at=1:3, labels=months[1:3])

#install.packages("plotrix")
require(plotrix)
plotCI(df$mean,y=NULL, uiw=df$upperlimit-df$mean, liw=df$mean-df$lowerlimit, err="y",      pch=20, slty=3, scol = "black", add=TRUE)


rm(upperlimit,lowerlimit,mean,df) #remove the objects stored from workspace

