package com.nanda.twoOutputs;

import com.datatorrent.api.Context;
import com.datatorrent.api.DefaultOutputPort;
import com.datatorrent.api.InputOperator;
import com.datatorrent.common.util.BaseOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a simple operator that emits random number, based in Apache Apex
repository random generator file.
 */
public class RandomNumberGenerator extends BaseOperator implements InputOperator {

    private int numTuples = 10;
    private transient int count = 0;
    private transient long id;      // operator id

    private static final Logger LOG = LoggerFactory.getLogger(RandomNumberGenerator.class);

    public final transient DefaultOutputPort<Double> out = new DefaultOutputPort<Double>();
    public final transient DefaultOutputPort<Double> out2 = new DefaultOutputPort<Double>();

    @Override
    public void setup(Context.OperatorContext context) {
        super.setup(context);
        id = context.getId();
        LOG.debug("Leaving setup, id = {}", id);
    }

    @Override
    public void beginWindow(long windowId)
    {
        count = 0;
    }

    @Override
    public void emitTuples() {
        if (count++ < numTuples) {
            out.emit(Math.random());
            out2.emit(Math.random());
        }
    }

    public int getNumTuples()
    {
        return numTuples;
    }

    /**
     * Sets the number of tuples to be emitted every window.
     * @param numTuples number of tuples
     */
    public void setNumTuples(int numTuples)
    {
        this.numTuples = numTuples;
    }
}
