package com.nanda.twoOutputs;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.validation.ConstraintViolationException;

import com.datatorrent.api.Attribute;
import jline.internal.Log;
import org.apache.apex.api.EmbeddedAppLauncher;
import org.apache.apex.api.Launcher;
import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.datatorrent.api.LocalMode;


public class ApplicationTest {

    private static final String baseDirName   = "target/fileOutput";
    private static final String outputDirName = baseDirName + "/output-dir";
    private static final File outputDirFile   = new File(outputDirName);

    private void cleanup() {
        try {
            FileUtils.deleteDirectory(outputDirFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // check that the requisite number of files exist in the output directory
    private boolean check() {
        // Look for files with a single digit extension
        String[] ext = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        Collection<File> list = FileUtils.listFiles(outputDirFile, ext, false);

        return ! list.isEmpty();
    }

    // return Configuration with suitable properties set
    private Configuration getConfig() {
        final Configuration result = new Configuration(false);
        result.addResource(this.getClass().getResourceAsStream("/META-INF/properties.xml"));
        //result.setInt("dt.application.fileOutput.dt.operator.randomGenerator.prop.divisor", 3);
        return result;
    }

    @Before
    public void beforeTest() {
        cleanup();
        try {
            FileUtils.forceMkdir(outputDirFile);
        } catch (IOException e) {
            Log.error("Error on directory creation.", e);
        }
    }

    @After
    public void afterTest() {
        cleanup();
    }

    @Test
    public void testApplication() throws Exception {
        try {
            EmbeddedAppLauncher<?> launcher = Launcher.getLauncher(Launcher.LaunchMode.EMBEDDED);
            Attribute.AttributeMap launchAttributes = new Attribute.AttributeMap.DefaultAttributeMap();
            launchAttributes.put(EmbeddedAppLauncher.RUN_ASYNC, true);
            Launcher.AppHandle appHandle = launcher
                    .launchApp(new Application(), getConfig(), launchAttributes);

            // wait for output files to show up
            while ( ! check() ) {
                System.out.println("Sleeping ....");
                Thread.sleep(1000);
            }

            System.out.println("Finishing ....");
            appHandle.shutdown(Launcher.ShutdownMode.KILL);
        } catch (ConstraintViolationException e) {
            Assert.fail("constraint violations: " + e.getConstraintViolations());
        }
    }

}

