package com.nanda.file2file;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.validation.constraints.NotNull;

import com.datatorrent.lib.io.fs.AbstractFileOutputOperator;

/**
 * Converts each tuple to a string and writes it as a new line to the output file
 */
public class LineOutputOperator<T> extends AbstractFileOutputOperator<T> {

    private static final String NL = System.lineSeparator();
    private static final Charset CS = StandardCharsets.UTF_8;

    //setting default value
    String lineDelimiter = "\n";

    @NotNull
    private String baseName;

    @Override
    protected byte[] getBytesForTuple(T tuple) {
        String temp = tuple.toString().concat(String.valueOf(lineDelimiter));
        byte[] theByteArray = temp.getBytes();

        return theByteArray;
    }

    @Override
    protected void processTuple(T tuple) { super.processTuple(tuple); }

    @Override
    protected String getFileName(T tuple) {
        return baseName;
    }

    public String getBaseName() { return baseName; }
    public void setBaseName(String v) { baseName = v; }
}
