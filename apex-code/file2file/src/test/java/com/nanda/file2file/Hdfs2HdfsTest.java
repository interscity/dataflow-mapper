package com.nanda.file2file;

import com.datatorrent.api.Attribute;
import org.apache.apex.api.EmbeddedAppLauncher;
import org.apache.apex.api.Launcher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static java.lang.System.exit;
import static org.junit.Assert.assertTrue;

public class Hdfs2HdfsTest {

    private static final Logger LOG = LoggerFactory.getLogger(Hdfs2HdfsTest.class);

    private static final String baseDirName   = "target/Hdfs2Hdfs";
    private static final String inputDirName  = baseDirName + "/input-dir";
    private static final String outputDirName = baseDirName + "/output-dir";

    private static final File inputDirFile    = new File(inputDirName);
    private static final File outputDirFile   = new File(outputDirName);

    private static final int numFiles      = 1;    // number of input files
    private static final int numLines      = 5;    // number of lines in each input file

    private static final String FILE_PATH = outputDirName + "/file.0";     // first part

    // create nFiles files with nLines lines in each
    private void createFiles(final int nFiles, final int nLines) throws IOException {
        for (int file = 0; file < nFiles; file++) {
            ArrayList<String> lines = new ArrayList<>();
            for (int line = 0; line < nLines; line++) {
                lines.add("file " + file + ", line " + line);
            }
            try {
                FileUtils.write(new File(inputDirFile, "file" + file),
                                        StringUtils.join(lines, "\n"));
            } catch (IOException e) {
                System.out.format("Error: Failed to create file %s%n", file);
                e.printStackTrace();
            }
        }
        System.out.format("Created %d files with %d lines in each%n", nFiles, nLines);
    }

    private void cleanup() {
        try {
            FileUtils.deleteDirectory(outputDirFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    // check that the requisite number of files exist in the output directory
    private static void chkOutput() throws Exception {
        File file = new File(FILE_PATH);
        /*String[] ext = {"0"};
        Collection<File> list = FileUtils.listFiles(outputDirFile, ext, false);*/

        final int MAX = 60;
        for (int i = 0; i < MAX && (! file.exists()); ++i ) {
        //for (int i = 0; i < MAX && list.isEmpty(); ++i ) {
            LOG.debug("Sleeping, i = {}", i);
            Thread.sleep(1000);
        }
        if (! file.exists()) {
            String msg = String.format("Error: %s not found after %d seconds%n", FILE_PATH, MAX);
            throw new RuntimeException(msg);
        }
    }

    private static void compare() throws Exception {
        // read output file
        File file = new File(FILE_PATH);
        BufferedReader br = new BufferedReader(new FileReader(file));
        ArrayList<String> list = new ArrayList<>();
        String line;
        while (null != (line = br.readLine())) {
            list.add(line);
        }
        br.close();

        // compare
        Assert.assertEquals("number of lines", list.size(), numLines);
        for (int i = 0; i < numLines; ++i) {
            String content = "file 0, line " + i;
            assertTrue("line", content.equals(list.get(i)));
        }
    }

    // return Configuration with suitable properties set
    private Configuration getConfig() {
        final Configuration conf = new Configuration(false);
        conf.addResource(this.getClass().getResourceAsStream("/META-INF/properties-HDFS.xml"));

        return conf;
    }

    @Before
    public void beforeTest() throws Exception {
        cleanup();
        FileUtils.forceMkdir(inputDirFile);
        FileUtils.forceMkdir(outputDirFile);

        createFiles(numFiles, numLines);
    }

    @After
    public void afterTest() {
        cleanup();
    }

    @Test
    public void testApplication() throws Exception {
        try {
            EmbeddedAppLauncher<?> launcher = Launcher.getLauncher(Launcher.LaunchMode.EMBEDDED);
            Attribute.AttributeMap launchAttributes = new Attribute.AttributeMap.DefaultAttributeMap();
            launchAttributes.put(EmbeddedAppLauncher.RUN_ASYNC, true);
            Launcher.AppHandle appHandle = launcher
                    .launchApp(new Hdfs2Hdfs(), getConfig(), launchAttributes);

            // check for presence of output file
            chkOutput();

            // compare output lines to input
            compare();

            appHandle.shutdown(Launcher.ShutdownMode.KILL);
        } catch (ConstraintViolationException e) {
            Assert.fail("constraint violations: " + e.getConstraintViolations());
        }

    }
}
