#If appears a warning about malformed project, just add a version to
#javadoc, for example <version>2.9</version> 
if [ "$#" -ne 1 ]; then
   echo "USAGE: ./generate_apex_project.sh ARG1"
   echo "ARG1: name of the package"
   exit 1;
fi

name=$1

mvn archetype:generate \
-DarchetypeGroupId=org.apache.apex \
-DarchetypeArtifactId=apex-app-archetype -DarchetypeVersion=3.7.0 \
-DgroupId=com.nanda -Dpackage=com.nanda.$name -DartifactId=$name \
-Dversion=1.0-SNAPSHOT \
-DinteractiveMode=false