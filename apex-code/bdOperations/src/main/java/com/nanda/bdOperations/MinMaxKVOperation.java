package com.nanda.bdOperations;

import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import com.datatorrent.api.Context.OperatorContext;
import com.datatorrent.lib.math.Max;
import com.datatorrent.lib.math.MaxKeyVal;
import com.datatorrent.lib.math.Min;
import com.datatorrent.lib.math.MinKeyVal;
import org.apache.hadoop.conf.Configuration;


public class MinMaxKVOperation implements StreamingApplication {
    @SuppressWarnings("unchecked")
    @Override
    public void populateDAG(DAG dag, Configuration conf) {
        dag.setAttribute(DAG.APPLICATION_NAME, "AverageSampleApplication");

        FileKVInputOperator fileIn = dag.addOperator("fileInput", new FileKVInputOperator());
        fileIn.setDirectory("/tmp/teste2.txt");

        /*Min*/
        MinKeyVal<String, Double> min = dag.addOperator("min", MinKeyVal.class);
        dag.addStream("min", fileIn.output, min.data);
        dag.getMeta(min).getAttributes()
                .put(OperatorContext.APPLICATION_WINDOW_COUNT, 5);

        ConsoleOutputOperator console = dag.addOperator("console", new ConsoleOutputOperator());
        dag.addStream("min_out", min.min, console.input);
        /******************************************************/

        FileKVInputOperator fileIn2 = dag.addOperator("fileInput2", new FileKVInputOperator());
        fileIn2.setDirectory("/tmp/teste2.txt");

        /*Max*/
        MaxKeyVal<String, Double> max = dag.addOperator("max", MaxKeyVal.class);
        dag.addStream("max", fileIn2.output, max.data);
        dag.getMeta(max).getAttributes()
                .put(OperatorContext.APPLICATION_WINDOW_COUNT, 5);

        ConsoleOutputOperator console2 = dag.addOperator("console2", new ConsoleOutputOperator());
        dag.addStream("max_out", max.max, console2.input);

    }

}
