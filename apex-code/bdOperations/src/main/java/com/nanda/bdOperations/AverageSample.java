package com.nanda.bdOperations;

import com.datatorrent.api.Attribute;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.Context.OperatorContext;
import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import com.datatorrent.lib.math.Average;
import com.datatorrent.lib.testbench.RandomEventGenerator;

    /**
     * This sample application code for showing sample usage of malhar operator(s). <br>
     * It was based on source:
     * https://github.com/amberarrow/Malhar/blob/dt-dev/samples/
     */

public class AverageSample implements StreamingApplication {
    @SuppressWarnings("unchecked")
    @Override
    public void populateDAG(DAG dag, Configuration conf) {
        dag.setAttribute(DAG.APPLICATION_NAME, "AverageSampleApplication");

        FileInputOperator fileIn = dag.addOperator("fileInput", new FileInputOperator());
        fileIn.setDirectory("/tmp/teste.txt");


        /*Average*/
        Average<Double> average = dag.addOperator("average", Average.class);
        dag.addStream("avg", fileIn.output, average.data);
        dag.getMeta(average).getAttributes()
                .put(OperatorContext.APPLICATION_WINDOW_COUNT, 5);
        dag.getMeta(average).getAttributes()
                .put(OperatorContext.LOCALITY_HOST , "localhost");



        ConsoleOutputOperator console = dag.addOperator("console", new ConsoleOutputOperator());
        dag.addStream("avg_out", average.average, console.input);

    }

}
