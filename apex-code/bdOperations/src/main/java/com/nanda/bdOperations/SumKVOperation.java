package com.nanda.bdOperations;

import com.datatorrent.api.Context;
import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.lib.io.ConsoleOutputOperator;
import com.datatorrent.lib.math.SumKeyVal;
import org.apache.hadoop.conf.Configuration;

public class SumKVOperation implements StreamingApplication {

    @SuppressWarnings("unchecked")
    @Override
    public void populateDAG(DAG dag, Configuration conf) {
        dag.setAttribute(DAG.APPLICATION_NAME, "AverageSampleApplication");

        FileKVInputOperator fileIn = dag.addOperator("fileInput", new FileKVInputOperator());
        fileIn.setDirectory("/tmp/teste2.txt");

        /*Sum*/
        SumKeyVal<String, Double> sum = dag.addOperator("sum", SumKeyVal.class);
        dag.addStream("sum", fileIn.output, sum.data);
        dag.getMeta(sum).getAttributes()
                .put(Context.OperatorContext.APPLICATION_WINDOW_COUNT, 5);

        ConsoleOutputOperator console = dag.addOperator("console", new ConsoleOutputOperator());
        dag.addStream("sum_out", sum.sum, console.input);

    }
}
