package com.nanda.bdOperations;

import com.datatorrent.api.Context;
import com.datatorrent.api.DefaultOutputPort;
import com.datatorrent.api.annotation.OutputPortFieldAnnotation;
import com.datatorrent.lib.io.fs.AbstractFileInputOperator;
import com.datatorrent.lib.util.KeyValPair;
import org.apache.hadoop.fs.Path;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileKVInputOperator extends AbstractFileInputOperator<KeyValPair<String, Double>> {
    private static final Logger LOG = LoggerFactory.getLogger(FileInputOperator.class);

    /*** output port for file data*/
    @OutputPortFieldAnnotation(optional = false)
    public final transient DefaultOutputPort<KeyValPair<String, Double>> output  = new DefaultOutputPort<>();

    private transient BufferedReader br = null;

    // Path is not serializable so convert to/from string for persistance
    private transient Path filePath;
    private String filePathStr;

    // set to true when end-of-file occurs, to prevent emission of additional tuples in current window
    private boolean stop;

    // pause for this many milliseconds after end-of-file
    private transient int pauseTime;

    @Override
    public void setup(Context.OperatorContext context) {
        super.setup(context);

        pauseTime = context.getValue(Context.OperatorContext.SPIN_MILLIS);

        if (null != filePathStr) {      // restarting from checkpoint
            filePath = new Path(filePathStr);
        }
    }

    @Override
    public void endWindow() {
        super.endWindow();
        stop = false;
    }

    @Override
    public void emitTuples() {
        if ( ! stop ) {        // normal processing
            super.emitTuples();
            return;
        }

        // end-of-file, so emit no further tuples till next window;
        try {
            Thread.sleep(pauseTime);
        } catch (InterruptedException e) {
            LOG.info("Sleep interrupted");
        }
    }

    @Override
    protected InputStream openFile(Path curPath) throws IOException {
        LOG.debug("openFile: curPath = {}", curPath);
        filePath = curPath;
        filePathStr = filePath.toString();

        InputStream is = super.openFile(filePath);
        br = new BufferedReader(new InputStreamReader(is));
        return is;
    }

    @Override
    protected void closeFile(InputStream is) throws IOException {
        LOG.debug("closeFile: filePath = {}", filePath);
        super.closeFile(is);

        br.close();
        br = null;
        filePath = null;
        filePathStr = null;
        stop = true;
    }

    @Override
    protected KeyValPair<String, Double> readEntity() throws IOException {
        // try to read a line
        String line = br.readLine();
        while (null != line) {  // normal case
            LOG.debug("readEntity: line = {}", line);

            String[] parts = line.split(",");
            if (parts.length == 2) {
                //System.out.println(parts[0] + "-> " + parts[1]);
                return new KeyValPair(parts[0], Double.parseDouble(parts[1]));
            }

            line = br.readLine();
        }

        // end-of-file (control tuple sent in closeFile()
        LOG.info("readEntity: EOF for {}", filePath);
        return null;
    }


    @Override
    protected void emit(KeyValPair tuple) {
        System.out.println(tuple);
        output.emit(tuple);
    }

}

