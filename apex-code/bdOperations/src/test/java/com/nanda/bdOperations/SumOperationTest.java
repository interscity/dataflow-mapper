package com.nanda.bdOperations;

import com.datatorrent.api.Attribute;
import org.apache.apex.api.EmbeddedAppLauncher;
import org.apache.apex.api.Launcher;
import org.apache.hadoop.conf.Configuration;
import org.junit.Assert;
import org.junit.Test;

import javax.validation.ConstraintViolationException;
import java.io.IOException;

public class SumOperationTest {

    @Test
    public void testApplication() throws IOException, Exception {
        try {
            Launcher.AppHandle ah = asyncRun();
            ah.shutdown(Launcher.ShutdownMode.KILL);
        } catch (ConstraintViolationException e) {
            Assert.fail("constraint violations: " + e.getConstraintViolations());
        }
    }

    private Launcher.AppHandle asyncRun() throws Exception {
        EmbeddedAppLauncher<?> launcher = Launcher.getLauncher(Launcher.LaunchMode.EMBEDDED);
        Attribute.AttributeMap launchAttributes = new Attribute.AttributeMap.DefaultAttributeMap();
        launchAttributes.put(EmbeddedAppLauncher.RUN_ASYNC, true);
        Configuration conf = new Configuration(false);
        Launcher.AppHandle appHandle = launcher.launchApp(new SumOperation(), conf, launchAttributes);
        Thread.sleep(5000);
        return appHandle;
    }

}
