package com.nanda.abstraction.pojo;

public class BusPosPojo {

	private String busLineId;
	private String busId;
	private String lineId;
	private String lastLat;
	private String lastLon;
	private String lastPosTimestamp;
	private String lastStop;
	private String lastStopTimestamp;

	public BusPosPojo() {};


	public BusPosPojo(String busLineId, String busId, String lineId, String lastLat, String lastLon, String lastPosTimestamp, String lastStop, String lastStopTimestamp) {

		this.busId = busId;
		this.lastStopTimestamp = lastStopTimestamp;
		this.lastLon = lastLon;
		this.lastLat = lastLat;
		this.lineId = lineId;
		this.lastStop = lastStop;
		this.busLineId = busLineId;
		this.lastPosTimestamp = lastPosTimestamp;
	}
	public String getBusId() { return busId; }
	public void setBusId(String busId) { this.busId = busId; }

	public String getLastStopTimestamp() { return lastStopTimestamp; }
	public void setLastStopTimestamp(String lastStopTimestamp) { this.lastStopTimestamp = lastStopTimestamp; }

	public String getLastLon() { return lastLon; }
	public void setLastLon(String lastLon) { this.lastLon = lastLon; }

	public String getLastLat() { return lastLat; }
	public void setLastLat(String lastLat) { this.lastLat = lastLat; }

	public String getLineId() { return lineId; }
	public void setLineId(String lineId) { this.lineId = lineId; }

	public String getLastStop() { return lastStop; }
	public void setLastStop(String lastStop) { this.lastStop = lastStop; }

	public String getBusLineId() { return busLineId; }
	public void setBusLineId(String busLineId) { this.busLineId = busLineId; }

	public String getLastPosTimestamp() { return lastPosTimestamp; }
	public void setLastPosTimestamp(String lastPosTimestamp) { this.lastPosTimestamp = lastPosTimestamp; }

	public String toString() {
		return busLineId + "," + busId + "," + lineId + "," + lastLat + "," + lastLon + "," + lastPosTimestamp + "," + lastStop + "," + lastStopTimestamp;
	}

}