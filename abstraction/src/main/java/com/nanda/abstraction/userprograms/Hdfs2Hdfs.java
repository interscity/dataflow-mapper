package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;
import com.nanda.abstraction.general.interfaces.MapFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hdfs2Hdfs {

    public static void main(String[] args) {

        ProjectSettings project = new ProjectSettings("Flink");
        project.setPackageName("experiment");
        project.start("batch");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read("localhost:8020/tmp/201904/","");

        d = d.map(new CalcDiff());
        d.print();
        f.write(TP.build("localhost:8020/tmp/output/"), TP.build("test.txt"), TP.build(1024), d);

        project.finish();
    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements MapFunction<String, String> {

        @Override
        public String map(String values) {

            String[] items = values.split(",");

            String line;
            String stop1;
            String stop2;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                stop1 = items[1].trim();
                Date time1 = tryParseDate(items[2].trim());
                stop2 = items[3].trim();
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + "-" + stop1 + "-" + stop2 + ", " +
                    diff + ", " + timestamp;
        }
    }
}

