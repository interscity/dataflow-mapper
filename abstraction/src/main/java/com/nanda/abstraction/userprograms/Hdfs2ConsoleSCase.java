package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.TP;
import com.nanda.abstraction.general.interfaces.MapFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Hdfs2ConsoleSCase {
    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("experiment");
        project.start("batch");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read(TP.build("localhost:8020/tmp/201904/"),TP.build(""));

        /*d.filterLambda(el -> {
                    String[] items = el.split(",");
                    for (String i : items)
                        System.out.println("===>" + i);
                    return "true";
                }
        );*/

        d.map(new CalcDiff()).print();

        project.finish();

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements MapFunction<String, String> {

        @Override
        public String map(String values) {

            String[] items = values.split(",");

            String line;
            String stop1;
            String stop2;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                stop1 = items[1].trim();
                Date time1 = tryParseDate(items[2].trim());
                stop2 = items[3].trim();
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + "-" + stop1 + "-" + stop2 + ", " +
                    diff + ", " + timestamp;
        }
    }
}
