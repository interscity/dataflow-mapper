package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.TP;
import com.nanda.abstraction.general.interfaces.MapFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Hdfs2Console {
    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings(TP.build("Both"));
        project.setPackageName(TP.build("experiment"));
        project.start(TP.build("batch"));

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read(TP.build("localhost:8020/tmp/201904/calcdiff/"),TP.build(""));

        d.map(new CalcDiff()).print();

        project.finish();

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements MapFunction<String, String> {

        @Override
        public String map(String values) {

            String[] items = values.split(",");

            String line;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                Date time1 = tryParseDate(items[2].trim());
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + ", " + diff + ", " + timestamp;
        }
    }
}
