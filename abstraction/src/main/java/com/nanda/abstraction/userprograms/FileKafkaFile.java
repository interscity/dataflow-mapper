package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;

import java.util.HashMap;

public class FileKafkaFile {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        System.out.println("Inside Dataflow");
        project.setPackageName("experiment");
        project.start("stream");

        FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs);
        Data<String> d = f.read("localhost:8020/tmp/","sample.txt");

        Kafka<String> k = new Kafka<>();
        k.write(TP.build("localhost:9092"), TP.build("example"), d);

        Kafka<String> k2 = new Kafka<>();

        Data<String> d2 = k2.read(TP.build("group"), TP.build("localhost:9092"),
                TP.build("example"), TP.build("localhost:2181"), TP.build(false));

        //d2.print();
        FileIO<String> f2 = new FileIO<>(FileIO.FileFormat.Text);
        f2.write(TP.build("/tmp/"), TP.build("output.txt"), TP.build(1024), d2);


        project.finish();

    }
}
