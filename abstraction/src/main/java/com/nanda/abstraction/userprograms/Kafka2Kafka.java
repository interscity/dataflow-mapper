package com.nanda.abstraction.userprograms;

import com.nanda.abstraction.general.*;
import com.nanda.abstraction.general.interfaces.FilterFunction;

public class Kafka2Kafka {

    public static void main(String[] args) {
        ProjectSettings project = new ProjectSettings("Both");
        project.setPackageName("novinho2");
        project.start("stream");

        Kafka<String> k = new Kafka<>();

        Data<String> d = k.read(TP.build("group"), TP.build("localhost:9092"),
                TP.build("input"), TP.build("localhost:2181"), TP.build(true));

        k.write(TP.build("localhost:9092"), TP.build("outputFull"), d);

        d = d.filter(new BiggerThanThirty());

        Kafka k2 = new Kafka<>();
        k2.write(TP.build("localhost:9092"), TP.build("biggerThanThirty"), d);

        d.print();

        project.finish();

    }


    public static class BiggerThanThirty implements FilterFunction<String> {

        @Override
        public boolean filter(String content) {
            if (!content.contains(","))
                return false;

            String max = content.split(",")[1].trim();

            if (Integer.parseInt(max) < 30)
                return false;

            return true;
        }
    }
}
