package com.nanda.abstraction.templateUDF;

import org.apache.flink.api.common.functions.FoldFunction;


/**Replace T with the appropriated type
 * Static is due to main (this function will be called inside main)*/
public final class FoldFlink implements FoldFunction<I, O> {

    @Override
    public O fold(O value1, I value2) {
        //put your code here
        return new O();
    }
}
