package com.nanda.abstraction.templateUDF;

import org.apache.apex.malhar.lib.function.Function;

/**Replace I with type of input elements and O with type of output elements*/
public class MapApex implements Function.MapFunction<I,O> {
    @Override
    public O f(I input) {
        //Put your code here and replace with appropriated output
        return new O();
    }
}
