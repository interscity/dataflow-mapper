package com.nanda.abstraction.general;

import com.nanda.abstraction.general.apex.FileA;
import com.nanda.abstraction.general.apex.JdbcA;
import com.nanda.abstraction.general.flink.FileF;
import com.nanda.abstraction.general.flink.JdbcF;

import java.util.HashMap;

public class Jdbc<T> extends IO {

    protected String jdbcVarName = "";
    protected String jdbcVarType = "";
    protected String dataName = "";
    protected String dataType = "";
    protected String tabs = "\t\t";
    protected String databaseDriver = "org.postgresql.Driver";
    protected String databaseUrl = "";

    private JdbcA jdbcA;
    private JdbcF jdbcF;

    private Auxiliar aux = new Auxiliar();

    /*User constructor*/
    public Jdbc() { super("jdbc"); }

    /*Constructors used for the mapping*/
    public Jdbc(String ioType, String jdbcVarName, String jdbcVarType) {
        super(ioType);
        this.jdbcVarName = jdbcVarName;
        this.jdbcVarType = jdbcVarType;
    }

    public Jdbc(String jdbcVarName, String jdbcVarType) {
        super("jdbc");
        /*This part sets content for Jdbc*/
        this.jdbcVarName = jdbcVarName;
        this.jdbcVarType = jdbcVarType;

        /*This part uses the other Jdbc constructor to set content to JdbcA and JdbcF*/
        this.jdbcA = new JdbcA("jdbc", jdbcVarName, jdbcVarType);
        this.jdbcF = new JdbcF("jdbc", jdbcVarName, jdbcVarType);
        System.out.println("Jdbc Constructor");
    }

    /*Return this to allow chaining*/
    public Jdbc<T> setDataInfo(String dataName, String dataType) {
        jdbcA.dataName = dataName;
        jdbcA.dataType = dataType;

        jdbcF.dataName = dataName;
        jdbcF.dataType = dataType;
        /*ItemReceiver receiver = new ItemReceiver();
        receiver.setDataReceiver(dataName, dataType);*/
        System.out.println("JJJJJJJJJJJJJJJJJ " + dataName + " >>>>>> " + dataType);
        return this;
    }

    public void setConnectionInfo(String databaseDriver, String databaseUrl) {}

    public void setConnectionInfo(String databaseDriver, String databaseUrl, String caller) {
        jdbcA.databaseDriver = databaseDriver;
        jdbcA.databaseUrl = databaseUrl;

        jdbcF.databaseDriver = databaseDriver;
        jdbcF.databaseUrl = databaseUrl;
        //this.databaseDriver = databaseDriver;
        //this.databaseUrl = databaseUrl;
    }

    /***User function***/

    public Data<T> read(String tableName, TP tupleAndTypes, String key,
                        String query, Integer partitionCount) {
        return new Data<>("batch");
    }

    /****
     These methods are not intended to be called by the user
     ***/

    protected Data<T> read(String tableName, TP tupleTypes, String key, String query, Integer partitionCount, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();
        String action = "read";
        TypeHelper typeHelper = new TypeHelper();

        if (mp.equals("flink")) action +="F";
        else if (mp.equals("apex")) action +="A";

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "jdbc"));

        System.out.println("HASH CONTENT ************");
        System.out.println(TP.hash.toString());
        if (mp.equals("apex") || mp.equals("both")) {
            jdbcA.read(tableName, TP.hash, key, query, partitionCount, caller);
            aux.incJdbcInSuffix();
        }

        if (mp.equals("flink") || mp.equals("both")) {
            jdbcF.read(tableName, TP.hash, key, query, partitionCount, caller);
            aux.incBuilderInSuffix();
        }

        Data<T> d = new Data<>(ProjectSettings.processingChoice);
        d.setDataName(jdbcA.dataName);
        d.setDataType(jdbcA.dataType);
        return d;
    }

    /***User function***/

    //public void write(String tableName, TP<HashMap<String, Object>> tupleAndTypes, String query) { }
    public void write(String tableName, TP tupleTypes, String query) { }

    /****
     These methods are not intended to be called by the user
     ***/

    public void write(String tableName, TP tupleTypes,
                      String query, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();
        String action = "write";
        TypeHelper typeHelper = new TypeHelper();

        if (mp.equals("flink")) action +="F";
        else if (mp.equals("apex")) action +="A";

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "jdbc"));

        if (mp.equals("apex") || mp.equals("both")) {
            jdbcA.write(tableName, TP.hash, query, caller);
            aux.incJdbcOutSuffix();
        }

        if (mp.equals("flink") || mp.equals("both")) {
            jdbcF.write(tableName, TP.hash, query, caller);
            aux.incBuilderOutSuffix();
        }
    }

}
