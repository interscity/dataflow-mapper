package com.nanda.abstraction.general.interfaces;

import java.io.Serializable;

public interface FilterFunction<T> extends Serializable{

    /**
     * Método Filter
     * Aplica filtro sobre os dados lidos,
     * decidindo se deve ser mantido ou descartado
     *
     * @param value T valor de entrada a ser filtrado
     * @return booleano que indica se valor deve ser mantido ou não
     *
     */

    boolean filter(T value);
}
