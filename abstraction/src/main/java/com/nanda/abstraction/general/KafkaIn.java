package com.nanda.abstraction.general;

public class KafkaIn {

    /*Environment contains methods that handles Kafka reading (addSource)*/
    public static String flinkKafkaInput(String streamOrBatch) {
        streamOrBatch = streamOrBatch.toLowerCase();
        switch (streamOrBatch) {
            case("batch"):
                return "ExecutionEnvironment";
            default:
                return "StreamExecutionEnvironment";
        }
    }

    /*Kafka Operator for input*/
    public static String apexKafkaInput() { return "KafkaSinglePortByteArrayInputOperator"; }
}
