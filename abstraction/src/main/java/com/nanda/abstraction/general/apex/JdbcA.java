package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.*;

import java.util.HashMap;
import java.util.Map;

public class JdbcA<T> extends Jdbc<T> {

    private Auxiliar aux = new Auxiliar();
    private String apexEndPath = "../abstraction-python-scripts/apex/out/";
    private String tabs = "\t\t";

    public JdbcA(String ioType, String jdbcVarName, String jdbcVarType) {
        super(ioType, jdbcVarName, jdbcVarType);
    }

    public Data<T> read(String tableName, HashMap<String, Object> tupleAndTypes, String key, String query, Integer partitionCount, String caller) {
        String action = "readF";
        TypeHelper typeHelper = new TypeHelper();

        Integer currSuffix = aux.getJdbcInSuffix();
        String tagName = "jdbcIn" + currSuffix;

        System.out.println("Criar properties e connector");
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                "jdbcR", tagName, databaseDriver, databaseUrl.replace("&", "&amp;"), tableName, "PojoEvent" + currSuffix, key, partitionCount.toString()));

        //aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/IO/add_connector.py", ProjectSettings.packageName, action, this.jdbcVarType));

        String imports = "import com.datatorrent.lib.db.jdbc.*;\n" +
                "import com.datatorrent.lib.util.FieldInfo;\n";


        String jdbcReadContent = "\n\n//placeholder\n";
        jdbcReadContent += String.format("JdbcPOJOPollInputOperator %s = " +
                "new JdbcPOJOPollInputOperator();\n", jdbcVarName);
        jdbcReadContent += String.format("%sjdbcInput.setStore(new JdbcStore());\n", tabs);
        jdbcReadContent += String.format("%sjdbcInput.setFieldInfos(addFieldInfos%s());\n", tabs, currSuffix);


        jdbcReadContent += String
                .format("%sApexStream<Object> %s = StreamFactory" +
                        ".fromInput(%s, %s.outputPort, name(\"%s\"));",
                        tabs, dataName, caller, caller, tagName);

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, jdbcReadContent);


        String endPath = "../generated-code/apex/" + ProjectSettings.packageName +
                "/src/main/java/com/nanda/" + ProjectSettings.packageName + "/PojoEvent" +
                currSuffix + ".java";

        typeHelper.createPojo(tupleAndTypes, endPath, ProjectSettings.packageName, "PojoEvent" + currSuffix);


        /*Creation of addFieldInfos method*/
        StringBuilder udf = new StringBuilder("");

        udf.append(String.format("\tprivate List<FieldInfo> addFieldInfos%s() {\n", currSuffix));
        udf.append("\t\tList<FieldInfo> fieldInfos = Lists.newArrayList();\n");

        //It can break if there is a different than expected data type
        for (Map.Entry<String, Object> entry : tupleAndTypes.entrySet()) {
            String type = entry.getValue().toString().toUpperCase();
            type = type.split("CLASS JAVA.LANG.")[1];
            udf.append(String.format("%sfieldInfos.add(new FieldInfo(\"%s\", \"%s\", SupportType.%s));\n", entry.getKey()
                    , tabs, entry.getKey().toLowerCase(), type));
        }

        udf.append("\t\treturn fieldInfos;\n\t}\n\n");

        System.out.println("UDF: " + udf);
        aux.writeGeneral(apexEndPath, "udf.java", udf.toString());

        execQueryCode(query);

        return new Data<T>(ProjectSettings.processingChoice);
    }


    /****
     This method is not intended to be called by the user
     ***/
    public void write(String tableName, HashMap<String, Object> tupleAndTypes, String query, String caller) {
        String action = "writeF";
        TypeHelper typeHelper = new TypeHelper();

        String outputOperatorName = this.jdbcVarName + "Out";

        Integer currSuffix = aux.getJdbcOutSuffix();

        String tagName = "jdbcOut" + currSuffix;

        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/properties-files/add_properties_apex.py",
                "jdbcW", tagName, databaseDriver, databaseUrl.replace("&", "&amp;"), tableName, "PojoEvent" + currSuffix));

        String imports = "import com.datatorrent.lib.db.jdbc.*;\n" +
                "import com.datatorrent.lib.util.FieldInfo.SupportType;\n";


        String jdbcWriteContent = "\n\n//placeholder\n";
        jdbcWriteContent += String
                .format("JdbcPOJOInsertOutputOperator %s = new JdbcPOJOInsertOutputOperator();\n", outputOperatorName);
        jdbcWriteContent += String.format("%s%s.setStore(new JdbcTransactionalStore();\n", tabs, outputOperatorName);
        jdbcWriteContent += String.format("%s%s.setFieldInfos(addJdbcFieldInfos%s());\n\n", tabs, outputOperatorName, currSuffix);


        jdbcWriteContent += String
                .format("%s%s.endWith(%s, %s.input, name(\"%s\"));\n"
                        , tabs, this.dataName, outputOperatorName, outputOperatorName, tagName);

        /*Write to imports.java and content.java*/
        aux.writeContent(apexEndPath, imports, jdbcWriteContent);


        String endPath = "../generated-code/apex/" + ProjectSettings.packageName +
                "/src/main/java/com/nanda/" + ProjectSettings.packageName + "/PojoEvent" +
                currSuffix + ".java";

        typeHelper.createPojo(tupleAndTypes, endPath, ProjectSettings.packageName, "PojoEvent" + currSuffix);


        /*Method addJdbcFieldInfos*/
        StringBuilder udf = new StringBuilder("");

        udf.append(String.format("\tprivate List<JdbcFieldInfo> addJdbcFieldInfos%s() {\n", currSuffix));
        udf.append("\t\tList<JdbcFieldInfo> fieldInfos = Lists.newArrayList();\n");

        //It may not work if there is a different than expected data type
        for (Map.Entry<String, Object> entry : tupleAndTypes.entrySet()) {
            String type = entry.getValue().toString().toUpperCase();
            type = type.split("CLASS JAVA.LANG.")[1];
            udf.append(String.format("%sfieldInfos.add(new JdbcFieldInfo(\"%s\", \"%s\", SupportType.%s, 0));\n", entry.getKey()
                    , tabs, entry.getKey().toLowerCase(), type));
        }

        udf.append("\t\treturn fieldInfos;\n\t}\n\n");

        System.out.println("UDF: " + udf);
        aux.writeGeneral(apexEndPath, "udf.java", udf.toString());

        execQueryCode(query);
    }


    private void execQueryCode(String query) {
        /*If setup is False, i. e., the first time JDBC is called in this program*/
        if (!aux.getSetup()) {
            aux.setSetup(true);
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/helpers/jdbc_setup_replace.py",
                    databaseDriver, databaseUrl, query));
        }
        else {
            String execQuery = "con = DriverManager.getConnection(\"" + databaseUrl + "\");\n";
            execQuery += "\t\tstmt = con.createStatement();\nstmt.executeUpdate(\"" + query + "\");\n";
            aux.writeGeneral(apexEndPath, "contentTest.java", execQuery);
        }
    }

}
