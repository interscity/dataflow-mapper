package com.nanda.abstraction.general;

public class Redirector {

    Auxiliar aux = new Auxiliar();

    public void copy(String content) {
        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/helpers/redirector.py", content, "body"));
    }

    public void copyImport(String content) {
        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/helpers/redirector.py", content, "import"));
    }
}
