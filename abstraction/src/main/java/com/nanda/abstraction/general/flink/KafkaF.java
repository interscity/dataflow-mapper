package com.nanda.abstraction.general.flink;

import com.nanda.abstraction.general.Auxiliar;
import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.Kafka;
import com.nanda.abstraction.general.ProjectSettings;

public class KafkaF<T> extends Kafka<T> {

    private Auxiliar aux = new Auxiliar();
    private String flinkEndPath = "../abstraction-python-scripts/flink/out/";

    public KafkaF(String ioType, String kafkaVarName, String kafkaVarType) {
        super(ioType, kafkaVarName, kafkaVarType);
    }

    public Data<T> read(String groupid, String bootstrap, String topic, String zookeeper, Boolean latest, String caller) {
        String action = "kafkaR";

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/properties-files/add_properties_flink.py", action, bootstrap, groupid));

        String imports =
                "import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;\n" +
                "import org.apache.flink.api.common.serialization.SimpleStringSchema;\n";

        String kafkaReadContent = "\n//placeholder\n";

        if (latest)
            kafkaReadContent +=  String.format("DataStream<String> %s = env.addSource(" +
                "new FlinkKafkaConsumer011<>(\"%s\", new SimpleStringSchema(), prop)" +
                    ".setStartFromLatest());\n", super.dataName, topic);
        else
            kafkaReadContent +=  String.format("DataStream<String> %s = env.addSource(" +
                    "new FlinkKafkaConsumer011<>(\"%s\", new SimpleStringSchema(), prop)" +
                    ".setStartFromEarliest());\n", super.dataName, topic);

        /*Write to imports.java and content.java*/
        aux.writeContent(flinkEndPath, imports, kafkaReadContent);

        return new Data<T>(ProjectSettings.processingChoice);
    }


    public void write(String bootstrap, String topic, Data data, String caller) {
        String action = "kafkaW";

        //aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "kafka"));

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/properties-files/add_properties_flink.py", action, bootstrap));
        String imports = "import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;\n" +
                "import org.apache.flink.api.common.serialization.SimpleStringSchema;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += String.format("%s.addSink(new FlinkKafkaProducer011<>(\"%s\", " +
                "new SimpleStringSchema(), prop));\n\n", data.getDataName(), topic);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);

        /*aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/IO/Kafka.py", action, bootstrap, topic, inputOperator, tagInputOperator));*/
    }

}
