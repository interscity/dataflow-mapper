package com.nanda.abstraction.general.flink;

import com.nanda.abstraction.general.Auxiliar;
import com.nanda.abstraction.general.Time;
import com.nanda.abstraction.general.Window;

public class WindowF extends Window {

    private String flinkEndPath = "../abstraction-python-scripts/flink/out/";
    private Auxiliar aux = new Auxiliar();

    public WindowF(Boolean keyed, String windowName) {
        super(keyed, windowName);
    }

    /**********Flink methods for dealing with windows*/

    /*This method handles with Flink tumbling time-based window code generation*/
    public void timeWindow(Time time, Boolean keyed, String caller) {
        String imports = "import org.apache.flink.streaming.api.windowing.time.Time;\n";
        String fileWriteContent = "\n//placeholder\n";
        if (keyed)
            fileWriteContent += String.format(".keyBy(0).timeWindow(Time.%s(%s));\n",
                    time.type, time.duration);
        else
            fileWriteContent += String.format(".timeWindowAll(Time.%s(%s));\n",
                    time.type, time.duration);
        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }


    /*This method handles with Flink sliding time-based window code generation*/
    public void timeWindow(Time time, Time slide, Boolean keyed, String caller) {
        String imports = "import org.apache.flink.streaming.api.windowing.time.Time;\n";
        String fileWriteContent = "\n//placeholder\n";
        if (keyed)
            fileWriteContent += String.format(".keyBy(0).timeWindow(Time.%s(%s), Time.%s(%s));\n",
                    time.type, time.duration, slide.type, slide.duration);
        else
            fileWriteContent += String.format(".timeWindowAll(Time.%s(%s), Time.%s(%s));\n",
                    time.type, time.duration, slide.type, slide.duration);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }

    public void sessionWindow(Time time, Boolean keyed, String caller) {
        String imports = "import org.apache.flink.streaming.api.windowing.time.Time;\n";
        String fileWriteContent = "\n//placeholder\n";
        if (keyed)
            fileWriteContent += String.format(".keyBy(0).window(Time.%s(%s));\n",
                    time.type, time.duration);
        else
            fileWriteContent += String.format(".windowAll(Time.%s(%s));\n",
                    time.type, time.duration);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }

    /*This method handles with Flink tumbling count-based window code generation*/
    public void countWindow(long size, Boolean keyed, String caller) {
        String imports = "";
        String fileWriteContent = "\n//placeholder\n";
        if (keyed)
            fileWriteContent += String.format(".keyBy(0).countWindow(long %s);\n", size);
        else
            fileWriteContent += String.format(".countWindowAll(long %s);\n", size);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }

    /*This method handles with Flink sliding count-based window code generation*/
    public void countWindow(long size, long slide, Boolean keyed, String caller) {
        String imports = "import org.apache.flink.streaming.api.windowing.time.Time;\n";
        String fileWriteContent = "\n//placeholder\n";

        if (keyed)
            fileWriteContent += String.format(".keyBy(0).countWindow(%s, %s);\n", size, slide);
        else
            fileWriteContent += String.format(".countWindowAll(%s, %s);\n", size, slide);

        aux.writeContent(flinkEndPath, imports, fileWriteContent);
    }

}