package com.nanda.abstraction.general;

import java.io.*;
import java.util.stream.Stream;

public class Auxiliar {

    private static Integer sumSuffix = 1;
    private static Integer minSuffix = 1;
    private static Integer maxSuffix = 1;
    private static Integer kafkaInSuffix = 1;
    private static Integer kafkaOutSuffix = 1;
    private static Integer jdbcInSuffix = 1;
    private static Integer jdbcOutSuffix = 1;

    private static Integer fileInSuffix = 1;
    private static Integer fileOutSuffix = 1;
    private static Integer hdfsInSuffix = 1;
    private static Integer hdfsOutSuffix = 1;

    private static Integer builderInSuffix = 1;
    private static Integer builderOutSuffix = 1;

    private static Integer reduceKeyValSuffix = 1;
    private static Integer reduceParamSuffix = 1;

    private static Boolean setup = false;


    public void callScript(ProcessBuilder pb) {
        try {
            Process p = pb.start();
            BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
            Stream<String> ret = in.lines();
            System.out.println("value is : ");
            ret.forEach(System.out::println);
        } catch(Exception e){
            System.out.println(e);
        }
    }

    public void writeContent(String endPath, String imports, String fileReadContent) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(endPath + "imports.java", true));
            writer.write(imports);
            writer.close();

            writer = new BufferedWriter(new FileWriter(endPath + "content.java", true));
            writer.write(fileReadContent);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeGeneral(String endPath, String fname, String udf) {
        BufferedWriter writer;
        try {
            writer = new BufferedWriter(new FileWriter(endPath + fname, true));
            writer.write(udf);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*Increment methods are protected to restrict the access. However, getters
    * are only for obtaining the content (does not change it). So it can be public
    * and apex/flink classes can access it as well.*/

    protected static void incSumSuffix() { sumSuffix += 1; }
    public static Integer getSumSuffix() { return sumSuffix; }

    protected static void incMinSuffix() { minSuffix += 1; }
    public static Integer getMinSuffix() { return minSuffix; }

    protected static void incMaxSuffix() { maxSuffix += 1; }
    public static Integer getMaxSuffix() { return maxSuffix; }

    protected void incKafkaInSuffix() { kafkaInSuffix += 1; }
    public Integer getKafkaInSuffix() { return kafkaInSuffix; }

    protected void incKafkaOutSuffix() { kafkaOutSuffix += 1; }
    public Integer getKafkaOutSuffix() { return kafkaOutSuffix; }

    protected void incFileInSuffix() { fileInSuffix += 1; }
    public Integer getFileInSuffix() { return fileInSuffix; }

    protected void incFileOutSuffix() { fileOutSuffix += 1; }
    public Integer getFileOutSuffix() { return fileOutSuffix; }

    protected void incHdfsInSuffix() { hdfsInSuffix += 1; }
    public Integer getHdfsInSuffix() { return hdfsInSuffix; }

    protected void incHdfsOutSuffix() { hdfsOutSuffix += 1; }
    public Integer getHdfsOutSuffix() { return hdfsOutSuffix; }

    protected void incJdbcInSuffix() { jdbcInSuffix += 1; }
    public Integer getJdbcInSuffix() { return jdbcInSuffix; }

    protected void incBuilderInSuffix() { builderInSuffix += 1; }
    public Integer getBuilderInSuffix() { return builderInSuffix; }

    protected void incJdbcOutSuffix() { jdbcOutSuffix += 1; }
    public Integer getJdbcOutSuffix() { return jdbcOutSuffix; }

    protected void incBuilderOutSuffix() { builderOutSuffix += 1; }
    public Integer getBuilderOutSuffix() { return builderOutSuffix; }

    protected void incReduceKeyValSuffix() { reduceKeyValSuffix += 1; }
    public Integer getReduceKeyValSuffix() { return reduceKeyValSuffix; }

    protected void incReduceParamSuffix() { reduceParamSuffix += 1; }
    public Integer getReduceParamSuffix() { return reduceParamSuffix; }

    public void setSetup(Boolean value) { setup = value; }
    public Boolean getSetup() { return setup; }

}
