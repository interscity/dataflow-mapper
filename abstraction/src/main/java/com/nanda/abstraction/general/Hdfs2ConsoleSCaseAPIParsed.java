package com.nanda.abstraction.general;
import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.FileIO;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.TP;
import com.nanda.abstraction.general.interfaces.MapFunction;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

 public class Hdfs2ConsoleSCaseAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "Hdfs2ConsoleSCase");
		project.setPackageName("experiment");
		project.start("batch");
		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		Data<String> d = f.setDataInfo("d", "String", new Data<String>()).read(TP.build("localhost:8020/tmp/201904/"), TP.build(""),"f");
		project.finish();
	}
}
