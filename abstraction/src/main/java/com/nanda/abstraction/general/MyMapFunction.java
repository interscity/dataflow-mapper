package com.nanda.abstraction.general;

import java.io.Serializable;

@FunctionalInterface
public interface MyMapFunction<T, O> extends Serializable {

    /**
     * Mapping method.
     * Takes an element from the input data and transforms it into exactly one element.
     *
     * @param value The input value.
     * @return The transformed value
     *
     */

    O myMap(T value) throws Exception;
}

