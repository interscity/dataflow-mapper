package com.nanda.abstraction.general;

import com.nanda.abstraction.general.apex.KafkaA;
import com.nanda.abstraction.general.flink.KafkaF;

public class Kafka<T> extends IO {

    protected String bootstrap;
    protected String kafkaVarName;
    protected String kafkaVarType;
    protected String dataName;
    protected String dataType;

    private KafkaA kafkaA;
    private KafkaF kafkaF;

    private Auxiliar aux = new Auxiliar();

    /*Topic is not in the constructor since a user can use one topic
    * for method read and another for write*/

    /*Abstraction user Constructor*/
    public Kafka() {
        super("kafka");
        System.out.println("Kafka Constructor");
    }

    /*Constructors used for the mapping*/
    public Kafka(String ioType, String kafkaVarName, String kafkaVarType) {
        super(ioType);
        this.kafkaVarName = kafkaVarName;
        this.kafkaVarType = kafkaVarType;
    }

    public Kafka(String kafkaVarName, String kafkaVarType) {
        super("kafka");
        /*This part sets content for Kafka*/
        this.kafkaVarName = kafkaVarName;
        this.kafkaVarType = kafkaVarType;

        /*This part uses the other Kafka constructor to set content to KafkaA and KafkaF*/
        this.kafkaF = new KafkaF("kafka", kafkaVarName, kafkaVarType);
        this.kafkaA = new KafkaA("kafka", kafkaVarName, kafkaVarType);
        System.out.println("Kafka Constructor");
    }

    public <O> Kafka<T> setDataInfo(String dataName, String dataType, Data<O> d) {
        kafkaA.dataName = dataName;
        kafkaA.dataType = dataType;

        kafkaF.dataName = dataName;
        kafkaF.dataType = dataType;
        System.out.println("Inside Kafka method setDataInfo");
        return this;
    }

    /***User function***/
    public Data<T> read(String groupid, String bootstrap, String topic, String zookeeper, Boolean latest) {
        return new Data<>("stream");
    }

    /****
     These methods are not intended to be called by the user
     ***/
    protected Data<T> read(String groupid, String bootstrap, String topic, String zookeeper, Boolean latest, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "kafka"));

        if (mp.equals("apex") || mp.equals("both")) {
            kafkaA.read(groupid, bootstrap, topic, zookeeper, latest, caller);
            aux.incKafkaInSuffix();
        }

        if (mp.equals("flink") || mp.equals("both"))
            kafkaF.read(groupid, bootstrap, topic, zookeeper, latest, caller);

        Data<T> d = new Data<>(ProjectSettings.processingChoice);
        d.setDataName(kafkaA.dataName);
        d.setDataType(kafkaF.dataType);
        return d;
    }

    /***User function***/
    public void write(String bootstrap, String topic, Data data) { }


    /****
     These methods are not intended to be called by the user
     ***/
    protected void write(String bootstrap, String topic, Data data, String caller) {
        String mp = ProjectSettings.mapper.toLowerCase();

        aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", ProjectSettings.packageName, ProjectSettings.mapper, "kafka"));

        if (mp.equals("apex") || mp.equals("both")) {
            kafkaA.write(bootstrap, topic, data, caller);
            aux.incKafkaOutSuffix();
        }

        if (mp.equals("flink") || mp.equals("both"))
            kafkaF.write(bootstrap, topic, data, caller);
    }

    public static void main(String args[]) throws Exception {
        ProjectSettings settings = new ProjectSettings("Myclass", "flink");
        Kafka k = new Kafka();
         /*Data d = k.read("group", "localhost:9092", "my-topic", "localhost:8081");
        System.out.println(d.getDataStreamName());*/

    }
}