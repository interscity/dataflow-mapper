package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.UDF;
import com.nanda.abstraction.general.interfaces.FilterFunction;
import com.nanda.abstraction.general.interfaces.FlatMapFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;

public class UdfA extends UDF {

    UdfA() {}

    @Override
    public void map(MapFunction nameClass, String instance) {
        super.map(nameClass, instance);
    }

    @Override
    public void flatMap(FlatMapFunction nameClass, String instance) {
        super.flatMap(nameClass, instance);
    }

    @Override
    public void filter(FilterFunction nameClass, String instance) {
        super.filter(nameClass, instance);
    }

    @Override
    public void reduce(Object nameClass, String instance) {
        super.reduce(nameClass, instance);
    }

}