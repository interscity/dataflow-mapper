package com.nanda.abstraction.general;

 public class Kafka2HdfsAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "Kafka2Hdfs");
		project.setPackageName("experiment");
		project.start("stream");
		Kafka<String> k = new Kafka<>("k", "String");
		Data<String> d = k.setDataInfo("d", "String", new Data<String>()).read(TP.build("group"), TP.build("localhost:9092"), TP.build("example"), TP.build("localhost:2181"), TP.build(false),"k");

		FileIO<String> f = new FileIO<>(FileIO.FileFormat.Hdfs, "f", "String");
		f.write(TP.build("localhost:8020/tmp/"), TP.build("sample.txt"), TP.build(1024), d,"f");
		project.finish();
	}
}
