package com.nanda.abstraction.general;

public class ItemReceiver {


    ItemReceiver() {}


    //The processing type will be given by the user in the case there is a constructor
    public void setDataReceiver(String dataName, String dataTypeF, String dataTypeA) {
        Auxiliar aux = new Auxiliar();

        System.out.println("Inside ItemReceiver");
        aux.callScript(new ProcessBuilder("python3",
                "../abstraction-python-scripts/helpers/item_receiver.py",
                ProjectSettings.mapper,
                ProjectSettings.processingChoice,
                dataTypeF,
                dataTypeA,
                dataName));
    }

    public void setDataReceiverProcessing(String dataName, String dataTypeF, String dataTypeA) {
        Auxiliar aux = new Auxiliar();

        System.out.println("Inside ItemReceiver");
        aux.callScript(new ProcessBuilder("python3",
                "../abstraction-python-scripts/helpers/item_receiver.py",
                ProjectSettings.mapper,
                ProjectSettings.processingChoice,
                dataTypeF,
                dataTypeA,
                dataName,
                "constructor"
        ));
    }

    public void putTPReceiver(String key, String value, String caller) {
        Auxiliar aux = new Auxiliar();

        System.out.println("Inside ItemReceiver");
        aux.callScript(new ProcessBuilder("python3",
                "../abstraction-python-scripts/helpers/item_receiver_tp.py",
                ProjectSettings.mapper,
                ProjectSettings.processingChoice,
                caller,
                "\"" + key + "\"",
                value));
    }

    public void createTPReceiver(String caller) {
        Auxiliar aux = new Auxiliar();

        System.out.println("Inside ItemReceiver");
        aux.callScript(new ProcessBuilder("python3",
                "../abstraction-python-scripts/helpers/item_receiver_tp.py",
                ProjectSettings.mapper,
                ProjectSettings.processingChoice,
                caller));
    }
}
