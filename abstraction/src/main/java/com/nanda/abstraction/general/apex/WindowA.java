package com.nanda.abstraction.general.apex;

import com.nanda.abstraction.general.Auxiliar;
import com.nanda.abstraction.general.Time;
import com.nanda.abstraction.general.Window;

public class WindowA extends Window {

    private String apexEndPath = "../abstraction-python-scripts/apex/out/";
    private Auxiliar aux = new Auxiliar();

    public WindowA(Boolean keyed, String windowName) {
        super(keyed, windowName);
    }

    /**********Apex methods for dealing with windows*/

    /*This method handles with Apex tumbling time-based window code generation*/
    public void timeWindow(Time time, String caller) {
        String timeStandard = time.type.substring(0, 1).toUpperCase() + time.type.substring(1);
        String imports = "import org.apache.apex.malhar.lib.window.WindowOption;\n";
        imports+= "import org.apache.apex.malhar.lib.window.TriggerOption;\n";
        imports+= "import org.joda.time.Duration;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += String.format(".window(new WindowOption.TimeWindows(Duration.standard%s(%s)), new TriggerOption().withEarlyFiringsAtEvery(Duration.standardSeconds(%s)));\n",
                timeStandard, time.duration, time.duration);

        aux.writeContent(apexEndPath, imports, fileWriteContent);
    }


    /*This method handles with Apex sliding time-based window code generation*/
    public void timeWindow(Time time, Time slide, String caller) {
        String timeStandard = time.type.substring(0, 1).toUpperCase() + time.type.substring(1);
        String slideStandard = slide.type.substring(0, 1).toUpperCase() + slide.type.substring(1);

        String imports = "import org.apache.apex.malhar.lib.window.WindowOption;\n";
        imports+= "import org.joda.time.Duration;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += String
                .format(".window(new WindowOption.TimeWindows(Duration.standard%s(%s), Duration.standard%s(%s)));\n",
                        timeStandard, time.duration, slideStandard, slide.duration);

        aux.writeContent(apexEndPath, imports, fileWriteContent);
    }

    /*This method handles with Apex sliding time-based window code generation*/
    public void sessionWindow(Time time, String caller) {
        String timeStandard = time.type.substring(0, 1).toUpperCase() + time.type.substring(1);
        String imports = "import org.apache.apex.malhar.lib.window.WindowOption;\n";
        imports+= "import org.joda.time.Duration;\n";

        String fileWriteContent = "\n//placeholder\n";
        fileWriteContent += String
                .format(".window(new WindowOption.SessionWindows(Duration.standard%s(%s));\n",
                        timeStandard, time.duration);
        aux.writeContent(apexEndPath, imports, fileWriteContent);
    }

}
