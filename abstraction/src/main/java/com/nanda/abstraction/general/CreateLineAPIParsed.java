package com.nanda.abstraction.general;
import com.nanda.abstraction.general.Data;
import com.nanda.abstraction.general.Jdbc;
import com.nanda.abstraction.general.ProjectSettings;
import com.nanda.abstraction.general.TP;

 public class CreateLineAPIParsed {

	 public static void main(String args[]) {

		ProjectSettings project = new ProjectSettings("MyClass2", "Both");
		project.setPackageName("novinho2");
		project.start("batch");
		TP tupleTypes = TP.setDataInfo("tupleTypes", "null");
		TP.put("line_id", String.class);
		TP.put("line_origin", String.class);
		TP.put("line_destination", String.class);
		TP.put("line_way", String.class);
		Jdbc<String> j = new Jdbc<>("j", "String");
		j.setConnectionInfo(TP.build("org.postgresql.Driver"), TP.build("jdbc:postgresql://localhost:5432/source?user=guest&password=guest"), "j");
		Data<String> dt = j.setDataInfo("dt", "String").read(TP.build("bus_line"), tupleTypes, TP.build("line_id"), TP.build("select * from bus_line;"), TP.build(2),"j");

	
	}
}