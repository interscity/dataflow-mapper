package com.nanda.abstraction.general;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.*;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.printer.lexicalpreservation.LexicalPreservingPrinter;
import com.github.javaparser.symbolsolver.JavaSymbolSolver;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.CombinedTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.JavaParserTypeSolver;
import com.github.javaparser.symbolsolver.resolution.typesolvers.ReflectionTypeSolver;
import com.nanda.abstraction.templateUDF.O;

import java.io.*;
import java.lang.invoke.MethodType;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ParserCp {
    private static final Pattern API_PACKAGE = Pattern.compile("com.nanda.abstraction.general");
    private static final Pattern PROJ_SETTINGS = Pattern.compile("ProjectSettings");
    private static final Pattern TIME_CLASS = Pattern.compile("Time");
    private static final Pattern IO_TP_CLASS = Pattern.compile("FileIO|Kafka|Jdbc|TP");
    private static final Pattern IO_CLASS = Pattern.compile("FileIO|Kafka|Jdbc");
    private static final Pattern TP_CLASS = Pattern.compile("TP");
    private static final Pattern KV_CLASS = Pattern.compile("KV");
    private static final Pattern DATA_CLASS = Pattern.compile("Data");
    private static final Pattern WINDOW_CLASS = Pattern.compile("Window");
    private static final Pattern IO_DATA_DTRANS = Pattern.compile("IO|Data|DataTransformation|Window|ProjectSettings|TP");

    private static TreeMap<Integer, String> apiStatements = new TreeMap<>();
    private static TreeMap<Integer, String> fileContentF = new TreeMap<>();
    private static TreeMap<Integer, String> fileContentA = new TreeMap<>();
    private static String mapper = "Both"; //default
    private static String streamOrBatch = "Stream"; //default
    private static List<String> toFlink = Arrays.asList("flink", "Flink", "both", "Both");
    private static List<String> toApex = Arrays.asList("apex", "Apex", "both", "Both");
    private static List<String> udfs = Arrays.asList("map", "flatMap", "reduce", "filter", "folder", "print");
    private static List<String> aggregationAndWindows = Arrays.asList("maxByKey", "minByKey", "sum", "timeWindow", "sessionWindow", "countWindow");

    private static List<String> importsFlink = new ArrayList<>();
    private static List<String> importsApex = new ArrayList<>();
    private static String FILE_NAME;

    private static Auxiliar aux = new Auxiliar();

    public static void main(String[] args) throws Exception {

        CompilationUnit comp;

        if (args.length != 1) {
            System.out.println("Usage: java Parser.java pathTofile.java");
            System.exit(0);
        }

        String WHOLE_FILE_PATH, FILE_PATH;

        WHOLE_FILE_PATH = args[0];
        System.out.println(WHOLE_FILE_PATH);
        FILE_NAME = WHOLE_FILE_PATH.replaceAll(".*/", "");
        FILE_PATH = WHOLE_FILE_PATH.substring(0, WHOLE_FILE_PATH.length() - FILE_NAME.length());
        FILE_NAME = FILE_NAME.replace(".java", "");

        System.out.println(FILE_NAME);
        System.out.println(FILE_PATH);


        TypeSolver typeSolver = new CombinedTypeSolver(
                new ReflectionTypeSolver(),
                new JavaParserTypeSolver(new File("src/main/java/")));

        JavaSymbolSolver symbolSolver = new JavaSymbolSolver(typeSolver);
        JavaParser.getStaticConfiguration().setSymbolResolver(symbolSolver);
        comp = LexicalPreservingPrinter.setup(JavaParser.parse(new FileInputStream(WHOLE_FILE_PATH)));


        /*Read from the used given file, mounting the compilation unit (syntax tree)*/
        BufferedReader reader;
        int cont = 1;
        try {
            reader = new BufferedReader(new FileReader(WHOLE_FILE_PATH));
            String line = reader.readLine();
            while (line != null) {
                //This includes the case: a method of the current class is called in the body
                //of the function, like ClassName.my_method();

                fileContentF.put(cont, line);
                fileContentA.put(cont, line);
                line = reader.readLine();
                cont++;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*Get mapper before doing the rest*/
        comp.findAll(VariableDeclarationExpr.class)
                .forEach(n -> {
                    if (PROJ_SETTINGS.matcher(n.toString()).find()) {
                        mapper = n.getVariable(0).getInitializer().get().toString();
                        mapper = mapper.split("\\([\\s\\t\n\"]*")[mapper.split("\\([\\s\\t\n\"]*").length - 1];
                        mapper = mapper.split("[\"\\s\\t\n]*\\)")[0];
                    }
                });

        comp.accept(new VarDeclVisitor(), JavaParserFacade.get(typeSolver));
        comp.accept(new MethodCallVisitor(), JavaParserFacade.get(typeSolver));

        if (toFlink.contains(mapper)) {
            FlinkParser f = new FlinkParser(apiStatements, fileContentF, importsFlink,
                    FILE_NAME, streamOrBatch);
            f.doMapping(comp, typeSolver);
        }

        if (toApex.contains(mapper)) {
            ApexParser a = new ApexParser(apiStatements, fileContentA, importsApex,
                    FILE_NAME, streamOrBatch);
            a.doMapping(comp, typeSolver);
        }

        /*It writes to a file the commands from our notation*/
        BufferedWriter writer;
        try {
            //writer = new BufferedWriter(new FileWriter(FILE_PATH + FILE_NAME + "APIParsed.java"));
            writer = new BufferedWriter(new FileWriter("src/main/java/com/nanda/abstraction/general/"
                    + FILE_NAME + "APIParsed.java"));
            //writer.write("package " + comp.getPackageDeclaration().get().getName().asString() + ";\n");
            writer.write("package com.nanda.abstraction.general;\n");
            for (ImportDeclaration item : comp.getImports())
                if (! item.toString().contains("com.nanda.abstraction.general.*;"))
                    writer.write(item.toString());
            ClassOrInterfaceDeclaration myClass = new ClassOrInterfaceDeclaration();
            writer.write("\n public class " + FILE_NAME + "APIParsed {\n");
            writer.write("\n\t public static void main(String args[]) throws Exception {\n");

            System.out.println(apiStatements);
            for (Integer key : apiStatements.keySet())
                writer.write("\n" + apiStatements.get(key));
            writer.write("\n\t}\n}");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    static class MethodCallVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(MethodCallExpr n, JavaParserFacade javaParserFacade) {
            super.visit(n, javaParserFacade);
            System.out.println(n);

            /*Here is retrieved the type of the object instances that call methods.
             * For example: f.read(args);*/

            /*This happens, for example, when a method is call without an associated object.
            * For example methodX() instead of obj.methodX()*/
            if (!n.getScope().isPresent()) return n;

            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;
            Matcher m = API_PACKAGE.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
            if (m.find()) { //if it is in our API
                String contentF, contentA;
                StringBuilder tempF = new StringBuilder();
                StringBuilder tempA = new StringBuilder();

                for (int i = beginLine; i <= endLine; i++) {
                    tempF.append("\n").append(fileContentF.get(i));
                    tempA.append("\n").append(fileContentA.get(i));
                }
                contentF = tempF.toString();
                contentA = tempA.toString();


                String target;
                String placeholderF;
                String placeholderA;

                MethodCallExpr copy = n.clone();

                /*If this is our API method call, add to apiStatements and replace fileContent with a
                 * placeholder*/

                /*If the method called is project settings, Time or it is one of common udfs,
                does not add the caller as argument*/
                Matcher proj = PROJ_SETTINGS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                Matcher ioAndTP = IO_TP_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                Matcher ioClass = IO_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                Matcher tpClass = TP_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                Matcher kvClass = KV_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                Matcher timeClass = TIME_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());

                /*Split on caller object is not a good idea, since if occurs a var attribution in which
                the variable is substring of the caller, it will fail. So it is a better option
                to split by the method*/
                String caller = n.toString().split("\\.")[0];
                Boolean isFirstMethod;
                String methods[] = n.toString().split("\\)\\.");

                if (methods[methods.length - 1].contains(caller)) {
                    isFirstMethod = true;
                    /*If last method is actually the first and contains the caller, remove the caller*/
                    methods[methods.length - 1] = methods[methods.length - 1].replace(caller + ".", "");
                } else
                    isFirstMethod = false;

                String lastMethod = methods[methods.length - 1];
                String lastMethodOld = lastMethod;
                String lastMethodName = lastMethod.split("\\(")[0];

                /*In this case, remove the whole line since it will not generate any code*/
                if (proj.find()) {
                    /*Target for Project methods are the same for Flink and Apex*/
                    target = contentF;
                    /*In Flink case, it is necessary to create the environment, that is why a
                     * placeholder is necessary*/
                    if (n.getNameAsString().equals("start") && toFlink.contains(mapper)) {
                        placeholderF = "\t\t/*$placeholderMethodCall*/";
                        placeholderA = "";
                        streamOrBatch = n.getArgument(0).toString();
                        System.out.println(streamOrBatch);
                    }
                    /*In Apex case, Project methods do not generate any code. In Flink case, only
                     * project start generates code*/
                    else
                        placeholderF = placeholderA = "";

                    contentF = contentF.replace(target, placeholderF);
                    fileContentF.put(beginLine, contentF);
                    contentA = contentA.replace(target, placeholderA);
                    fileContentA.put(beginLine, contentA);

                    n.getRange().ifPresent(r -> apiStatements.put(beginLine, "\t\t" + copy.toString() + ";"));
                }


                else {
                    tpClass = TP_CLASS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());
                    /*It means that a caller need to be added, since it is actually the first method and
                     * it is from class Data. There is a mix of VariableDeclarationExpr with MethodCallExpr*/
                    //if (contentF.matches("[\n\\s]*|[\n\\s]*/\\*\\$placeholderVariableDeclarationExpr\\*/")) {
                    if (contentF.matches("[\n\\s]*/\\*\\$placeholderVariableDeclarationExpr\\*/")) {
                        Matcher dataMatcher = DATA_CLASS.matcher(javaParserFacade.getType(n.getScope().get()).describe());
                        if (dataMatcher.find()) {
                            lastMethod = " " + caller + "." + lastMethod;
                            isFirstMethod = true;
                        }
                    }


                   if (udfs.contains(lastMethodName)) {
                        if (contentF.matches("[\n\\s]*.*;[\n\\s]*$") && !isFirstMethod)
                            contentF = contentF.replace(";", "");

                        /*This solves cases like System.out.println(d.map("Example"));*/
                        if (contentF.contains(lastMethodName))
                            fileContentF.put(beginLine, contentF.replace(lastMethodOld, lastMethod));
                        else {
                            /*If caller is present, does not add '.'*/
                            if (isFirstMethod)
                                fileContentF.put(beginLine, contentF + lastMethod + ";");
                            else
                                fileContentF.put(beginLine, contentF + "." + lastMethod + ";");
                        }

                        if (lastMethod.contains("reduce")) {
                            lastMethod = lastMethod.replace("reduce", "reduceByKey");
                            Integer suffix = aux.getReduceParamSuffix();
                            String suffixToAdd = ", new ToKeyVal" + suffix + "())";
                            lastMethod = lastMethod.substring(0, lastMethod.lastIndexOf(")")) + suffixToAdd;
                        }

                        if (contentA.matches("[\n\\s]*.*;[\n\\s]*$")  && !isFirstMethod)
                            contentA = contentA.replace(";", "");

                        if (contentA.contains(lastMethodName))
                           fileContentA.put(beginLine, contentA.replace(lastMethodOld, lastMethod));
                        else {

                            /*If caller is present, does not add '.'*/
                            if (isFirstMethod) fileContentA.put(beginLine, contentA + lastMethod + ";");
                            else fileContentA.put(beginLine, contentA + "." + lastMethod + ";");
                        }

                    //} else if (tpClass.find()) {
                        //fileContentF.put(beginLine, contentA.replace("TP." + lastMethod, "/*$placeholderMethodCall*/"));
                        //fileContentA.put(beginLine, contentF.replace("TP." + lastMethod, "/*$placeholderMethodCall*/"));
                    //} else if (timeClass.find()) {
                        //fileContentF.put(beginLine, contentA.replace("Time." + lastMethod, "/*$placeholderMethodCall*/"));
                        //fileContentA.put(beginLine, contentF.replace("Time." + lastMethod, "/*$placeholderMethodCall*/"));
                   } else if (methods.length >= 1 && !tpClass.find() && !timeClass.find()) {

                        if (aggregationAndWindows.contains(n.getNameAsString())){
                           target = "." + lastMethod;
                           placeholderF = "/*$placeholderMethodCall*/";
                           placeholderA = "/*$placeholderMethodCall*/";

                           //String newArg = n.getScope().get().toString();
                           copy.addArgument('\"' + caller + '\"');
                           //n.getRange().ifPresent(r -> apiStatements.put(beginLine, "\t\t" + copy.toString() + ";"));

                           if (contentF.matches("[\n\\s]*.*;[\n\\s]*$")  && !isFirstMethod)
                               contentF = contentF.replace(";", "");
                           contentF = contentF.replace(target, placeholderF);
                           fileContentF.put(beginLine, contentF);

                           if (contentA.matches("[\n\\s]*.*;[\n\\s]*$")  && !isFirstMethod)
                               contentA = contentA.replace(";", "");
                           contentA = contentA.replace(target, placeholderA);
                           fileContentA.put(beginLine, contentA);
                        }

                        String curr;
                        System.out.println("Last method: " + lastMethod);
                        Pattern noArgs = Pattern.compile("\\([ \t\n]*\\)");
                        Matcher mNoArgs = noArgs.matcher(lastMethod);
                        Integer idxParenthesis = lastMethod.lastIndexOf(")");

                        String addCaller = ",\"" + caller + "\");";

                        if (kvClass.find() && (lastMethod.contains("getKey") || lastMethod.contains("getValue"))) {
                            curr = "KeyValue." + lastMethod.substring(0, idxParenthesis) + ");\n";

                            if (apiStatements.get(beginLine) == null) {
                                lastMethodOld = caller + "." + lastMethodOld;
                                n.getRange().ifPresent(r -> apiStatements.put(beginLine, "\t\t" + curr));
                            }
                            else {
                                lastMethodOld = "." + lastMethodOld;
                                n.getRange()
                                        .ifPresent(r -> apiStatements.put(beginLine,
                                                apiStatements.get(beginLine) + "\t\t" + curr));
                            }
                        }

                        else {

                            /*If method does not have arguments, else (has arguments)*/
                            if (mNoArgs.find())
                                addCaller = "\"" + caller + "\");";

                            curr = "." + lastMethod.substring(0, idxParenthesis) + addCaller;


                            String pattern = "^\\s*$";
                            Pattern whitespace = Pattern.compile(pattern);
                            Matcher mwhitespace;
                            /*If line is null, empty or with whitespace*/
                            if (apiStatements.get(beginLine) == null || (whitespace.matcher(apiStatements.get(beginLine)).find())) {
                                lastMethodOld = caller + "." + lastMethodOld;
                                n.getRange().ifPresent(r -> apiStatements.put(beginLine,
                                        "\t\t" + caller + curr));
                            }

                            else {
                                lastMethodOld = "." + lastMethodOld;
                                n.getRange()
                                        .ifPresent(r -> apiStatements.put(beginLine,
                                                apiStatements.get(beginLine).replace(";", curr)));
                            }
                        }

                        fileContentF.put(beginLine, contentF.replace(lastMethodOld, "/*$placeholderMethodCall*/"));
                        fileContentA.put(beginLine, contentA.replace(lastMethodOld, "/*$placeholderMethodCall*/"));

                   }
                }

                //For other lines
                for (int i = beginLine + 1; i <= endLine; i++) {
                    if (apiStatements.get(i) == null) {
                        apiStatements.put(i, "");
                        fileContentF.put(i, "");
                        fileContentA.put(i, "");
                    }
                }
            }


            return n;
        }

    }


    static String[] getReceiverAndType(Node n, Expression item) {
        String[] returnValues = new String[2];
        String temp = n.toString().split("=")[0].trim();
        String[] tokens = temp.split("\\s+");
        String receiver = tokens[tokens.length-1].trim();

        returnValues[0] = receiver;
        if (temp.contains("<") && temp.contains(">")) {
            String dataType = temp.substring(temp.indexOf("<") + 1, temp.lastIndexOf(">"));
            returnValues[1] = dataType;
        }
        return returnValues;
    }

    /*It is being considered only the 4 scenarios below: varDeclaration is
     "int var = 0" or "Data<String> d = new Data<>();" or Data<String> d = d2.print();
     or Data<String> d = d2.flatMap().reduce().print();*/
    static class VarDeclVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(VariableDeclarationExpr n, JavaParserFacade arg) {

            super.visit(n, arg);

            Matcher m = API_PACKAGE.matcher(arg.getType(n).describe());

            Matcher io_data_dtrans_matcher = IO_DATA_DTRANS
                    .matcher(arg.getType(n).describe());

            System.out.println(">>>>" + arg.getType(n).describe());

            if (m.find() /*&& io_data_dtrans_matcher.find()*/) {
                int beginLine = n.getBegin().get().line;
                int endLine = n.getEnd().get().line;
                String placeholder = "\t\t/*$placeholderVariableDeclarationExpr*/";

                Node nodeCopy = n.clone();

                List<String> test = new LinkedList<>();
                List<Boolean> hasMethodOrObjectDecl = new LinkedList<>();

                nodeCopy.findAll(MethodCallExpr.class)
                        .forEach(item -> {
                            /*Add the caller to the first method (the others will receive it in MethodCallVisitor).
                             * When there is a method chaining, we get a list with the partial calls
                             * [d.flatMap().reduce(), d.flatMap(), d]
                             * The necessary part is 'd', the caller, which is not a MethodCall
                             * Also, if it is TP, does not need .setDataInfo*/
                            String instanceName = item.getScope().get().toString();
                            //MethodCallExpr methodSet = new MethodCallExpr(instanceName + ".setDataInfo");
                            String methodSet = instanceName + ".setDataInfo(";

                            if (! item.getScope().get().isMethodCallExpr()) {
                                String[] returnedValues = getReceiverAndType(n, item);

                                methodSet += String
                                        .format("\"%s\", \"%s\", new Data<%s>())", returnedValues[0],
                                                returnedValues[1], returnedValues[1]);

                                /*for (String i : returnedValues)
                                      methodSet.addArgument("\"" + i + "\"");*/

                                String firstPart = nodeCopy.toString().split(instanceName + "\\.")[0];


                                //In most cases it is added the caller as method parameter, except for TP.build
                                if (! instanceName.contains("Time") && (! instanceName.contains("TP") || nodeCopy.toString().contains("TP.create"))) {
                                    //String content = firstPart + methodSet.toString();
                                    String content = firstPart + methodSet;
                                    test.add(content);
                                }
                            }
                            if (hasMethodOrObjectDecl.size() == 0) hasMethodOrObjectDecl.add(true);

                        });

                nodeCopy.findAll(ObjectCreationExpr.class)
                        .forEach(item -> {

                            System.out.println(">>>>" + item.getType().getParentNode().get());
                            System.out.println(arg.getType(n).describe());

                            if (! item.getType().toString().equals("ProjectSettings")) {
                                String[] returnedValues = getReceiverAndType(n, item);
                                for (String i : returnedValues)
                                    item.addArgument("\"" + i + "\"");
                            }
                            else
                                item.addArgument("\"" + FILE_NAME + "\"");

                            /*FileContent line will be empty if constructor is not Data*/
                            Matcher data_matcher = DATA_CLASS.matcher(item.getType().toString());
                            if (!data_matcher.find()) {
                                System.out.println("I'M HERE " + nodeCopy.toString().split("\\)[\n \t]*\\.")[0]);
                                test.add(nodeCopy.toString().split("\\)[\n \t]*\\.")[0]);
                                test.add("");
                            }

                            if (hasMethodOrObjectDecl.size() == 0) hasMethodOrObjectDecl.add(true);
                        });

                /*This covers the case of variable declaration without setting it, for example
                * Data<String> d; If it is not a MethodCall nor an ObjectDecl, it is not necessary
                * to execute the code below.*/
                if (hasMethodOrObjectDecl.size() > 0) {
                    /*First method has to be handled here, after all possible changes in nodeCopy*/
                    String firstMethod = nodeCopy.toString().split("\\)[\n \t]*\\.")[0];

                    if (test.size() >= 1)
                        firstMethod = test.get(0);
                    if (test.size() == 2)
                        placeholder = test.get(1);


                    /*if (! firstMethod.endsWith(")"))
                        firstMethod += ")";*/
                    if (! firstMethod.matches(";[\\s\t\n]*$"))
                        firstMethod += ";";

                    String methodToAdd = "\t\t" + firstMethod;
                    System.out.println("METHOD TO ADD " + methodToAdd);

                    n.getRange().ifPresent(r -> apiStatements.put(beginLine, methodToAdd));
                    fileContentF.put(beginLine, placeholder);
                    fileContentA.put(beginLine, placeholder);


                    for (int i = beginLine + 1; i <= endLine; i++) {
                        apiStatements.put(i, "");
                        fileContentF.put(i, "");
                        fileContentA.put(i, "");
                    }
                }
                else {
                    String toReplace = Parser.mapFlinkTypes(n.toString(), n.toString());
                    fileContentF.put(beginLine, "\t\t" + toReplace + ";");
                    toReplace = Parser.mapApexTypes(n.toString(), n.toString());
                    fileContentA.put(beginLine, "\t\t" + toReplace + ";");
                }
            }
            return n;
        }
    }
}
