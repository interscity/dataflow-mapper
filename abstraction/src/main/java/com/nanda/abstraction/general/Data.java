package com.nanda.abstraction.general;

import com.nanda.abstraction.general.interfaces.FilterFunction;
import com.nanda.abstraction.general.interfaces.FlatMapFunction;
import com.nanda.abstraction.general.interfaces.MapFunction;
import com.nanda.abstraction.general.interfaces.ReduceFunction;

import javax.annotation.Nullable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;


public class Data<T> {

    @Nullable
    private boolean isStream;
    private String dataName;
    private String dataType;
    private String inputOperator;
    private String tagInputOperator; //name
    private String genericType;
    private UDF udf = new UDF();
    private DataTransformation dtTrans = new DataTransformation();

    /*User functions*/
    public Data(String processingType) { }
    //public Data(TP<String> processingType) { }

    /*These are the constructors used for the mapping*/

    public Data() { }

    public Data(String dataName, String dataType) {
        String dataTypeF, dataTypeA;
        this.dataName = dataName;
        this.dataType = dataType;

        dataTypeF = Parser.mapFlinkTypes(dataType, dataType);
        dataTypeA = Parser.mapApexTypes(dataType, dataType);
        /*ItemReceiver receiver = new ItemReceiver();
        receiver.setDataReceiver(dataName, dataTypeF, dataTypeA);*/
    }

    public Data(String processingType, String dataName, String dataType) {
        String dataTypeF, dataTypeA;
        this.isStream = (processingType.equals("stream") || processingType.equals("Stream"));
        this.dataName = dataName;
        this.dataType = dataType;

        dataTypeF = Parser.mapFlinkTypes(dataType, dataType);
        dataTypeA = Parser.mapApexTypes(dataType, dataType);
        ItemReceiver receiver = new ItemReceiver();
        receiver.setDataReceiverProcessing(dataName, dataTypeF, dataTypeA);
        //this.genericType = getGenericClassType();
    }

    /***
     *** Getters and setters
     ***/
    public String getGenericType() {
        return this.genericType;
    }

    private String getGenericClassType() {
        Type type = getClass().getGenericSuperclass();

        while (!(type instanceof ParameterizedType))
            type = ((Class<?>) type).getGenericSuperclass();

        return ((ParameterizedType) type).getActualTypeArguments()[0].getTypeName();
        /*final ParameterizedType type = (ParameterizedType) getClass().getGenericSuperclass();
        return type.getActualTypeArguments()[0].getTypeName();*/
    }

    public void setDataName(String dataName) { this.dataName = dataName; }

    public String getDataName() { return dataName; }

    public void setDataType(String dataType) { this.dataType = dataType; }

    public String getDataType() { return dataType; }

    /*Return this to allow chaining*/
    public <O> Data<O> setDataInfo(String dataName, String dataType, Data<O> d) {
        this.dataName = dataName;
        this.dataType = dataType;

        String dataTypeF = Parser.mapFlinkTypes(dataType, dataType);
        String dataTypeA = Parser.mapApexTypes(dataType, dataType);
        ItemReceiver receiver = new ItemReceiver();
        receiver.setDataReceiver(dataName, dataTypeF, dataTypeA);
        System.out.println(">>>>>>>>>>>>>>>>>> " + dataName + " >>>>>> " + dataType);

        return new Data<O>(dataName, dataType);
        //return this;
    }


    public void setIsStream(boolean isStream) { this.isStream = isStream; }

    public boolean getIsStream() { return this.isStream; }

    public void setInputOperator(String inputOperator) { this.inputOperator = inputOperator; }

    public String getInputOperator() { return inputOperator; }

    public void setTagInputOperator(String tagInputOperator) {
        this.tagInputOperator = tagInputOperator;
    }

    public String getTagInputOperator() { return tagInputOperator; }


    /***
     ***Transformations
     ***/
    public <O> Data<O> maxByKey() {
        this.dtTrans.maxByKey();
        return new Data<O>();
        //return this;
    }

    public Data maxByKey(String caller) {
        this.dtTrans.maxByKey(caller, this.dataType);
        return this;
    }

    public <O> Data<O> minByKey() {
        this.dtTrans.minByKey();
        return new Data<O>();
        //return this;
    }

    public Data minByKey(String caller) {
        this.dtTrans.minByKey(caller, this.dataType);
        return this;
    }

    public <O> Data<O> sum() {
        dtTrans.sum();
        return new Data<O>();
        //return this;
    }

    public Data sum(String caller) {
        this.dtTrans.sum(caller, this.dataType);
        return this;
    }

    public Data<T> print() {
        return this;
    }

    public Data<T> print(String dataName) {
        return this;
    }

    /*About the transformations below, it is necessary to watch out that
    reduce and fold functions must receive KeyedStreams instead of Data,
    i. e. they must have already been passed by a transformation before.
    * */

    public <O> Data<O> map(MapFunction<T, O> className) {
        Class<?> classObj = className.getClass();
        Object ret = classObj.getMethods()[0].getReturnType();
        return new Data<O>(ProjectSettings.processingChoice, this.dataName, ret + "");
    }

    public Data<T> map(MapFunction className, String instance) {
        udf.map(className, instance);
        return this;
    }

    public Data<T> map(UnaryOperator<T> unaryOperator) {
        return this;
    }


    /*Do another call, to treat the case the function is in a separated file*/

    /*It handles flatMap functions*/
    public <O> Data<O> flatMap(FlatMapFunction<T, O> className) {
        Class<?> classObj = className.getClass();
        Object ret = classObj.getMethods()[0].getReturnType();
        Data<O> d = new Data<O>(ProjectSettings.processingChoice, this.dataName, ret + "");
        d.setDataName(this.dataName);
        d.setDataType(this.dataType);

        return d;

        //return this;
    }

    public Data<T> flatMap(FlatMapFunction className, String instance) {
        udf.flatMap(className, instance);
        return this;
    }

    public Data<T> flatMap(UnaryOperator<T> unaryOperator) {
        return this;
    }

    /*It handles filter functions*/
    public Data<T> filter(FilterFunction className) { return this; }

    public Data<T> filter(FilterFunction className, String instance) {
        udf.filter(className, instance);
        return this;
    }

    public Data<T> filterLambda(UnaryOperator<T> unaryOperator) {
        return this;
    }


    /*It handles reduce functions*/
    public <T> Data<T> reduce(ReduceFunction<T> className) {
        Class<?> classObj = className.getClass();
        Object ret = classObj.getMethods()[0].getReturnType();
        return new Data<T>(ProjectSettings.processingChoice, this.dataName, ret + "");
    }

    public Data<T> reduce(ReduceFunction<T> className, String instance) {
        udf.reduce(className, instance);
        return this;
    }

    public Data<T> reduce(BinaryOperator<T> binaryOperator) {
        return this;
    }


    /*It handles fold functions*/
    public Data<T> fold(String className) {
        //udf.fold(className);
        return this;
    }

    public Data<T> fold(String className, String instance) {
        udf.fold(className, instance);
        return this;
    }


    /***
     ***Window Transformations
     ***/

    /*Method interface for tumbling time windows*/
    public <O> Data<O> timeWindow(Window window, Time time) {
        Data<O> d = new Data<O>(ProjectSettings.processingChoice, this.dataName, "");
        d.setDataName(this.dataName);
        d.setDataType(this.dataType);

        return d;
        //return this;
    }

    /*Method which calls method that handles tumbling time windows*/
    protected Data timeWindow(Window window, Time time, String instance) {
        window.timeWindow(time, instance);
        return this;
    }

    /*Method interface for sliding time windows*/
    public Data timeWindow(Window window, Time time, Time slide) {
        return this;
    }

    /*Method which calls method that handles sliding time windows*/
    protected Data timeWindow(Window window, Time time, Time slide, String caller) {
        window.timeWindow(time, slide, caller);
        return this;
    }


    /*****************Session Window*****************/
    /*Method interface for session windows*/
    public Data sessionWindow(Window window, Time gap) {
        return this;
    }

    /*Method which calls method that handles session windows*/
    protected Data sessionWindow(Window window, Time gap, String instance) {
        System.out.println("Inside Data sessionWindow: " + window.getWindowName());
        System.out.println("Inside Data sessionWindow: " + window.mp);
        window.sessionWindow(gap, instance);
        return this;
    }


    /*****************Count Window****************
     * Probably I will not implement count windows, due to Apex problems.
    /*Method interface for tumbling count windows*/
    public Data countWindow(Window window, long size) {
        return this;
    }

    /*Method which calls method that handles tumbling count windows*/
    protected Data countWindow(Window window, long size, String instance) {
        window.countWindow(size, instance);
        return this;
    }

    /*Method interface for tumbling count windows*/
    public Data countWindow(Window window, long size, long slide) {
        return this;
    }

    /*Method which calls method that handles tumbling count windows*/
    protected Data countWindow(Window window, long size, long slide, String instance) {
        window.countWindow(size, slide, instance);
        return this;
    }


    /*****************Mapping UDF parameters*****************/
    /*This method maps Data type parameters for Flink*/
    public static String flinkData(String streamOrBatch) {
        streamOrBatch = streamOrBatch.toLowerCase();
        switch (streamOrBatch) {
            case("batch"):
                return "DataSet";
            default:
                return "DataStream";
        }
    }

    /*This method maps Data type parameters for Apex*/
    public static String apexData() { return "ApexStream"; }


}
