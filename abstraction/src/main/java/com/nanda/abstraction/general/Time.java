package com.nanda.abstraction.general;


/***This class is used by KeyedWindow and Window to measure time for
 * time-based windows*/
public class Time {

    public String type;
    public long duration;

    public Time() {}

    public Time(String type, long duration) {
        this.type = type;
        this.duration = duration;
    }
    public static Time milis(long milis) {
        return new Time("milis", milis);
    }

    public static Time seconds(long seconds) {
        return new Time("seconds", seconds);
    }

    public static Time minutes(long minutes) {
        return new Time("minutes", minutes);
    }

    public static Time hours(long hours) {
        return new Time("hours", hours);
    }

    public static Time days(long days) {
        return new Time("days", days);
    }
}
