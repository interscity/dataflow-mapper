package com.nanda.abstraction.general;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;


public class ProjectSettings {

    /***************************************************
     * *This class is used by other classes to retrieve
     * which mapper was selected by the user.
     * *************************************************/

    public static String packageName;
    public static String className;
    public static String mapper;
    public static String processingChoice;

    public ProjectSettings(String mapper) {
        ProjectSettings.mapper = mapper;
    }

    public ProjectSettings(String mapper, String className) {
        ProjectSettings.mapper = mapper;
        ProjectSettings.className = className;
    }

    /**This set is useful when the user don't want to create a new project but use one that
    * already exists*/
    public void setPackageName(String packageName) {
        ProjectSettings.packageName = packageName;
    }

    private static void printProcess(Process p) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

        String line = "";
        while ((line = reader.readLine()) != null){
            System.out.println(line);
        }
    }

    private static Integer projectCreation(String name, String tool) throws Exception {

        Integer status = -1;
        Process p;
        tool = tool.toLowerCase();

        try {
            String command = " ../maven/generate_" + tool + "_project.sh";
            System.out.println("Please, wait some seconds...");
            ProcessBuilder pb = new ProcessBuilder("/bin/bash", command, name);
            p = pb.start();
            status = p.waitFor();
            if (status != 0)
                System.out.println("BUILD FAILURE");
            else {
                printProcess(p);
                ProjectSettings.packageName = name;
            }
        }catch(Exception e){
            System.out.println(e);
        }

        return status;
    }

    /**Creates the project with the specified name*/
    public void create(String name, String args[]) throws Exception {

        Integer status = -1, cont = 0;
        String tool = ProjectSettings.mapper;
        Scanner s = new Scanner(System.in);

        while (status != 0) {
            if (cont > 0) {
                System.out.println("Type a name for the application (that doesn't exist in your directory)");
                name = s.next();
            }
            status = projectCreation(name, tool);
            cont++;
        }
    }

    /**Before code generation start, it is necessary to clean previous intermediary files
     * with code of other projects. So, this method is some code cleaning.*/
    public void start(String processingChoice) {
        ProjectSettings.processingChoice = processingChoice;

        Auxiliar aux = new Auxiliar();
        aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/remove_interm_files.py"));

        /*Calls the parser.py*/
        //String callerFileName = new Exception().getStackTrace()[1].getFileName();

        if (mapper.toLowerCase().equals("flink") || mapper.toLowerCase().equals("both"))
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/setup.py", processingChoice, packageName, className));

        if (mapper.toLowerCase().equals("apex") || mapper.toLowerCase().equals("both"))
            aux.callScript(new ProcessBuilder("python3","../abstraction-python-scripts/pom-files/add_pom_dep.py", packageName, mapper, "apexStream"));
    }

    /**Finishes code generation, calling Manager.py to produce the final code*/
    public void finish() {
        Auxiliar aux = new Auxiliar();
        String mapper = ProjectSettings.mapper.toLowerCase();
        if (mapper.equals("flink") || mapper.equals("both"))
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/Manager.py", ProjectSettings.packageName, ProjectSettings.className, "flink"));
        if (mapper.equals("apex") || mapper.equals("both"))
            aux.callScript(new ProcessBuilder("python3", "../abstraction-python-scripts/Manager.py", ProjectSettings.packageName, ProjectSettings.className, "apex"));
    }

    public DataTransformation.MapperType getMapperType() {

        switch(ProjectSettings.mapper) {
            case ("flink"):
                return DataTransformation.MapperType.Flink;
            case ("apex"):
                return DataTransformation.MapperType.Apex;
            default:
                return DataTransformation.MapperType.Both;
        }
    }

}
