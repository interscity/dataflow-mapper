package com.nanda.abstraction.general.flink;

import com.nanda.abstraction.general.Auxiliar;
import com.nanda.abstraction.general.DataTransformation;
import com.nanda.abstraction.general.ProjectSettings;

public class DataTransformationF extends DataTransformation {



    public void maxByKey(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();

        String imports = "";
        String fileWriteContent = "\n//placeholder\n";
        if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
            fileWriteContent += ".maxBy(1);\n";
            //fileWriteContent += String.format("%s.keyBy(0).maxBy(1);\n", caller);
        else
            fileWriteContent += String.format("%s.groupBy(0).maxBy(1);\n", caller);

        String endPath = "../abstraction-python-scripts/flink/out/";
        aux.writeContent(endPath, imports, fileWriteContent);

    }


    public <T> void minByKey(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();

            String imports = "";
            String fileWriteContent = "\n//placeholder\n";
            if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
                fileWriteContent += ".minBy(1);\n";
                //fileWriteContent += String.format("%s.keyBy(0).minBy(1);\n", caller);
            else
                fileWriteContent += String.format("%s.groupBy(0).minBy(1);\n", caller);

            String endPath = "../abstraction-python-scripts/flink/out/";
            aux.writeContent(endPath, imports, fileWriteContent);
    }


    public <T> void sum(String caller, String dataType) {

        Auxiliar aux = new Auxiliar();

        String imports = "";
        String fileWriteContent = "\n//placeholder\n";
        if (ProjectSettings.processingChoice.toLowerCase().equals("stream"))
            fileWriteContent += ".sum(1);\n";
            //fileWriteContent += String.format("%s.keyBy(0).sum(1);\n\n", caller);
        else
            fileWriteContent += String.format("%s.groupBy(0).sum(1);\n", caller);

        String endPath = "../abstraction-python-scripts/flink/out/";
        aux.writeContent(endPath, imports, fileWriteContent);


    }
}
