package com.nanda.abstraction.general;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import com.github.javaparser.symbolsolver.javaparsermodel.JavaParserFacade;
import com.github.javaparser.symbolsolver.model.resolution.TypeSolver;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FlinkParser {

    private static final Pattern API_PACKAGE = Pattern.compile("com.nanda.abstraction.general");
    private static final Pattern PROJ_SETTINGS = Pattern.compile("ProjectSettings");
    private static final Pattern TIME_CLASS = Pattern.compile("Time");
    private static final Pattern DATA_CLASS = Pattern.compile("Data");
    private static final Pattern IO_DATA_DTRANS = Pattern.compile("IO|Data|DataTransformation");

    private TreeMap<Integer, String> apiStatements;
    private TreeMap<Integer, String> fileContentF;
    private static List<String> importsFlink;
    private String fileName;
    private static String streamOrBatch = "Stream";


    FlinkParser(TreeMap<Integer, String > apiStatements, TreeMap<Integer, String> fileContentF,
            List<String> importsFlink, String fileName, String streamOrBatch) {
        this.apiStatements = apiStatements;
        this.fileContentF = fileContentF;
        this.importsFlink = importsFlink;
        this.fileName = fileName;
        this.streamOrBatch = streamOrBatch;
    }

    protected void doMapping(CompilationUnit comp, TypeSolver typeSolver) {
        /*comp.accept(new VarDeclVisitor(), JavaParserFacade.get(typeSolver));
        comp.accept(new MethodCallVisitor(), JavaParserFacade.get(typeSolver));*/
        comp.accept(new MethodChangerVisitor(), null);
        comp.accept(new ClassChangerVisitor(), null);

        writeParsedF();
    }


    public static String mapFlinkTypes(String currentParam, String toReplace) {

        if (currentParam.contains("KV")) {
            toReplace = toReplace.replace("KV", KV.flinkKV());
            if (importsFlink != null && !importsFlink.contains("import org.apache.flink.api.java.tuple.Tuple2;"))
                importsFlink.add("import org.apache.flink.api.java.tuple.Tuple2;");
        }

        //Matcher proj = PROJ_SETTINGS.matcher(javaParserFacade.getType(n.getScope().orElse(null)).describe());

        if (currentParam.contains("Data"))
            toReplace = toReplace.replace("Data", Data.flinkData(streamOrBatch));
        else if (currentParam.contains("FileIn"))
            toReplace = toReplace.replace("FileIn", FileIn.flinkFileInput(streamOrBatch));
        else if (currentParam.contains("FileOut"))
            toReplace = toReplace.replace("FileOut", FileOut.flinkFileOutput(streamOrBatch));
        else if (currentParam.contains("KafkaIn"))
            toReplace = toReplace.replace("KafkaIn", KafkaIn.flinkKafkaInput(streamOrBatch));
        else if (currentParam.contains("KafkaOut"))
            toReplace = toReplace.replace("KafkaOut", KafkaOut.flinkKafkaOutput(streamOrBatch));

        return toReplace;
    }


    /*** Here can be done the logic for mapping UDFs signature*/
    private class ClassChangerVisitor extends VoidVisitorAdapter<Void> {
        @Override
        public void visit(ClassOrInterfaceDeclaration n, Void arg) {
            super.visit(n, arg);
            boolean found = false;
            boolean ignore = false;
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;
            String before = "";

            String func = n.getImplementedTypes().toString();

            if (! func.isEmpty()) {
                if (func.contains("MapFunction")) {
                    before = "MapFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.MapFunction;");
                }
                else if (func.contains("FlatMapFunction")) {
                    before = "FlatMapFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FlatMapFunction;");
                }
                else if (func.contains("ReduceFunction")) {
                    before = "ReduceFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.ReduceFunction;");
                }
                else if (func.contains("FilterFunction")) {
                    before = "FilterFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FilterFunction;");
                }
                else if (func.contains("FoldFunction")) {
                    before = "FoldFunction";
                    importsFlink.add("import org.apache.flink.api.common.functions.FoldFunction;");
                }
                else
                    ignore = true;

                if (!ignore) {
                    for (int i = beginLine; i <= endLine && !found; i++) {
                        String contentF = fileContentF.get(i);
                        if (contentF.contains(before)) {
                            /*Mapping parameters type for Flink. Split is done to separate each
                             * type present implements interface. For ex: MapFunction<String, Integer>*/
                            String[] parameters = func.split(",");
                            for (String param : parameters)
                                contentF = mapFlinkTypes(param, contentF);

                            fileContentF.put(beginLine, contentF);
                            found = true;
                        }
                    }
                }

            }
        }
    }

    private class MethodChangerVisitor extends ModifierVisitor<JavaParserFacade> {
        @Override
        public Node visit(MethodDeclaration n, JavaParserFacade arg) {
            super.visit(n, arg);
            int beginLine = n.getBegin().get().line;
            int endLine = n.getEnd().get().line;
            boolean found = false;
            String searchedMethod = n.getName().toString();

            /*Get the parameters together with its types*/
            NodeList parameters = n.getParameters();

            int line = beginLine;
            for (; line <= endLine && !found; line++)
                if (fileContentF.get(line).contains(searchedMethod))
                    found = true;

            line--;

            String toReplace = fileContentF.get(line);
            if (searchedMethod.equals("main") && !toReplace.contains("throws Exception"))
                toReplace = toReplace.replace("args)", "args) throws Exception");

            /*Mapping parameters type for Flink*/
            for (int i = 0; i < parameters.size(); i++) {
                String current = parameters.get(i).toString();
                toReplace = mapFlinkTypes(current, toReplace);
            }
            fileContentF.put(line, toReplace);

            return n;
        }
    }

    private void writeParsedF() {

        String line = "";
        BufferedWriter writer;

        try {
            /*It writes an output file for Flink*/
            String endPath = "../abstraction-python-scripts/flink/out/" + fileName + "ParsedF.java";
            writer = new BufferedWriter(new FileWriter(endPath));

            /*Package line*/
            writer.write("package com.nanda.$packageName;\n\n");

            if (! importsFlink.isEmpty())
                for (String item : importsFlink)
                    writer.write(item + "\n");

            Pattern publicClass = Pattern.compile("public\\s+class\\s+" + fileName);
            Pattern abstractionImport = Pattern.compile("com.nanda.abstraction.general");
            Pattern pojoImport = Pattern.compile("com.nanda.abstraction.pojo");
            Matcher abstractionMatch, pojoMatch;
            for (int i = 2; i <= fileContentF.size(); i++) {
                line = fileContentF.get(i);
                abstractionMatch = abstractionImport.matcher(line);
                pojoMatch = pojoImport.matcher(line);

                if (!abstractionMatch.find() && !pojoMatch.find()) {
                    line = mapFlinkTypes(line, line);
                    writer.write(line + "\n");
                }
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
