package com.nanda.abstraction.general;
import com.nanda.abstraction.general.interfaces.FilterFunction;

 public class Kafka2KafkaAPIParsed {

	 public static void main(String args[]) throws Exception {

		ProjectSettings project = new ProjectSettings("Both", "Kafka2Kafka");
		project.setPackageName("experiment");
		project.start("stream");
		Kafka<String> k = new Kafka<>("k", "String");
		Data<String> d = k.setDataInfo("d", "String", new Data<String>()).read(TP.build("group"), TP.build("localhost:9092"), TP.build("input"), TP.build("localhost:2181"), TP.build(true),"k");

		k.write(TP.build("localhost:9092"), TP.build("outputFull"), d,"k");
		Kafka k2 = new Kafka<>("k2", "null");
		k2.write(TP.build("localhost:9092"), TP.build("biggerThanThirty"), d,"k2");
		project.finish();
	}
}
