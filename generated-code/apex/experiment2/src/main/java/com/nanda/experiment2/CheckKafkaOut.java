package com.nanda.experiment2;

import com.datatorrent.api.DAG;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import org.apache.hadoop.conf.Configuration;

import static org.apache.apex.malhar.stream.api.Option.Options.name;

public class CheckKafkaOut implements StreamingApplication {

    @Override
    public void populateDAG(DAG dag, Configuration conf) {

        KafkaSinglePortStringInputOperator kafkaInput = new KafkaSinglePortStringInputOperator();
        ApexStream<String> kafkaContent = StreamFactory.fromInput(kafkaInput, kafkaInput.outputPort, name("kafkaIn1"));

        kafkaContent.print();

        kafkaContent.populateDag(dag);


    }
}