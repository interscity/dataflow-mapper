package com.nanda.experiment2;
import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import com.datatorrent.lib.math.MaxKeyVal;
import com.datatorrent.lib.math.MinKeyVal;
import com.datatorrent.lib.math.SumKeyVal;
import org.apache.apex.malhar.lib.window.WindowOption;
import org.apache.apex.malhar.lib.window.TriggerOption;
import org.joda.time.Duration;

import com.datatorrent.lib.util.KeyValPair;
import org.apache.apex.malhar.lib.window.accumulation.ReduceFn;
import org.apache.apex.malhar.lib.window.Tuple;
import org.apache.apex.malhar.lib.function.Function;
import org.apache.apex.malhar.lib.function.Function;
import org.apache.apex.malhar.lib.function.Function;
import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;

import com.datatorrent.lib.util.KeyValPair;
import org.apache.apex.malhar.lib.function.Function;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

@ApplicationAnnotation(name="StudyCase2")
public class StudyCase2Old implements StreamingApplication {

    private static TreeMap<String, BusPosLastStopPojo> lastStop = new TreeMap<>();
    private static ArrayList<StopPojo> stops = new ArrayList<>();
    private static TreeMap<String, StopPojo> stopsLineId = new TreeMap<>();
    private static TreeMap<String, String> busLineNames = new TreeMap<>();
    //key: stopId-lineId

    private static TreeMap<String, TimeTablePojo> timeTable = new TreeMap<>();

@Override
public void populateDAG(DAG dag, Configuration conf) {
				
	MaxKeyVal<String, Integer> maxOperator1 = new MaxKeyVal<>();

	MinKeyVal<String, Integer> minOperator1 = new MinKeyVal<>();

	SumKeyVal<String, Integer> sumOperator1 = new SumKeyVal<>();

        busLineNames.put("1450","TERM. BANDEIRA - TERM. GUARAPIRANGA");
        busLineNames.put("1465", "TERM. BANDEIRA - TERM. VARGINHA");
        busLineNames.put("1651", "TERM. MERCADO - TERM. SACOMÃ");
        busLineNames.put("198", "METRÔ STA. CRUZ - TERM. JD. ÂNGELA");
        busLineNames.put("32772","TERM. PRINC. ISABEL - TERM. STO. AMARO");
        busLineNames.put("34694", "PARAÍSO - TERM. CAMPO LIMPO");

        String stopsInfo = readFromFile("/tmp/stops_info_sorted.txt");
        stops = setStopPojo(stopsInfo);

        Double lon;
        Double lat;

        for (StopPojo stop : stops) {

            lon = stop.getLon();
            lat = stop.getLat();
            stopsLineId.put(stop.getStopId().trim() + "-" + stop.getLineId().trim(),
                    new StopPojo(stop.getStopId().trim(), stop.getStopName().trim(),
                            lon, lat,
                            stop.getLineId().trim(), stop.getNextStop().trim()));
        }

        String prevTimeInfo = readFromFile("/tmp/prev_metrics_time.txt");
        timeTable = setTimeTablePojo(prevTimeInfo);

		KafkaSinglePortStringInputOperator kafkaIn = new KafkaSinglePortStringInputOperator();
		ApexStream<String> kafkaContent = StreamFactory.fromInput(kafkaIn, kafkaIn.outputPort, name("kafkaIn1"));


		ApexStream<BusPosPojo> busPosInput = kafkaContent.map(new UpdateBusPosPojo());


        busPosInput
                .filter(new FilterByRadius())

                .map(new PrevTimeNextStop())
                .print();

		/*ApexStream<KeyValPair<String, Integer>> formattedBusPos =*/
		busPosInput.filter(new FilterByRadius())
                    .map(new FormatLineStopTime())
                    .window(new WindowOption.TimeWindows(Duration.standardSeconds(40)),
                            new TriggerOption().withEarlyFiringsAtEvery(Duration.standardSeconds(40)))
                    .reduceByKey(new Sum(), new ToKeyVal1())
                    .print();




        /*FileStringIntegerOutput fOut1 = new FileStringIntegerOutput();
		formattedBusPos.endWith(fOut1, fOut1.input, name("fOut1"));*/

		busPosInput.populateDag(dag);

    }

    /*** A reduce function to concat two strings together.*/
    public static class Sum extends ReduceFn<Integer> {
        @Override
        public Integer reduce(Integer value1, Integer value2) {

            return value1 + value2;


        }
    }

    private static String readFromFile(String fileName) {
        //String fileName = "../sptrans-data/stops_info_sorted.txt";
        System.out.println("File name is " + fileName);
        StringBuilder content = new StringBuilder("");
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line = "";
            while (line!= null) {
                line = bufferedReader.readLine();
                content.append(line);
                content.append("\n");
            }
        } catch (Exception e) {
            System.out.println("Java IO Exception: " + e.getMessage());
        }
        return content.toString();
    }

    //public TreeMap<String, StopPojo> setStopPojo(String values) {
    private static ArrayList<StopPojo> setStopPojo(String values) {
        ArrayList<StopPojo> allStops = new ArrayList<>();
        String[] lines = values.split("\n");
        for (String line : lines) {
            if (line.contains(",")) {
                String[] items = line.split(",");

                if (items.length >= 6)
                    allStops.add(new StopPojo(
                            items[0].trim(), items[1].trim(),
                            tryParseDouble(items[2].trim()),
                            tryParseDouble(items[3].trim()),
                            items[4].trim(), items[5].trim()
                    ));
            }
        }

        return allStops;
    }

    private static TreeMap<String, TimeTablePojo> setTimeTablePojo(String values) {
        TreeMap<String, TimeTablePojo> prevTime = new TreeMap<>();
        String[] lines = values.split("\n");
        for (String line : lines) {
            if (line.contains(",")) {
                String[] items = line.split(",");

                if (items.length >= 6) {
                    String stopId = items[0].trim();
                    String lineId = items[1].trim();
                    String key = stopId + "-" + lineId;

                    Long max = tryParseLong(items[2].trim());
                    Long min = tryParseLong(items[3].trim());
                    Long sum = tryParseLong(items[4].trim());
                    Integer cont = tryParseInt(items[5].trim());
                    Long avg = 0L;
                    if (sum != null && cont != null && cont != 0)
                        avg = sum/cont;

                    //stopId, lineId, maxTime, minTime, sum, cont, lastTimestamp
                    prevTime.put(key,
                            new TimeTablePojo(
                                    stopId, lineId,
                                    max, min, sum, cont, avg
                            ));
                }
            }
        }

        return prevTime;

    }

    private static Double tryParseDouble(String item) {
        try {
            return Double.parseDouble(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Long tryParseLong(String item) {
        try {
            return Long.parseLong(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Integer tryParseInt(String item) {
        try {
            return Integer.parseInt(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class UpdateBusPosPojo implements Function.MapFunction<String, BusPosPojo> {

        @Override
        public BusPosPojo f(String values) {
            if (values.charAt(0) == '"' && values.charAt(values.length() - 1) == '"')
                values = values.substring(1, values.length() - 1).trim();

            String[] items = values.split(",");

            //Replace by a TreeMap to get the value in O(1)

            BusPosPojo busPos = new BusPosPojo();

            if (items.length >= 5) {
                String busId = items[0].trim();
                String lineId = items[1].trim();

                busPos.setBusLineId(busId + lineId);
                busPos.setBusId(items[0].trim());
                busPos.setLineId(items[1].trim());
                busPos.setLastLat(items[2].trim());
                busPos.setLastLon(items[3].trim());
                busPos.setLastPosTimestamp(items[4].trim());

                String busLineId = busId + "-" + lineId;
                if (lastStop.containsKey(busLineId)) {
                    busPos.setLastStop(lastStop.get(busLineId).getLastStopId());
                    busPos.setLastStopTimestamp(lastStop.get(busLineId).getLastStopTimestamp());
                }

            }

            return busPos;
        }
    }

    public static class FilterByRadius implements Function.FilterFunction<BusPosPojo> {

        @Override
        public boolean f(BusPosPojo content) {
            //This is given in Km

            Double radius = 0.025;
            Boolean isInsideRadius = false;

            if (content.getLineId() == null || content.getLastLat() == null ||
                    content.getLastLon() == null)
                return isInsideRadius;

            String lineId = content.getLineId().trim();
            Double y = Double.parseDouble(content.getLastLat());
            Double x = Double.parseDouble(content.getLastLon());

            //Filter by line id, to restrict the received information only for stop of a given line

            ArrayList<StopPojo> stopsInfo = new ArrayList<>();

            for (StopPojo stop : stops)
                if (stop.getLineId().equals(lineId))
                    stopsInfo.add(stop);

            /*It checks for all stops of a line since we might receive an information
             *within a big interval of time (for example, some positions were not sent
             * to Olho Vivo) and then the bus is in a new travel, in a previous bus stop*/

            for (StopPojo el : stopsInfo) {

                Double center_y = el.getLat();
                Double center_x = el.getLon();
                if (!x.isNaN() && !y.isNaN() && center_x != null && center_y != null) {
                    if ((x - center_x) * (x - center_x) +  (y - center_y) * (y - center_y)
                            <  radius * radius) {
                        //It continues at the same bus stop
                        if (el.getStopId().equals(content.getLastStop())) {
                            isInsideRadius = false;
                            break;
                        }
                        isInsideRadius = true;
                        content.setLastStop(el.getStopId());
                        content.setLastStopTimestamp(content.getLastPosTimestamp());

                        System.out.println(String.format("Ônibus %s da linha %s na parada %s, %s",
                                content.getBusId(), busLineNames.get(content.getLineId().trim()),
                                el.getStopId(), content.getLastPosTimestamp().trim()));

                        String busLineId = content.getBusId() + "-" + lineId;

                        //Avoid adding unnecessary registers
                        if (lastStop.containsKey(busLineId)) {
                            lastStop.replace(busLineId,
                                    new BusPosLastStopPojo(content.getBusId(),
                                            lineId, el.getStopId(),
                                            content.getLastPosTimestamp().trim()));
                        } else {
                            lastStop.put(busLineId,
                                    new BusPosLastStopPojo(content.getBusId(),
                                            lineId, el.getStopId(),
                                            content.getLastPosTimestamp().trim()));
                        }

                        //content.setLastStop(lastStop.get(busLineId).getLastStopId());
                        //content.setLastStopTimestamp(lastStop.get(busLineId).getLastStopTimestamp());

                        break;
                    }
                }

            }

            return isInsideRadius;
        }
    }

    //public static class PrevTimeNextStop implements MapFunction<KeyValPair<BusPosPojo, BusPosPojo>> {
    public static class PrevTimeNextStop implements Function.MapFunction<BusPosPojo, String> {

        @Override
        public String f(BusPosPojo busPos) {

            if (busPos.getLastStop() == null || busPos.getLastPosTimestamp() == null) {
                System.out.println("Last stop or timestamp is null");
                return "";
            }

            StringBuilder previsions = new StringBuilder("");

            /*Since the bus is already in current stop, it is necessary to
             * find out the previsions for the other ones ahead. So, the start
             * point to consider is from the next stop, instead of current*/

            String currStop = busPos.getLastStop().trim();
            String lineId = busPos.getLineId().trim();
            String key = currStop + "-" + lineId;
            currStop = stopsLineId.get(key).getNextStop();

            String currTimestamp = busPos.getLastPosTimestamp().trim();
            LocalDateTime localDateTime = LocalDateTime.parse(currTimestamp, DateTimeFormatter.ISO_DATE_TIME);

            LocalDateTime prevTime = LocalDateTime
                    .of(localDateTime.getYear(), localDateTime.getMonth(),
                            localDateTime.getDayOfMonth(), localDateTime.getHour(),
                            localDateTime.getMinute());

            Long timeToAdd = 0L;
            String nextStop;
            String stopName;

            while (currStop != null && !currStop.equals("None")) {
                key = currStop + "-" + lineId;

                /*These four bus stops do not have any information about
                 * prevision time, so they will be skipped. This lack of
                 * info is because three files that compare two consecutive
                 * stops were empty and one of the stop was final. That is why
                 * 4 stops are out of info*/

                if (currStop.equals("3305797") || currStop.equals("3305796")
                        || currStop.equals("230009856") || currStop.equals("230009858"))
                    currStop = stopsLineId.get(key).getNextStop();

                else if (stopsLineId.containsKey(key) && timeTable.containsKey(key)) {
                    stopName = stopsLineId.get(key).getStopName();
                    nextStop = stopsLineId.get(key).getNextStop();
                    //timeToAdd += timeTable.get(key).getAvg();
                    timeToAdd = timeTable.get(key).getAvg();

                    prevTime = prevTime.plusMinutes(timeToAdd);

                    String currTime = String.format("%02d:%02d", prevTime.getHour(), prevTime.getMinute());

                    //line_name, stop_name and prevision time
                    /*System.out.println(
                            String.format("%s Line %s, stop %s, prevision of arrival time: %s",
                                    currTimestamp, lineId, stopName, currTime));*/
                    previsions.append(String.format("%s Line %s, stop %s, prevision of arrival time: %s\n",
                            currTimestamp, busLineNames.get(lineId), stopName, currTime));

                    currStop = nextStop;
                }
                else if (!stopsLineId.containsKey(key))
                    System.out.println("StopLineId " + key + " not found");
                else
                    System.out.println("Key " + key + " not found in timetable");
            }

            return previsions.toString();
        }
    }

    public static class FormatLineStopTime implements Function.MapFunction<BusPosPojo, KeyValPair<String, Integer>> {

        @Override
        public KeyValPair<String, Integer> f(BusPosPojo values) {

            String currTimestamp = values.getLastPosTimestamp().trim();
            LocalDateTime localDateTime = LocalDateTime.parse(currTimestamp, DateTimeFormatter.ISO_DATE_TIME);

            LocalDateTime prevTime = LocalDateTime
                    .of(localDateTime.getYear(), localDateTime.getMonth(),
                            localDateTime.getDayOfMonth(), localDateTime.getHour(),
                            localDateTime.getMinute());

            String time = prevTime.getMonth() + "-" +
                    prevTime.getDayOfMonth() + " " + prevTime.getHour() + "h";
            String lineStop = values.getLineId() + "-" + values.getLastStop();
            return new KeyValPair<>(lineStop + "-" + time, 1);
            //Maybe I can use a reduce here to maintain only different busId
            //If the values are different, count like 2, else like 1

        }
    }

	public static class ToKeyVal1 implements Function.ToKeyValue<KeyValPair<String, Integer>, String, Integer> {
		@Override
		public Tuple<KeyValPair<String, Integer>> f(KeyValPair<String, Integer> input) {
			return new Tuple.PlainTuple<KeyValPair<String, Integer>>(input);
		}
	}

}
