package com.nanda.experiment2;

import info.batey.kafka.unit.KafkaUnit;

import info.batey.kafka.unit.KafkaUnitRule;
import com.datatorrent.api.Attribute;
import org.apache.apex.api.EmbeddedAppLauncher;
import org.apache.apex.api.Launcher;
import org.apache.hadoop.conf.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import javax.validation.ConstraintViolationException;
import java.io.IOException;

public class StudyCase2Test {
    private static final Logger LOG = LoggerFactory.getLogger(StudyCase2Test.class);
    /*private static final String TOPIC = "hdfs2kafka";
    private static final String directory = "target/hdfs2kafka";
    private static final String FILE_NAME = "messages.txt";*/

    private static final int zkPort = 2181;
    private static final int  brokerPort = 9092;
    private static final String BROKER = "localhost:" + brokerPort;

    @Rule
	public KafkaUnitRule kafkaUnitRule = new KafkaUnitRule(zkPort, brokerPort);


    @Test
    public void testApplication() throws IOException, Exception {
    try {

        	KafkaUnit ku1 = kafkaUnitRule.getKafkaUnit();
		ku1.createTopic("example");

			KafkaUnit kuOut1 = kafkaUnitRule.getKafkaUnit();
		kuOut1.createTopic("filtered");

		/*placeholderKafka*/



        Launcher.AppHandle ah = asyncRun();
        ah.shutdown(Launcher.ShutdownMode.KILL);
    } catch (ConstraintViolationException e) {
        Assert.fail("constraint violations: " + e.getConstraintViolations());
      }
    }

    private Launcher.AppHandle asyncRun() throws Exception {
        EmbeddedAppLauncher<?> launcher = Launcher.getLauncher(Launcher.LaunchMode.EMBEDDED);
        Attribute.AttributeMap launchAttributes = new Attribute.AttributeMap.DefaultAttributeMap();
        launchAttributes.put(EmbeddedAppLauncher.RUN_ASYNC, true);
        Configuration conf = getConfig();
        Launcher.AppHandle appHandle = launcher.launchApp(new StudyCase2(), conf, launchAttributes);
        Thread.sleep(100000);
        return appHandle;
    }

    private Configuration getConfig() {
        Configuration conf = new Configuration(false);
        conf.addResource(this.getClass().getResourceAsStream("/META-INF/properties-StudyCase2.xml"));
        //conf.set("dt.operator.lines.prop.directory", directory);
        return conf;
    }


}