package com.nanda.experiment;

import com.datatorrent.lib.db.jdbc.*;
import com.datatorrent.lib.util.FieldInfo;

import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import com.google.common.collect.Lists;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;

import org.apache.apex.malhar.stream.api.ApexStream;

import java.util.HashMap;
import java.util.List;

import static org.apache.apex.malhar.stream.api.Option.Options.name;


@ApplicationAnnotation(name="CreateLine")
public class CreateLine implements StreamingApplication {
    @Override
    public void populateDAG(DAG dag, Configuration conf) {

        System.out.println("Inside Dataflow");

        HashMap<String, Object> tupleTypes = new HashMap<>();

        tupleTypes.put("line_id", String.class);

        tupleTypes.put("line_origin", String.class);

        tupleTypes.put("line_destination", String.class);

        tupleTypes.put("line_way", String.class);


        JdbcPOJOPollInputOperator j = new JdbcPOJOPollInputOperator();
        j.setStore(new JdbcStore());
        j.setFieldInfos(addFieldInfos1());
        ApexStream<Object> dt = StreamFactory.fromInput(j, j.outputPort, name("jdbcIn1"));

        /*$placeholderVariableDeclarationExpr*/

        dt.print();

    }

    private List<FieldInfo> addFieldInfos1() {
        List<FieldInfo> fieldInfos = Lists.newArrayList();
        fieldInfos.add(new FieldInfo("		", "line_destination", FieldInfo.SupportType.STRING));
        fieldInfos.add(new FieldInfo("		", "line_way", FieldInfo.SupportType.STRING));
        fieldInfos.add(new FieldInfo("		", "line_origin", FieldInfo.SupportType.STRING));
        fieldInfos.add(new FieldInfo("		", "line_id", FieldInfo.SupportType.STRING));
        return fieldInfos;
    }

}
