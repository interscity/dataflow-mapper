package com.nanda.experiment;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import com.datatorrent.contrib.kafka.KafkaSinglePortOutputOperator;
import com.datatorrent.contrib.kafka.KafkaSinglePortStringInputOperator;

import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;

@ApplicationAnnotation(name="FileKafkaFile")
public class FileKafkaFile implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

        System.out.println("Inside Dataflow");

		LineByLineFileInputOperator f = new LineByLineFileInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(f, f.output, name("f"));


        KafkaSinglePortOutputOperator<String,String> kOut = new KafkaSinglePortOutputOperator<>();
		d.endWith(kOut, kOut.inputPort, name("kafkaOut1"));


		KafkaSinglePortStringInputOperator k2 = new KafkaSinglePortStringInputOperator();
		ApexStream<String> d2 = StreamFactory.fromInput(k2, k2.outputPort, name("kafkaIn1"));

        //d2.print();

		//LineOutputOperator<String> f2Out = new LineOutputOperator<>();
        FileStringOutput f2Out = new FileStringOutput();
		d2.endWith(f2Out, f2Out.input, name("f2Out"));

		d.populateDag(dag);
		d2.populateDag(dag);
    }
}
