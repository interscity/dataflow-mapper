package com.nanda.experiment;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;

import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@ApplicationAnnotation(name="Hdfs2Hdfs")
public class Hdfs2Hdfs implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

		LineByLineFileInputOperator f = new LineByLineFileInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(f, f.output, name("f"));


        d = d.map(new CalcDiff());

        LineOutputOperator<String> fOut = new LineOutputOperator<>();
		d.endWith(fOut, fOut.input, name("fOut"));

        d.print();

		d.populateDag(dag);

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements Function.MapFunction<String, String> {

        @Override
        public String f(String values) {

            String[] items = values.split(",");

            String line;
            String stop1;
            String stop2;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                stop1 = items[1].trim();
                Date time1 = tryParseDate(items[2].trim());
                stop2 = items[3].trim();
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + "-" + stop1 + "-" + stop2 + ", " +
                    diff + ", " + timestamp;
        }
    }
}

