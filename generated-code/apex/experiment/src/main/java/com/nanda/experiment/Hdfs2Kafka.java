package com.nanda.experiment;
import static org.apache.apex.malhar.stream.api.Option.Options.name;
import org.apache.apex.malhar.lib.fs.LineByLineFileInputOperator;
import org.apache.apex.malhar.stream.api.ApexStream;
import org.apache.apex.malhar.stream.api.impl.StreamFactory;
import com.datatorrent.contrib.kafka.KafkaSinglePortOutputOperator;

import org.apache.apex.malhar.lib.function.Function;
import com.datatorrent.api.StreamingApplication;
import com.datatorrent.api.annotation.ApplicationAnnotation;
import org.apache.hadoop.conf.Configuration;
import com.datatorrent.api.DAG;

@ApplicationAnnotation(name="Hdfs2Kafka")
public class Hdfs2Kafka implements StreamingApplication {

@Override
public void populateDAG(DAG dag, Configuration conf) {

        System.out.println("Inside Dataflow");

		LineByLineFileInputOperator f = new LineByLineFileInputOperator();
		ApexStream<String> d = StreamFactory.fromInput(f, f.output, name("f"));


        d.filter(new Splitter());

        d.print();

        KafkaSinglePortOutputOperator<String,String> kOut = new KafkaSinglePortOutputOperator<>();
        d.endWith(kOut, kOut.inputPort, name("kafkaOut1"));

		d.populateDag(dag);

    }

    public static class Splitter implements Function.FilterFunction<String> {

        @Override
        public boolean f(String values) {
            String[] items = values.split(",");
            for (String i : items)
                System.out.println("===>" + i);
            return true;
        }
    }
}
