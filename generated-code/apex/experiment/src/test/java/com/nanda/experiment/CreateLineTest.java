/*package com.nanda.novinho2;

import com.datatorrent.api.Attribute;
import com.datatorrent.lib.db.jdbc.JdbcTransactionalStore;
import org.apache.apex.api.EmbeddedAppLauncher;
import org.apache.apex.api.Launcher;
import org.apache.hadoop.conf.Configuration;
import org.junit.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreateLineTest {
    private static final Logger LOG = LoggerFactory.getLogger(CreateLineTest.class);

    @Test
    public void testApplication() throws IOException, Exception {
        try {
            Launcher.AppHandle ah = asyncRun();
            Thread.sleep(10000);

            ah.shutdown(Launcher.ShutdownMode.KILL);
        } catch (ConstraintViolationException e) {
            Assert.fail("constraint violations: " + e.getConstraintViolations());
        }
    }

    private Launcher.AppHandle asyncRun() throws Exception {
        EmbeddedAppLauncher<?> launcher = Launcher.getLauncher(Launcher.LaunchMode.EMBEDDED);
        Attribute.AttributeMap launchAttributes = new Attribute.AttributeMap.DefaultAttributeMap();
        launchAttributes.put(EmbeddedAppLauncher.RUN_ASYNC, true);
        Configuration conf = getConfig();
        Launcher.AppHandle appHandle = launcher.launchApp(new CreateLine(), conf, launchAttributes);
        Thread.sleep(100);
        return appHandle;
    }

    private Configuration getConfig() {
        Configuration conf = new Configuration(false);
        conf.addResource(this.getClass().getResourceAsStream("/META-INF/properties-$className.xml"));
        // conf.set("dt.operator.lines.prop.directory", directory);
        return conf;
    }

    @BeforeClass
    public static void setup() {
        try {
            Class.forName("org.postgresql.Driver").newInstance();

            Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/source?user=guest&password=guest");
            System.out.println("Connecting to the database...");
            System.out.println(con.toString());
            Statement stmt = con.createStatement();

            String createMetaTable = "CREATE TABLE IF NOT EXISTS " + JdbcTransactionalStore.DEFAULT_META_TABLE + " ( "
                    + JdbcTransactionalStore.DEFAULT_APP_ID_COL + " VARCHAR(100) NOT NULL, "
                    + JdbcTransactionalStore.DEFAULT_OPERATOR_ID_COL + " INT NOT NULL, "
                    + JdbcTransactionalStore.DEFAULT_WINDOW_COL + " BIGINT NOT NULL, " + "UNIQUE ("
                    + JdbcTransactionalStore.DEFAULT_APP_ID_COL + ", " + JdbcTransactionalStore.DEFAULT_OPERATOR_ID_COL + ", "
                    + JdbcTransactionalStore.DEFAULT_WINDOW_COL + ") " + ")";

            stmt.executeUpdate(createMetaTable);

            stmt.executeUpdate("create table bus_line;");
            stmt.executeUpdate("select * from bus_line;");

            //placeholder
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}*/
