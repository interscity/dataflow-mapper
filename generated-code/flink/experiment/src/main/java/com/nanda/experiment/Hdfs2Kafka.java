package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;

import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.api.common.serialization.SimpleStringSchema;

import org.apache.flink.api.common.functions.FilterFunction;

public class Hdfs2Kafka {

    public static void main(String[] args) throws Exception {

        System.out.println("Inside Dataflow");

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = Hdfs2Kafka.class.getResourceAsStream("/Hdfs2Kafka.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.readTextFile("hdfs://localhost:8020/tmp/sample.txt");


        d.filter(new Splitter());

        d.addSink(new FlinkKafkaProducer011<>("example", new SimpleStringSchema(), prop));


    }

    public static class Splitter implements FilterFunction<String> {

        @Override
        public boolean filter(String values) {
            String[] items = values.split(",");
            for (String i : items)
                System.out.println("===>" + i);
            return true;
        }
    }
}
