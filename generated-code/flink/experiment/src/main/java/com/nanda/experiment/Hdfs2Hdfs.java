package com.nanda.experiment;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.core.fs.FileSystem;

import org.apache.flink.api.common.functions.MapFunction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Hdfs2Hdfs {

    public static void main(String[] args) throws Exception {

		final ExecutionEnvironment env = ExecutionEnvironment.getExecutionEnvironment();

        //InputStream inputStream = Hdfs2Hdfs.class.getResourceAsStream("/Hdfs2Hdfs.properties");

        //Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataSet<String> d = env.readTextFile("hdfs://localhost:8020/tmp/201906/calcdiff/");


        d = d.map(new CalcDiff());

        d.print();

        d.writeAsText("hdfs://localhost:8020/tmp/output/201906/flink_prev_june.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        env.execute();

    }

    private static Date tryParseDate(String item) {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {
            return format.parse(item);
        } catch (ParseException e) {
            return null;
        }
    }

    public static class CalcDiff implements MapFunction<String, String> {

        @Override
        public String map(String values) {

            String[] items = values.split(",");

            String line;
            String stop1;
            String stop2;
            Long diff = 0L;
            String timestamp;

            if (items.length == 6) {
                line = items[0].trim();
                stop1 = items[1].trim();
                Date time1 = tryParseDate(items[2].trim());
                stop2 = items[3].trim();
                Date time2 = tryParseDate(items[4].trim());

                //Division by 60000 converts from millis to minutes
                if (time1 != null && time2 != null) {
                    if (time2.getTime() > time1.getTime())
                        diff = (time2.getTime() - time1.getTime()) / 60000;
                    else
                        diff = (time1.getTime() - time2.getTime()) / 60000;
                }
                timestamp = items[5].trim();
            }
            else
                return "";

            return line + "-" + stop1 + "-" + stop2 + ", " +
                    diff + ", " + timestamp;
        }
    }
}

