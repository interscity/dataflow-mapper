package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.core.fs.FileSystem;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;

public class FileMinMaxFile {

    public static void main(String[] args) throws Exception {

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = FileMinMaxFile.class.getResourceAsStream("/FileMinMaxFile.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.readTextFile("/tmp/201904/prev/201904_flink.txt");


		DataStream<Tuple2<String, Double>> d2 = d.map(new Example());


        d2.keyBy(0).minBy(1).print();


        d2.writeAsText("/tmp/output/201904/prev/min_flink.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


        d2.keyBy(0).maxBy(1).print();


        d2.writeAsText("/tmp/output/201904/prev/max_flink.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


    }

    public static class Example implements MapFunction<String, Tuple2<String, Double>> {
        @Override
        public Tuple2<String, Double> map(String value) {
            String[] items = value.split(",");
            return new Tuple2<>(items[0], tryParseTime(items[1].trim()));
        }
    }

    private static Double tryParseTime(String inputItem) {
        try {
            return Double.parseDouble(inputItem);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
