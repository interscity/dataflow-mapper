package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;

import org.apache.flink.api.common.functions.FilterFunction;

public class Kafka2Kafka {

    public static void main(String[] args) throws Exception {

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = Kafka2Kafka.class.getResourceAsStream("/Kafka2Kafka.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.addSource(new FlinkKafkaConsumer011<>("input", new SimpleStringSchema(), prop).setStartFromLatest());


        d.addSink(new FlinkKafkaProducer011<>("outputFull", new SimpleStringSchema(), prop));


        d = d.filter(new BiggerThanThirty());

        d.addSink(new FlinkKafkaProducer011<>("biggerThanThirty", new SimpleStringSchema(), prop));


        d.print();

    }

    public static class BiggerThanThirty implements FilterFunction<String> {

        @Override
        public boolean filter(String content) {
            if (!content.contains(","))
                return false;

            String max = content.split(",")[1].trim();

            if (Integer.parseInt(max) < 30)
                return false;

            return true;
        }
    }
}
