package com.nanda.experiment;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;

public class FileKafkaFile {

    public static void main(String[] args) throws Exception {

        System.out.println("Inside Dataflow");

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = FileKafkaFile.class.getResourceAsStream("/FileKafkaFile.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


		DataStream<String> d = env.readTextFile("hdfs://localhost:8020/tmp/sample.txt");


        d.addSink(new FlinkKafkaProducer011<>("example", new SimpleStringSchema(), prop));


		DataStream<String> d2 = env.addSource(new FlinkKafkaConsumer011<>("example", new SimpleStringSchema(), prop).setStartFromEarliest());


        //d2.print();

        d2.writeAsText("/tmp/output.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);

        env.execute();
    }
}
