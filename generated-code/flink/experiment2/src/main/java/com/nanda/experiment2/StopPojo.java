package com.nanda.experiment2;

public class StopPojo {

	private String stopId;
	private String stopName;
	private Double lat;
	private Double lon;
	private String lineId;
	private String nextStop;

	public StopPojo() {};


	public StopPojo(String stopId, String stopName, Double lat, Double lon, String lineId, String nextStop) {

		this.nextStop = nextStop;
		this.stopId = stopId;
		this.lineId = lineId;
		this.lon = lon;
		this.stopName = stopName;
		this.lat = lat;
	}
	public String getNextStop() { return nextStop; }
	public void setNextStop(String nextStop) { this.nextStop = nextStop; }

	public String getStopId() { return stopId; }
	public void setStopId(String stopId) { this.stopId = stopId; }

	public String getLineId() { return lineId; }
	public void setLineId(String lineId) { this.lineId = lineId; }

	public Double getLon() { return lon; }
	public void setLon(Double lon) { this.lon = lon; }

	public String getStopName() { return stopName; }
	public void setStopName(String stopName) { this.stopName = stopName; }

	public Double getLat() { return lat; }
	public void setLat(Double lat) { this.lat = lat; }

	public String toString() {
		return stopId + "," + stopName + "," + lat + "," + lon + "," + lineId + "," + nextStop;
	}

}