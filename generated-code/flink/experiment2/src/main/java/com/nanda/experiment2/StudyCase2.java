package com.nanda.experiment2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.api.java.utils.ParameterTool;
import java.io.InputStream;
import java.util.Properties;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.windowing.time.Time;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.MapFunction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class StudyCase2 {

    private static final Logger logger = Logger.getLogger(StudyCase2.class.getName());

    private static TreeMap<String, BusPosLastStopPojo> lastStop = new TreeMap<>();
    private static ArrayList<StopPojo> stops = new ArrayList<>();
    private static TreeMap<String, StopPojo> stopsLineId = new TreeMap<>();
    private static TreeMap<String, String> busLineNames = new TreeMap<>();
    //key: stopId-lineId

    private static TreeMap<String, TimeTablePojo> timeTable = new TreeMap<>();

    public static void main(String[] args) throws Exception {

		StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        InputStream inputStream = StudyCase2.class.getResourceAsStream("/StudyCase2.properties");

        Properties prop = ParameterTool.fromPropertiesFile(inputStream).getProperties();


        busLineNames.put("1450","TERM. BANDEIRA - TERM. GUARAPIRANGA");
        busLineNames.put("1465", "TERM. BANDEIRA - TERM. VARGINHA");
        busLineNames.put("1651", "TERM. MERCADO - TERM. SACOMÃ");
        busLineNames.put("198", "METRÔ STA. CRUZ - TERM. JD. ÂNGELA");
        busLineNames.put("32772","TERM. PRINC. ISABEL - TERM. STO. AMARO");
        busLineNames.put("34694", "PARAÍSO - TERM. CAMPO LIMPO");

        String stopsInfo = readFromFile("/tmp/stops_info_sorted.txt");
        stops = setStopPojo(stopsInfo);

        Double lon;
        Double lat;

        for (StopPojo stop : stops) {

            lon = stop.getLon();
            lat = stop.getLat();
            stopsLineId.put(stop.getStopId().trim() + "-" + stop.getLineId().trim(),
                    new StopPojo(stop.getStopId().trim(), stop.getStopName().trim(),
                            lon, lat,
                            stop.getLineId().trim(), stop.getNextStop().trim()));
        }

        String prevTimeInfo = readFromFile("/tmp/prev_metrics_time.txt");
        timeTable = setTimeTablePojo(prevTimeInfo);

		DataStream<String> kafkaContent = env.addSource(new FlinkKafkaConsumer011<>("example", new SimpleStringSchema(), prop).setStartFromLatest());


		DataStream<BusPosPojo> busPosInput = kafkaContent.map(new UpdateBusPosPojo())

.filter(new FilterByRadius());

		DataStream<String> formattedBusPos = busPosInput.map(new PrevTimeNextStop());


        formattedBusPos.addSink(new FlinkKafkaProducer011<>("filtered", new SimpleStringSchema(), prop));


        formattedBusPos.writeAsText("/tmp/output.txt", FileSystem.WriteMode.OVERWRITE).setParallelism(1);


        busPosInput.map(new FormatLineStopTime())
                        .keyBy(0).timeWindow(Time.seconds(10))

                        .reduce(new Sum())

                        .print();

        env.execute();

    }

    /*** A reduce function to concat two strings together.*/
    public static class Sum implements ReduceFunction<Tuple2<String, Integer>> {
        @Override
        public Tuple2<String, Integer> reduce(Tuple2<String, Integer> value1, Tuple2<String, Integer> value2) {

            return new Tuple2<>(value1.f0, value1.f1 + value2.f1);



        }
    }

    private static String readFromFile(String fileName) {
        logger.log(Level.INFO, "File name is {0}", fileName);
        StringBuilder content = new StringBuilder("");
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(fileName));

            try {
                String line = "";
                while (line != null) {
                    line = bufferedReader.readLine();
                    content.append(line);
                    content.append("\n");
                }
            } finally {
                bufferedReader.close();
            }
        }
        catch(IOException e) {
            logger.log(Level.SEVERE, "Problem occured : {0}", e.getMessage());
        }
        return content.toString();
    }

    //public TreeMap<String, StopPojo> setStopPojo(String values) {
    private static ArrayList<StopPojo> setStopPojo(String values) {
        ArrayList<StopPojo> allStops = new ArrayList<>();
        String[] lines = values.split("\n");
        for (String line : lines) {
            if (line.contains(",")) {
                String[] items = line.split(",");

                if (items.length >= 6)
                    allStops.add(new StopPojo(
                            items[0].trim(), items[1].trim(),
                            tryParseDouble(items[2].trim()),
                            tryParseDouble(items[3].trim()),
                            items[4].trim(), items[5].trim()
                    ));
            }
        }

        return allStops;
    }

    private static TreeMap<String, TimeTablePojo> setTimeTablePojo(String values) {
        TreeMap<String, TimeTablePojo> prevTime = new TreeMap<>();
        String[] lines = values.split("\n");
        for (String line : lines) {
            if (line.contains(",")) {
                String[] items = line.split(",");

                if (items.length >= 6) {
                    String stopId = items[0].trim();
                    String lineId = items[1].trim();
                    String key = stopId + "-" + lineId;

                    Long max = tryParseLong(items[2].trim());
                    Long min = tryParseLong(items[3].trim());
                    Long sum = tryParseLong(items[4].trim());
                    Integer cont = tryParseInt(items[5].trim());
                    Long avg = 0L;
                    if (sum != null && cont != null && cont != 0)
                        avg = sum/cont;

                    //stopId, lineId, maxTime, minTime, sum, cont, lastTimestamp
                    prevTime.put(key,
                            new TimeTablePojo(
                                    stopId, lineId,
                                    max, min, sum, cont, avg
                            ));
                }
            }
        }

        return prevTime;

    }

    private static Double tryParseDouble(String item) {
        try {
            return Double.parseDouble(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Long tryParseLong(String item) {
        try {
            return Long.parseLong(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private static Integer tryParseInt(String item) {
        try {
            return Integer.parseInt(item);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static class UpdateBusPosPojo implements MapFunction<String, BusPosPojo> {

        @Override
        public BusPosPojo map(String values) {

            if (values.charAt(0) == '"' && values.charAt(values.length() - 1) == '"')
                values = values.substring(1, values.length() - 1).trim();

            String[] items = values.split(",");

            //Replace by a TreeMap to get the value in O(1)

            BusPosPojo busPos = new BusPosPojo();

            if (items.length >= 5) {
                String busId = items[0].trim();
                String lineId = items[1].trim();

                busPos.setBusLineId(busId + lineId);
                busPos.setBusId(items[0]);
                busPos.setLineId(items[1]);
                busPos.setLastLat(items[2]);
                busPos.setLastLon(items[3]);
                busPos.setLastPosTimestamp(items[4]);

                String busLineId = busId + "-" + lineId;
                if (lastStop.containsKey(busLineId)) {
                    busPos.setLastStop(lastStop.get(busLineId).getLastStopId());
                    busPos.setLastStopTimestamp(lastStop.get(busLineId).getLastStopTimestamp());
                }

            }

            return busPos;
        }
    }

    public static class FilterByRadius implements FilterFunction<BusPosPojo> {

        @Override
        public boolean filter(BusPosPojo content) {
            //This is given in Km

            Double radius = 0.025;
            Boolean isInsideRadius = false;

            if (content.getLineId() == null || content.getLastLat() == null ||
                    content.getLastLon() == null)
                return false;

            String lineId = content.getLineId().trim();
            Double y = Double.parseDouble(content.getLastLat());
            Double x = Double.parseDouble(content.getLastLon());

            //Filter by line id, to restrict the received information only for stop of a given line

            ArrayList<StopPojo> stopsInfo = new ArrayList<>();

            for (StopPojo stop : stops)
                if (stop.getLineId().equals(lineId))
                    stopsInfo.add(stop);

            /*It checks for all stops of a line since we might receive an information
             *within a big interval of time (for example, some positions were not sent
             * to Olho Vivo) and then the bus is in a new travel, in a previous bus stop*/

            String busLineId = content.getBusId().trim() + "-" + lineId;
            for (StopPojo el : stopsInfo) {

                Double center_y = el.getLat();
                Double center_x = el.getLon();
                if (!x.isNaN() && !y.isNaN() && center_x != null && center_y != null) {
                    if ((x - center_x) * (x - center_x) +  (y - center_y) * (y - center_y)
                            <  radius * radius) {
                        //It continues at the same bus stop
                        if ((lastStop.get(busLineId) != null &&
                                el.getStopId().equals(lastStop.get(busLineId).getLastStopId())) ||
                                (el.getStopId().equals(content.getLastStop()))) {
                            isInsideRadius = false;
                            break;
                        }

                        isInsideRadius = true;
                        content.setLastStop(el.getStopId());
                        content.setLastStopTimestamp(content.getLastPosTimestamp());

                        logger.finer(String.format("Ônibus %s da linha %s na parada %s, %s",
                                content.getBusId(), busLineNames.get(content.getLineId().trim()),
                                el.getStopId(), content.getLastPosTimestamp()));

                        //Avoid adding unnecessary registers
                        if (lastStop.containsKey(busLineId)) {
                            lastStop.replace(busLineId,
                                    new BusPosLastStopPojo(content.getBusId(),
                                            lineId, el.getStopId(),
                                            content.getLastPosTimestamp()));
                        } else {
                            lastStop.put(busLineId,
                                    new BusPosLastStopPojo(content.getBusId(),
                                            lineId, el.getStopId(),
                                            content.getLastPosTimestamp()));
                        }

                        //content.setLastStop(lastStop.get(busLineId).getLastStopId());
                        //content.setLastStopTimestamp(lastStop.get(busLineId).getLastStopTimestamp());

                        break;
                    }
                }

            }

            return isInsideRadius;
        }
    }

    public static class PrevTimeNextStop implements MapFunction<BusPosPojo, String> {

        @Override
        public String map(BusPosPojo busPos) {

            if (busPos.getLastStop() == null || busPos.getLastPosTimestamp() == null) {
                logger.log(Level.INFO, "Last stop or timestamp is null");
                return "";
            }

            StringBuilder previsions = new StringBuilder("");

            /*Since the bus is already in current stop, it is necessary to
             * find out the previsions for the other ones ahead. So, the start
             * point to consider is from the next stop, instead of current*/

            String currStop = busPos.getLastStop().trim();
            String lineId = busPos.getLineId().trim();
            String key = currStop + "-" + lineId;
            currStop = stopsLineId.get(key).getNextStop();

            String currTimestamp = busPos.getLastPosTimestamp().trim();
            LocalDateTime localDateTime = LocalDateTime.parse(currTimestamp, DateTimeFormatter.ISO_DATE_TIME);

            LocalDateTime prevTime = LocalDateTime
                    .of(localDateTime.getYear(), localDateTime.getMonth(),
                            localDateTime.getDayOfMonth(), localDateTime.getHour(),
                            localDateTime.getMinute());

            Long timeToAdd = 0L;
            String nextStop;
            String stopName;

            while (currStop != null && !currStop.equals("None")) {
                key = currStop + "-" + lineId;

                /*These four bus stops do not have any information about
                 * prevision time, so they will be skipped. This lack of
                 * info is because three files that compare two consecutive
                 * stops were empty and one of the stop was final. That is why
                 * 4 stops are out of info*/

                if (currStop.equals("3305797") || currStop.equals("3305796")
                        || currStop.equals("230009856") || currStop.equals("230009858"))
                    currStop = stopsLineId.get(key).getNextStop();

                else if (stopsLineId.containsKey(key) && timeTable.containsKey(key)) {
                    stopName = stopsLineId.get(key).getStopName();
                    nextStop = stopsLineId.get(key).getNextStop();
                    //timeToAdd += timeTable.get(key).getAvg();
                    timeToAdd = timeTable.get(key).getAvg();

                    prevTime = prevTime.plusMinutes(timeToAdd);

                    String currTime = String.format("%02d:%02d", prevTime.getHour(), prevTime.getMinute());

                    //line_name, stop_name and prevision time
                    /*System.out.println(
                            String.format("%s Line %s, stop %s, prevision of arrival time: %s",
                                    currTimestamp, lineId, stopName, currTime));*/
                    previsions.append(String.format("%s Line %s, stop %s, prevision of arrival time: %s\n",
                            currTimestamp, busLineNames.get(lineId), stopName, currTime));

                    currStop = nextStop;
                }
                else if (!stopsLineId.containsKey(key))
                    logger.log(Level.INFO, "StopLineId {0} not found", key);
                else
                    logger.log(Level.INFO, "Key {0} not found in timetable", key);
            }

            return previsions.toString();
        }
    }

    public static class FormatLineStopTime implements MapFunction<BusPosPojo, Tuple2<String, Integer>> {

        @Override
        public Tuple2<String, Integer> map(BusPosPojo values) {

            String currTimestamp = values.getLastPosTimestamp().trim();
            LocalDateTime localDateTime = LocalDateTime.parse(currTimestamp, DateTimeFormatter.ISO_DATE_TIME);

            LocalDateTime prevTime = LocalDateTime
                    .of(localDateTime.getYear(), localDateTime.getMonth(),
                            localDateTime.getDayOfMonth(), localDateTime.getHour(),
                            localDateTime.getMinute());

            String time = prevTime.getMonth() + "-" +
                    prevTime.getDayOfMonth() + " " + prevTime.getHour() + "h";
            String lineStop = values.getLineId() + "-" + values.getLastStop();
            return new Tuple2<>(lineStop + "-" + time, 1);
            //Maybe I can use a reduce here to maintain only different busId
            //If the values are different, count like 2, else like 1

        }
    }
}
