#!/usr/bin/python3

import json
import os

curr_dir = os.path.dirname(os.path.abspath(__file__))
input_dir = curr_dir + "/input/"
output_dir = curr_dir + "/output/"

months = ["april", "may", "june"]
line_id = ["1450", "1465", "1651", "198", "32772", "34694"]
#prev_timestamp = curr_timestamp = "2019-04-14"

for month in months:
    for line in line_id:
        fname = input_dir + month + "/bus_pos_line_" + line + ".txt"
        fout = output_dir + month + "/bus_pos_line_" + line + ".txt"

        #The w+ means opens for writing and creates the file in case it does not exist
        with open(fname, "r") as infile, open(fout, "w+") as outfile:
            data = json.load(infile)
            print(fname)
            for item in data["info"]:
                for vehicle in item["vs"]:
                    content = "%s, %s, %s, %s, %s, %s\n" % (item["hr"], vehicle['p'], line, vehicle['px'], vehicle['py'], vehicle['ta'])
                    outfile.write(content)                
            

