#!/usr/bin/python3

import os
import sys
import json
import math
from datetime import datetime

dirpath = os.path.abspath(os.path.dirname(sys.argv[0])) + "/"

FMT = '%H:%M'
#months = ["april", "may", "june"]
months = ["april"]

dict_line_stops = {
"1651": [1410074, 1410075, 1410076, 3305795, 3305796, 3305797],

"1465": [670016557, 706325, 70016561, 440015161, 440015158, 440015164, 340015323, 340015329, 530015342, 530015348, 340015750, 340015746, 150015738, 720015734, 720015730],
"1465-2": [810009963, 810009941, 810009943, 810009945, 810009948, 810009950, 810009951, 810009954, 810009956, 230009957, 230009960, 230009962, 230009829, 230009870, 230009868, 230009867, 230009866, 230009864, 230009861, 230009860, 230009858],

"1450": [670016557, 706325, 70016561, 440015161, 440015158, 440015164, 340015323, 340015329, 530015342, 530015348, 340015750, 340015746, 150015738, 720015734, 720015730],

"32772": [260016860,260016858,260016856,260016855],
"32772-2": [340015333,340015337,340015346,340015751,340015748,340015740,340015736,720015731],

"34694": [440015007,440015005, 440015003, 630015010,120011357,120011361,120011363,550011365,120011367,960011368,960011373],
"198": [920015225,920015205,920015204,920015203,530015173,530015320,530015319,530015318,530015317,1506163]}



#metric_per_stop = { line-stop = [max, min, sum, cont] }
metrics_per_stop = { }


for month in months:
    for lineId in dict_line_stops:
        i = 0
        lineIdentifier = lineId.split("-")[0]
        while (len(dict_line_stops[lineId]) > i+1):
            stop1 = str(dict_line_stops[lineId][i])
            stop2 = str(dict_line_stops[lineId][i+1])
            fname = dirpath + "output/" + month + "/arrival_time/line_" + lineIdentifier + "_stop_" + stop1 + "_stop2_" + stop2 + ".txt" 
            key = stop2 + "-" + lineIdentifier
            i+=1
            with open(fname, "r") as infile:
                cont = 1
                for line in infile:
                    content = line.split(",")
                    if (len(content) > 5):
                        prev1 = content[2].strip()
                        prev2 = content[4].strip()
                        tdelta = datetime.strptime(prev2, FMT) - datetime.strptime(prev1, FMT)
                        minutes_diff = tdelta.seconds/60
                        
                        #[max, min,sum, cont]
                        if (metrics_per_stop.get(key) != None):
                            values = metrics_per_stop.get(key)
                            if (minutes_diff > values[0]):
                                values[0] = minutes_diff
                            if (minutes_diff < values[1]):
                                values[1] = minutes_diff
                            values[2] += minutes_diff
                            values[3] = cont

                            metrics_per_stop[key] = values
                        else:
                            metrics_per_stop[key] = [minutes_diff, minutes_diff, minutes_diff, cont]
                    
                    cont+=1

for key in metrics_per_stop:
    print(key, " values: ", metrics_per_stop[key] )
