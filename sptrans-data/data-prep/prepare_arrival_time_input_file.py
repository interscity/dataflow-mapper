#!/usr/bin/python3

import os
import sys
import re

curr_dir = os.path.dirname(os.path.abspath(__file__))

months = ["september"]
lines = ["1450", "1465", "1651", "198", "32772", "34694"]
stops_line_1450 = ["706325", "70016561", "150015738", "340015323", "340015329", "340015746", "340015750", "440015158", "440015161", "440015164", "530015342", "530015348", "670016557", "720015730", "720015734"]
stops_line_1465 = ["706325", "70016561", "150015738", "230009829", "230009856", "230009858", "230009860", "230009861", "230009864", "230009866", "230009867", "230009868", "230009870", "230009957", "230009960", "230009962", "340015323", "340015329", "340015746", "340015750", "440015158", "440015161", "440015164", "530015342", "530015348", "670016557", "720015730", "720015734", "810009941", "810009943", "810009945", "810009948", "810009950", "810009951", "810009954", "810009956", "810009963"]
stops_line_1651 = ["1410074", "1410075", "1410076", "3305795", "3305796", "3305797"]
stops_line_198 = ["1506163", "4200946", "4203717", "530015173", "530015317", "530015318", "530015319", "530015320", "920015203", "920015204", "920015205", "920015225"]
stops_line_32772 = ["260016855", "260016856", "260016858", "260016860", "340015333", "340015337", "340015346", "340015736", "340015740", "340015748", "340015751", "720015731"]
stops_line_34694 = ["120011357", "120011361", "120011363", "120011367", "440015003", "440015005", "440015007", "550011365", "630015010", "630015011", "960011368", "960011373"]
stops = [stops_line_1450, stops_line_1465, stops_line_1651, stops_line_198, stops_line_32772, stops_line_34694]

i = 0
#Treatment to convert to JSON
for month in months:
    for line in lines:
        for stop in stops[i]:
            dir_name = curr_dir + "/input/" + month + "/arrival_time/"
            fname = "%sline_%s_stop_%sold.txt" % (dir_name, line, stop)
            outfname = "%sline_%s_stop_%s.txt" % (dir_name, line, stop)
            with open(fname, "r") as infile, open(outfname, "w") as outfile:
                content = infile.readlines()
                content = ''.join(content)
                #content = re.sub(r"}[,]*{", "},{", content)
                content = content.replace('{\n    "Message": "Authorization has been denied for this request."\n}', '')
                content = content.replace("}{", "},{")
                content = '{ \"info\": [\n' + content + '\n]}'
                outfile.write(content)
            
            print(fname)
        i+=1
    i = 0
